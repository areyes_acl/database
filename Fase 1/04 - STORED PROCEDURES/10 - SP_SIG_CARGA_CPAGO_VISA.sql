create or replace PROCEDURE       "SP_SIG_CARGA_CPAGO_VISA" (warning       OUT VARCHAR2,
                                             cod_error      OUT NUMBER
                                                       
)
IS
   
        voCursor  Sys_RefCursor:=null;
        prfCursor Sys_RefCursor:=null;

-- Proyecto: SIG
-- Nombre Objeto:sp_Sig_CargaIncoming
-- By: Developer || ACL PTM | 10/07/2013
-- Versión : 1.1
-- Descripcion:carga tbl_cpago_visa      desde tmp_cpago_visa


BEGIN
      DECLARE
           SID                                       SIG.TMP_CPAGO_VISA.SID%TYPE                          ;
           MIT                                       SIG.TMP_CPAGO_VISA.MIT%TYPE                          ;
           CODIGO_FUNCION                            SIG.TMP_CPAGO_VISA.CODIGO_FUNCION%TYPE               ;
           DESTINATION_BIN                           SIG.TMP_CPAGO_VISA.DESTINATION_BIN%TYPE              ;
           SOURCE_BIN                                SIG.TMP_CPAGO_VISA.SOURCE_BIN%TYPE                   ;
           REP_SRE_IDENTIFIER                        SIG.TMP_CPAGO_VISA.REP_SRE_IDENTIFIER%TYPE           ;
           REC_SRE_IDENTIFIER                        SIG.TMP_CPAGO_VISA.REC_SRE_IDENTIFIER%TYPE           ;
           SERVICE_IDENTIFIER                        SIG.TMP_CPAGO_VISA.SERVICE_IDENTIFIER%TYPE           ;
           CURRENCY_CODE                             SIG.TMP_CPAGO_VISA.CURRENCY_CODE%TYPE                ;
           BUSINESS_MODE                             SIG.TMP_CPAGO_VISA.BUSINESS_MODE%TYPE                ;
           RESERVED1                                 SIG.TMP_CPAGO_VISA.RESERVED1%TYPE                    ;
           REPORT                                    SIG.TMP_CPAGO_VISA.REPORT%TYPE                       ;
           SETTLEMENT_DATE                           SIG.TMP_CPAGO_VISA.SETTLEMENT_DATE%TYPE              ;
           REPORT_DATE                               SIG.TMP_CPAGO_VISA.REPORT_DATE%TYPE                  ;
           TOTAL_INTERCHANGE_COUNT                   SIG.TMP_CPAGO_VISA.TOTAL_INTERCHANGE_COUNT%TYPE      ;
           TOTAL_INTERCHANGE_VALUE                   SIG.TMP_CPAGO_VISA.TOTAL_INTERCHANGE_VALUE%TYPE      ;
           INTERCHANGE_VALUE_SIGN                    SIG.TMP_CPAGO_VISA.INTERCHANGE_VALUE_SIGN%TYPE       ;
           TOTAL_REIMBURSEMENT_FEES                  SIG.TMP_CPAGO_VISA.TOTAL_REIMBURSEMENT_FEES%TYPE     ;
           REIMBURSEMENT_FEES_SIGN                   SIG.TMP_CPAGO_VISA.REIMBURSEMENT_FEES_SIGN%TYPE      ;
           TOTAL_VISA_CHARGES                        SIG.TMP_CPAGO_VISA.TOTAL_VISA_CHARGES%TYPE           ;
           VISA_CHARGES_SIGN                         SIG.TMP_CPAGO_VISA.VISA_CHARGES_SIGN%TYPE            ;
           NET_SETTLEMENT_AMOUNT                     SIG.TMP_CPAGO_VISA.NET_SETTLEMENT_AMOUNT%TYPE        ; 
           NET_SETTLEMENT_AMOUNT_SIGN                SIG.TMP_CPAGO_VISA.NET_SETTLEMENT_AMOUNT_SIGN%TYPE   ;
           SUMMARY_LEVEL                             SIG.TMP_CPAGO_VISA.SUMMARY_LEVEL%TYPE                ;
           RESERVED2                                 SIG.TMP_CPAGO_VISA.RESERVED2%TYPE                    ;
           OPERADOR                                  SIG.TMP_CPAGO_VISA.OPERADOR%TYPE                     ;

     BEGIN

      dbms_output.enable(NULL);

                OPEN voCursor FOR
                SELECT
                      MIT,
                      CODIGO_FUNCION,
                      DESTINATION_BIN,
                      SOURCE_BIN,
                      REP_SRE_IDENTIFIER,
                      REC_SRE_IDENTIFIER,
                      SERVICE_IDENTIFIER,
                      CURRENCY_CODE,
                      BUSINESS_MODE,
                      RESERVED1,
                      REPORT,
                      SETTLEMENT_DATE,
                      REPORT_DATE,
                      TOTAL_INTERCHANGE_COUNT,
                      TOTAL_INTERCHANGE_VALUE,
                      INTERCHANGE_VALUE_SIGN,
                      TOTAL_REIMBURSEMENT_FEES,
                      REIMBURSEMENT_FEES_SIGN,
                      TOTAL_VISA_CHARGES,
                      VISA_CHARGES_SIGN,
                      NET_SETTLEMENT_AMOUNT,
                      NET_SETTLEMENT_AMOUNT_SIGN,
                      SUMMARY_LEVEL,
                      RESERVED2,
                      OPERADOR                         
                 FROM  SIG.TMP_CPAGO_VISA;
                    prfCursor:=voCursor;

        LOOP                                                                                           
                    FETCH prfCursor                                                                              
                    INTO
                      MIT,
                      CODIGO_FUNCION,
                      DESTINATION_BIN,
                      SOURCE_BIN,
                      REP_SRE_IDENTIFIER,
                      REC_SRE_IDENTIFIER,
                      SERVICE_IDENTIFIER,
                      CURRENCY_CODE,
                      BUSINESS_MODE,
                      RESERVED1,
                      REPORT,
                      SETTLEMENT_DATE,
                      REPORT_DATE,
                      TOTAL_INTERCHANGE_COUNT,
                      TOTAL_INTERCHANGE_VALUE,
                      INTERCHANGE_VALUE_SIGN,
                      TOTAL_REIMBURSEMENT_FEES,
                      REIMBURSEMENT_FEES_SIGN,
                      TOTAL_VISA_CHARGES,
                      VISA_CHARGES_SIGN,
                      NET_SETTLEMENT_AMOUNT,
                      NET_SETTLEMENT_AMOUNT_SIGN,
                      SUMMARY_LEVEL,
                      RESERVED2,
                      OPERADOR            ;

                    EXIT WHEN prfCursor%NOTFOUND; 

              --SOLO PRUEBAS
           DBMS_OUTPUT.PUT_LINE('salida fetch');

            select case 
               when exists(SELECT SID FROM TBL_BINES_NACIONALES WHERE OBIN = SOURCE_BIN)
               then 2
               else 3
             end  into OPERADOR
           from dual;
              --FIN PRUEBAS             


                    DBMS_OUTPUT.PUT_LINE('INSERT A LA TBL_CPAGO_VISA');
                    INSERT INTO SIG.TBL_CPAGO_VISA (
                      SID,
                      MIT,
                      CODIGO_FUNCION,
                      DESTINATION_BIN,
                      SOURCE_BIN,
                      REP_SRE_IDENTIFIER,
                      REC_SRE_IDENTIFIER,
                      SERVICE_IDENTIFIER,
                      CURRENCY_CODE,
                      BUSINESS_MODE,
                      RESERVED1,
                      REPORT,
                      SETTLEMENT_DATE,
                      REPORT_DATE,
                      TOTAL_INTERCHANGE_COUNT,
                      TOTAL_INTERCHANGE_VALUE,
                      INTERCHANGE_VALUE_SIGN,
                      TOTAL_REIMBURSEMENT_FEES,
                      REIMBURSEMENT_FEES_SIGN,
                      TOTAL_VISA_CHARGES,
                      VISA_CHARGES_SIGN,
                      NET_SETTLEMENT_AMOUNT,
                      NET_SETTLEMENT_AMOUNT_SIGN,
                      SUMMARY_LEVEL,
                      RESERVED2,
                      OPERADOR,
                      FECHA_INCOMING) 

                      VALUES 

                      (SEQ_TBL_CPAGO_VISA.NEXTVAL,
                      TRIM(MIT),
                      TRIM(CODIGO_FUNCION),
                      TRIM(DESTINATION_BIN),
                      TRIM(SOURCE_BIN),
                      TRIM(REP_SRE_IDENTIFIER),
                      TRIM(REC_SRE_IDENTIFIER),
                      TRIM(SERVICE_IDENTIFIER),
                      TRIM(CURRENCY_CODE),
                      TRIM(BUSINESS_MODE),
                      TRIM(RESERVED1),
                      TRIM(REPORT),
                      TO_DATE(SETTLEMENT_DATE, 'DDMMYY'),
                      TO_DATE(REPORT_DATE, 'DDMMYY'),
                      TO_NUMBER(TOTAL_INTERCHANGE_COUNT),
                      TO_NUMBER(TOTAL_INTERCHANGE_VALUE),
                      (SELECT SID FROM TBL_CPAGO_VISA_SIGN WHERE SIGN = TRIM(INTERCHANGE_VALUE_SIGN)),
                      TO_NUMBER(TOTAL_REIMBURSEMENT_FEES),
                      (SELECT SID FROM TBL_CPAGO_VISA_SIGN WHERE SIGN = TRIM(REIMBURSEMENT_FEES_SIGN)),
                      TO_NUMBER(TOTAL_VISA_CHARGES),
                      (SELECT SID FROM TBL_CPAGO_VISA_SIGN WHERE SIGN = TRIM(VISA_CHARGES_SIGN)),
                      TO_NUMBER(NET_SETTLEMENT_AMOUNT),
                      (SELECT SID FROM TBL_CPAGO_VISA_SIGN WHERE SIGN = TRIM(NET_SETTLEMENT_AMOUNT_SIGN)),
                      TRIM(SUMMARY_LEVEL),
                      TRIM(RESERVED2),
                      TO_NUMBER(OPERADOR),
                      SYSDATE);


                END LOOP;
                DBMS_OUTPUT.PUT_LINE('salida loop');
                CLOSE prfCursor;
                DBMS_OUTPUT.PUT_LINE('cierre cursor');
     END;            

  --tabla SIG.TMP_CPAGO_VISA SE LIMPIA
 execute immediate 'truncate table SIG.TMP_CPAGO_VISA';
 COMMIT;
EXCEPTION
WHEN  NO_DATA_FOUND     THEN
            DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS EN TABLA SIG.TMP_CPAGO_VISA');
WHEN  OTHERS THEN
      warning:=SQLERRM;   
      cod_error:=SQLCODE;
      DBMS_OUTPUT.PUT_LINE('ERROR : '||warning||' SQLCODE : '||cod_error);   
--insercion en tabla log error en caso de algun error desconocido          
     INSERT INTO LOG_ERROR (SID, 
                       FECHA_INSERCION, 
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_CARGA_CPAGO_VISA',
                        cod_error||warning );
END SP_SIG_CARGA_CPAGO_VISA;

