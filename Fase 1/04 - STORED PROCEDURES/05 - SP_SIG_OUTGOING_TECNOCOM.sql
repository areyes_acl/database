create or replace PROCEDURE SP_SIG_OUTGOING_TECNOCOM
(
  warning             OUT VARCHAR2,
  cod_error           OUT NUMBER,
  prfCursor           OUT SYS_REFCURSOR
)
AS
  voCursor SYS_REFCURSOR:=null;
BEGIN
OPEN voCursor FOR
  SELECT 
  sid                                    AS NUMEROCASO,
  fecha_accion						     AS FECHAINGRESO,                
  nro_tarjeta                            AS NROTARJETA,                 
  nro_secuencia_tarjeta                  AS RUT,  
  datos_referencia_adquirente            AS CLIENTE,   
  fecha_hr_trasac                        AS FECHATRANSACCION,   
  cod_motivo_mensaje                     AS MOTIVORECLAMO,  
  monto_transac                          AS MONTO,   
  nombre_ubic_acep_tarjeta               AS COMERCIO
  FROM TMP_INCOMING;
prfCursor:=voCursor;
EXCEPTION                                                                               
  WHEN OTHERS THEN                                                                      
  DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);                                                            
  DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);     
  warning:=SQLERRM;    
  cod_error:=SQLCODE;  
  INSERT INTO LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) 
  VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_OUTGOING_TECNOCOM', warning||cod_error );
  commit;
END;