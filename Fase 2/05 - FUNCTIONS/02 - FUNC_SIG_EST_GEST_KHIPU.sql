create or replace FUNCTION       "FUNC_SIG_EST_GEST_KHIPU" ( 
                                                          SID_ID IN NUMBER 
                                                        ) 
RETURN VARCHAR2 AS 
response VARCHAR(10);

BEGIN

    SELECT * into response FROM (
                   SELECT ESTADO
                   FROM TBL_CONC_KHIPU_GEST  
                   WHERE SID_CONC = SID_ID
                   ORDER BY SID DESC
                 ) 
   WHERE ROWNUM <= 1;
   

 DBMS_OUTPUT.PUT_LINE('RESPONSE: '||response);   
return response;
 
 
END FUNC_SIG_EST_GEST_KHIPU;