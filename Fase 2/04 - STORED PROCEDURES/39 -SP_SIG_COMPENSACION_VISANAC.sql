create or replace PROCEDURE       "SP_SIG_COMPENSACION_VISANAC" (
    fecha_inicio         IN VARCHAR2,
    fecha_termino        IN VARCHAR2,
    filtro_operador      IN NUMBER,
    cod_error           OUT NUMBER,
    warning             OUT VARCHAR2,
    prfCursor           OUT Sys_RefCursor
)
IS
    voCursor  Sys_RefCursor:=null;
    rptCursor Sys_RefCursor:=null;
BEGIN
DECLARE
    
BEGIN
    
   -- select op.sid into id_operador_producto from tbl_operador_producto op where op.ID_PRODUCTO = producto_id and op.ID_OPERADOR = filtro_operador;
	--select p.BIN, p.SUBLEDGER into bin_producto, subledger from tbl_producto p where p.SID = producto_id;
	
   -- DBMS_OUTPUT.PUT_LINE('tipoMov: '|| tipoMov);
    cod_error := 0;
    warning := 'Se ha ejecutado SP_SIG_COMPENSACION_VISANAC correctamente.';

  --  IF(tipo_movimiento <> '0') THEN
   --     sql_extra := ' AND TIPO_MOV = ''' || tipo_movimiento || '''';
 --   END IF;

  --  sql_stmt := ' SELECT MIT,CODIGO_FUNCION,DEBE,HABER FROM TBL_ASIENTOS WHERE OPE_PROD = ' || id_operador_producto || ' AND ESTADO=1 ' || sql_extra || ' ORDER BY TIPO_MOV ASC';

    -- IMPRIME LA QUERY           
   -- DBMS_OUTPUT.PUT_LINE (sql_stmt);

    -- EJECUTA LA QUERY DE BUSQUEDA DE REGISTROS
   -- OPEN voCursor FOR sql_stmt;
   -- rptCursor:=voCursor;

   -- sql_stmt := '';

     OPEN voCursor FOR
       SELECT  	TI.SID,
								TI.MIT,
                TRUNC(TI.FECHA_INCOMING) AS FECHA_INCOMING,
								TI.NRO_TARJETA,
                TI.CODIGO_AUTORIZACION,
                TI.COD_IDENT_ACEP_TARJETA,
                substr(TI.NOMBRE_UBIC_ACEP_TARJETA,0,30)AS NOMBRE_UBIC_ACEP_TARJETA,
                TI.MCC,
                substr(TI.DATOS_REFERENCIA_ADQUIRENTE, 2,6) AS DATOS_REFERENCIA_ADQUIRENTE,
                TRUNC(TI.FECHA_HR_TRASAC) AS FECHA_HR_TRASAC,
								TI.MONTO_TRANSAC AS MONTO_TRANSAC,
								TRUNC(TP.P1000) AS FECHA_COMPENSACION,-- fecha de compensacion,
								TP.P0158 AS IVA_COMISION,--IVA COMISION
								TP.P1003 AS IRF,-- IRF
                TP.P1006 AS TASA_COMERCIO,--VALOR CUOTA
								TP.P1007 AS COMISION_CUOTA,-- COMISION COMERCIO
                TP.P1004 AS NUMERO_CUOTA,
                TP.P1005 AS VALOR_CUOTA,
                TP.P1009 AS MONTO_TCR2,
                TP.P1012 AS NRO_CUOTA,
                (((TI.MONTO_TRANSAC- TO_NUMBER(TP.P0158)- TO_NUMBER(TP.P1003)))/100) AS MONTO_NETO,
                (TO_NUMBER(TP.P0158)- TO_NUMBER(TP.P1003))/TI.MONTO_TRANSAC  AS TASA_INTERCAMBIO_IVA,
                 TO_NUMBER(TP.P1003)/TI.MONTO_TRANSAC AS TASA_INTERCAMBIO
                     
            FROM TBL_INCOMING TI
            JOIN SIG.TBL_PDS TP ON (TI.SID = TP.transaccion)
            WHERE  OPERADOR =filtro_operador AND 
            TI.COD_MONEDA_TRANSAC = '152'
            AND TI.mit in ('05','06','07','25','26','27') AND
           -- TRUNC(TI.FECHA_INCOMING) >= TRUNC(to_DATE('01-JAN-21', 'DD-MM-YY'))  AND TRUNC(TI.FECHA_INCOMING) <=  TRUNC(to_DATE('12-JAN-21', 'DD-MM-YY'));
             TRUNC(TI.FECHA_INCOMING) BETWEEN  TRUNC(to_date(fecha_inicio, 'DD-MM-YY')) AND TRUNC(to_date(fecha_termino, 'DD-MM-YY'));
     prfCursor:=voCursor;  


END;     
EXCEPTION
    WHEN NO_DATA_FOUND THEN
      warning:='NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS: ' ||SQLERRM;    
      cod_error:=SQLCODE;  
      DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || warning);
      INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_DETALLE_CONTABLE', cod_error || '-' || warning || '-' ||  DBMS_UTILITY.format_error_backtrace);
    WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);
       warning:=SQLERRM;    
       cod_error:=SQLCODE;
       DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace); 
      INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_DETALLE_CONTABLE', cod_error || '-' || warning || '-' ||  DBMS_UTILITY.format_error_backtrace);

      
END SP_SIG_COMPENSACION_VISANAC;