create or replace PROCEDURE       "SP_SIG_CARGA_PCORE_GESTION" (
-- Proyecto: SIG
-- By: Developer || ACL | XX/11/2018
-- Versi�n : 1.0
    sidConci in NUMBER,
    comentario in VARCHAR2,
    gestion in NUMBER,
    idUser in NUMBER,
    FLAG out varchar2,
    ERROR_OUT out varchar2   
)
IS
contador NUMBER:=0;
BEGIN

    SELECT COUNT(*) INTO contador FROM TBL_CONC_PREUNIC_GEST WHERE SID_CONC = sidConci AND ESTADO = 1;

        IF(contador = 0)THEN
          INSERT INTO TBL_CONC_PREUNIC_GEST(SID, SID_CONC, FECHA, SID_USER, COMENTARIO, ESTADO)
          VALUES (SEQ_TBL_CONC_PREUNIC_GEST.NEXTVAL, sidConci, SYSDATE, idUser, comentario, gestion);
          FLAG :='ok';
          ERROR_OUT := '0';
        ELSE
          FLAG :='EXISTE';
          ERROR_OUT := '1';
        END IF;

         COMMIT;  
    EXCEPTION

       WHEN no_data_found THEN
            ERROR_OUT := 'Error';
            INSERT INTO LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CARGA_SACORE_GESTION', ERROR_OUT);
            DBMS_OUTPUT.PUT_LINE(ERROR_OUT); 

        WHEN OTHERS THEN
            ERROR_OUT := 'Error';
            INSERT INTO LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CARGA_SACORE_GESTION', ERROR_OUT);
            DBMS_OUTPUT.PUT_LINE(ERROR_OUT); 

END;