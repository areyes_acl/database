create or replace PROCEDURE SP_SIG_OUTGOING_CONTABLE
(
  warning             OUT VARCHAR2,
  cod_error           OUT NUMBER,
  prfCursor           OUT SYS_REFCURSOR
)
AS
  voCursor SYS_REFCURSOR:=null;
BEGIN
OPEN voCursor FOR
  SELECT 
  RGDIDTRXSEND						     AS RGDIDTRXSEND,                RGDMNMONTO                AS RGDMNMONTO,
  RGDTPMOVORG                            AS RGDTPMOVORG,                 RGDIDPRODUCTO             AS RGDIDPRODUCTO,
  RGDIDSUCURSALMOVIMIENTO                AS RGDIDSUCURSALMOVIMIENTO,     RGDIDSUCURSALAPERTURA     AS RGDIDSUCURSALAPERTURA,
  RGIDSUCURDALPAGO                       AS RGIDSUCURDALPAGO,            RGDIDCOMERCIO             AS RGDIDCOMERCIO,
  TO_CHAR(RGDDTFECHAMOV,'DD-MM-YYYY')    AS RGDDTFECHAMOV,               RGDCTIPODEBCRED           AS RGDCTIPODEBCRED,
  RGDSZCOMPANIA                          AS RGDSZCOMPANIA,               RGDSZSUBLEDGER            AS RGDSZSUBLEDGER,
  NVL(RGDSZEXPLICACION,'')               AS RGDSZEXPLICACION,            RGDSZPORTAFOLIOCLIENTE    AS RGDSZPORTAFOLIOCLIENTE,  
  RGDSZCUENTA                            AS RGDSZCUENTA,
  RUT_CLIENTE                            AS RUTCLIENTE
  FROM TMP_CONTABLE;
prfCursor:=voCursor;
EXCEPTION                                                                               
  WHEN OTHERS THEN                                                                      
  DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);                                                            
  DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);     
  warning:=SQLERRM;    
  cod_error:=SQLCODE;  
  INSERT INTO LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) 
  VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_OUTGOING_CONTABLE', warning||cod_error );
  commit;
END;