CREATE TABLE "SIG"."TBL_LOG_OUT_KHIPU" 
   (	
   "SID" NUMBER NOT NULL, 
	"FILE_TS" TIMESTAMP (6), 
	"FILE_NAME" VARCHAR2(50 BYTE), 
	"FILE_FLAG" VARCHAR2(2 BYTE), 
	"FECHA" DATE, 
	 CONSTRAINT TBL_LOG_OUT_KHIPU_PK PRIMARY KEY 
	  (
		SID 
	  )
	  ENABLE 
	);

   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_KHIPU"."SID" IS 'Corresponde al id de la tabla TBL_LOG_OUT_KHIPU';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_KHIPU"."FILE_TS" IS 'Corresponde a la estampa de tiempo del proceso del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_KHIPU"."FILE_NAME" IS 'Corresponde al nombre del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_KHIPU"."FILE_FLAG" IS 'Corresponde al flag de procesamiento creado 0:subido 1:-1:error';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_KHIPU"."FECHA" IS 'Corresponde a la fecha de Procesamiento del archivo';
   COMMIT;