--Script para setear el lenguaje a Inglés y no tener conflictos con las fechas en las tablas

ALTER SESSION SET NLS_DATE_LANGUAGE = 'AMERICAN';
ALTER SESSION SET NLS_TERRITORY = 'AMERICA';
ALTER SESSION SET NLS_LANGUAGE = 'AMERICAN';
commit;