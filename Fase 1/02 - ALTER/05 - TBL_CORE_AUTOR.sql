
ALTER TABLE TBL_CORE_AUTOR ADD "IMP_TRANSACCION_VISA" NUMBER(10,2);
ALTER TABLE TBL_CORE_AUTOR ADD "COD_MONEDA_VISA" NUMBER(3,0);
ALTER TABLE TBL_CORE_AUTOR ADD "COD_MONEDA_ORG_VISA" NUMBER(3,0);
ALTER TABLE TBL_CORE_AUTOR ADD "COD_MONEDA_CON_VISA" NUMBER(3,0);
ALTER TABLE TBL_CORE_AUTOR ADD "FEC_TRANSAC_VISA" VARCHAR2(20);
ALTER TABLE TBL_CORE_AUTOR ADD "IMP_OPERACION" NUMBER(10,2);
ALTER TABLE TBL_CORE_AUTOR ADD "IMP_CONCILIACION" NUMBER(10,2);

   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."IMP_TRANSACCION_VISA" IS 'Monto de la transacción en Moneda de Origen
Se usará para validar en la conciliación, cuando se trate de una transacción Internacional NO CONCILIADA, comparando este campo contra el Monto en Moneda de Origen informado en el Incoming Visa.
Si ambos montos son iguales, entonces se debe considerar CONCILIADA la transacción.';
   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."COD_MONEDA_VISA" IS 'Codigo de Moneda de Intercambio (152)';
   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."COD_MONEDA_ORG_VISA" IS 'Código de Moneda de Origen
Si este valor es distinto de 152, entonces se trata de una transacción Internacional a la que se debe aplica la logica detallada mas arriba si no está CONCILIADA.';
   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."COD_MONEDA_CON_VISA" IS 'Código de Moneda de Liquidación (152)';
   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."FEC_TRANSAC_VISA" IS 'Fecha de la Autorización';
   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."IMP_OPERACION" IS 'Monto de la transacción en Moneda de Intercambio';
   COMMENT ON COLUMN "SIG"."TBL_CORE_AUTOR"."IMP_CONCILIACION" IS 'Monto de la transacción en Moneda de Liquidación';
