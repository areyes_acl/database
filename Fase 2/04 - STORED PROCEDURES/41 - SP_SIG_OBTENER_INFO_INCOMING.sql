create or replace PROCEDURE "SP_SIG_OBTENER_INFO_INCOMING"(
                                                              id_sid           IN NUMBER,
                                                              warning       OUT VARCHAR2,
                                                              cod_error     OUT NUMBER,
                                                              prfcursor     OUT SYS_REFCURSOR)
IS  
/******************************************************************************
   NAME:       SP_SIG_OBTENER_INFO_INCOMING
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        XX-10-2018   acl(PTM)       

   NOTES:

      Object Name:     SP_SIG_OBTENER_INFO_TRX_BICE
      Sysdate:          XX-10-2018 
******************************************************************************/
  vocursor                 SYS_REFCURSOR := NULL;
BEGIN
    cod_error := 0;
    warning := 'Proceso SP_SIG_OBTENER_INFO_INCOMING ejecutado correctamente ';

    BEGIN
        OPEN vocursor FOR    
            SELECT 
                SID AS sid,
                XKEY AS xkey,
                MIT AS mit,
                NRO_TARJETA AS nroTarjeta,
                COD_DATOS_PUNTO_SERVICIO AS codDatosPuntoServicio,
                datos_referencia_adquirente AS datosReferenciaAdquiriente,
                CODIGO_AUTORIZACION AS codigoAutorizacion,
                CODIGO_SERVICIO AS codigoServicio,
                OPERADOR AS operador,
                MONTO_TRANSAC AS monto,
                MONTO_CONCILIACION as montoConciliacion,
                MONTO_FACTURACION as montoFacturacion,
                NOMBRE_UBIC_ACEP_TARJETA AS nombreUbicAcepTarjeta,
                NVL(TO_CHAR(FECHA_INCOMING,'DD-MON-RR'),' ') AS fechaIncoming,
                NVL(TO_CHAR(FECHA_HR_TRASAC,'DD-MON-RR'),' ') AS fechaHrTransac,
                COD_IDENT_ACEP_TARJETA as rut -- para salcobrand y preunic
            FROM TBL_INCOMING
            WHERE SID = id_sid;

        prfcursor := vocursor;
 END;

EXCEPTION
  WHEN no_data_found THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             dbms_output.Put_line( 'NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS' ); WHEN OTHERS THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             INSERT INTO sig.LOG_ERROR
                         (sid,
                          fecha_insercion,
                          nombre_sp,
                          msg_error)
             VALUES      (sig.seq_log_error.NEXTVAL,
                          SYSDATE,
                          'SP_SIG_OBTENER_INFO_INCOMING',
                          cod_error
                          || '-'
                          || warning);

             dbms_output.Put_line( 'ERROR : '
                                   || ' '
                                   || cod_error
                                   || '-'
                                   || warning );

END SP_SIG_OBTENER_INFO_INCOMING;