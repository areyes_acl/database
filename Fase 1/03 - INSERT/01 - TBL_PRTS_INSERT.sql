
--Inserts en tabla PRTS de par�metros de descarga de la interfaz del core de autorizaciones

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_TRX_AUTH_SBPAY','127.0.0.1','Ip del servidor SFTP desde donde se descargaran las interfaces de trx AUTORIZADAS Sbpay.','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_TRX_AUTH_SBPAY','22','Puerto del servidor SFTP desde donde se descargaran las interfaces de autorizaciones .','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_TRX_AUTH_SBPAY','sig','Usuario con el cual se conectara el cron al servidor SFTP para descargar los archivos DE TRX AUTORIZADAS Sbpay.','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_TRX_AUTH_SBPAY','sig','Password con la cual se conectara el cron al  servidor SFTP desde donde se descargara el archivo de TRX AUTORIZADAS Sbpay.','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DWNLD_TRX_SBPAY','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SBPAY/CORE_AUTH/','Ruta del servidor SFTP desde donde se descargara el archivo de TRX AUTORIZADAS Sbpay.','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILE_AUTH_SBPAY','CORE_','Formato Nomenclatura log de trx "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_AUTH_SBPAY','txt','Extension del archivo de autorizaciones core SBPAY"','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_AUTH_SBPAY','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/CORE_AUTH/','Ruta donde se almacenan las interfaces del core de autorizaciones para ser procesadas','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_AUTH_SBPAY_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/CORE_AUTH/BKP/','Ruta Donde ACL guardara los archivos de autorizaciones procesados','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_AUTH_SBPAY_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/CORE_AUTH/ERROR/','Ruta Donde ACL guardara los archivos de autorizaciones con error','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_INC_VISA','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/VISA','Ruta en el servidor de SBPAY donde se dejan los archivos Incoming VISA descargados','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_INC_VISA_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/VISA/BKP/','Ruta donde SBPAY dejar� los archivos procesados y subidos al FTP ','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_INC_VISA_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/VISA/ERROR/','Ruta donde SBPAY dejar� los archivos con error que no pudieron ser subidos al FTP ','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_OUT_VISA','PTJV','Formato Nomenclatura arch Incoming VISA "que comience con"','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_OUT_VISA','D2','Extension del archivo Incoming VISA','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_OUT_CTR_VISA','D2','Extension del archivo de control de Incoming VISA','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_UPLOAD_OUT_VISA','127.0.0.1','IP del servidor SFTP de VISA donde se sube el archivo Outgoing','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_UPLOAD_OUT_VISA','22','PUERTO del servidor SFTP de VISA donde se sube el archivo Outgoing','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_UPLOAD_OUT_VISA','sig','USER del servidor SFTP de VISA donde se sube el archivo Outgoing','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_UPLOAD_OUT_VISA','sig','PASS  del servidor SFTP de VISA donde se sube el archivo Outgoing','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_IN_UPLD_OUT_VISA','OUTGOING_VISA','Ruta del sftp donde se sube archivo Outgoing de VISA ','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','RUT_COM_OUT_VISA','96.623.540-3',' RUT SBPAY','admin',to_date('01-SEP-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_UP_OUT_VISA','C:/SFTP_SBPAY/OUTGOING_VISA/','Path donde se dejara el archivo Outgoing de VISA para ser subido','admin',to_date('01-SEP-20','DD-MON-RR'),to_date('16-NOV-15','DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_INC_VISA','^PTJV[a-zA-Z_0-9].*[.D][0-9]{8}$','Expresion regular para validar el formato de los archivos','admin',to_date('26-JUN-19','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','RUT_COM_VI_VISA','96.623.540-3','RUT SBPAY','admin',to_date('26-JUN-19','DD-MON-RR'),null);
INSERT INTO SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_INC_VISA_OUT', '^PTJV[a-zA-Z_0-9].*[.D]', 'Expresion regular para validar el formato de los archivos en la salida', 'admin', SYSDATE, SYSDATE);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_INCOMING_VISA','127.0.0.1','Ip del servidor SFTP desde donde se descargar? el archivo OUTGOING VISA INTERNACIONAL.',null,null,null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_INCOMING_VISA','22','Puerto del servidor SFTP desde donde se descargar? el archivo OUTGOING VISA INTERNACIONAL.',null,null,null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_INCOMING_VISA','sig','PASS  del servidor FTP donde se descarga archivo outgoin visa internacional','admin',to_date('09-APR-19','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_INCOMING_VISA','sig','USER del servidor FTP donde se descarga archivo outgoin visa internacional','admin',to_date('09-APR-19','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DWNLD_INCOMING_VISA','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SBPAY/VISA','Ruta en el servidor de FTP  desde donde se descargan los archivos outgoing visa internacional','admin',to_date('09-APR-19','DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_OUT_TECNOCOM','csv','Extension del archivo Outgoing Tecnocom','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_OUT_TECNOCOM','VISA_DOM_PRD_','NOMBRE del archivo Outgoing Tecnocom','admin',to_date(sysdate,'DD-MON-RR'),null);


/*FILES CONTABLE*/
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_OUT_CONTABLE','AR_','NOMBRE del archivo Outgoing CONTABLE','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_OUT_CONTABLE','txt','Extension del archivo Outgoing CONTABLE','admin',to_date(sysdate,'DD-MON-RR'),null);
/*SFTP CONTABLE*/	
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_UPLOAD_OUT_CONTABLE','127.0.0.1','IP del servidor SFTP de SBPAY donde se sube el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_PORT_UPLOAD_OUT_CONTABLE',
'22',
'PUERTO del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_USER_UPLOAD_OUT_CONTABLE',
'sig',
'USUARIO del servidor SFTP de SBPAY para subir el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_PASS_UPLOAD_OUT_CONTABLE',
'sig',
'PASS del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'PATH_IN_UPLD_OUT_CONTABLE',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SBPAY/CORE_CONTABLE',
'Ruta del sftp donde se sube archivo Outgoing CONTABLE',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'PATH_UP_OUT_CONTABLE',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/CORE_CONTABLE',
'Path donde se dejara el archivo Outgoing CONTABLE para ser subido',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'RUT_COM_OUT_CONTABLE',
'1-9',
'RUT SBPAY',
'admin',
to_date(sysdate,'DD-MON-RR'),null);




/*FILES CARGO ABONO*/
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_OUT_CARGO_ABONO','CARGO_ABONO_','NOMBRE del archivo Outgoing CARGO ABONO','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_OUT_CARGO_ABONO','txt','Extension del archivo Outgoing CARGO ABONO','admin',to_date(sysdate,'DD-MON-RR'),null);	




/*SFTP CARGO ABONO*/	
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_IP_UPLOAD_OUT_CARGO_ABONO',
'127.0.0.1',
'IP del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_PORT_UPLOAD_OUT_CARGO_ABONO',
'22',
'PUERTO del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_USER_UPLOAD_OUT_CARGO_ABONO',
'sig',
'USUARIO del servidor SFTP de SBPAY para subir el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_PASS_UPLOAD_OUT_CARGO_ABONO',
'sig',
'PASS del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'PATH_IN_UPLD_OUT_CARGO_ABONO',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SBPAY/CORE_CARGO_ABONO',
'Ruta del sftp donde se sube archivo Outgoing CARGO_ABONO',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'PATH_UP_OUT_CARGO_ABONO',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/CORE_CARGO_ABONO',
'Path donde se dejara el archivo Outgoing CARGO_ABONO para ser subido',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'RUT_COM_OUT_CARGO_ABONO',
'1-9',
'RUT SBPAY',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

/*SFTP TECNOCOM*/	
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_IP_UPLOAD_OUT_TECNOCOM',
'127.0.0.1',
'IP del servidor SFTP de SBPAY donde se sube el archivo Outgoing TECNOCOM',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_PORT_UPLOAD_OUT_TECNOCOM',
'22',
'PUERTO del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_USER_UPLOAD_OUT_TECNOCOM',
'sig',
'USUARIO del servidor SFTP de SBPAY para subir el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'SFTP_PASS_UPLOAD_OUT_TECNOCOM',
'sig',
'PASS del servidor SFTP de SBPAY donde se sube el archivo',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'PATH_IN_UPLD_OUT_TECNOCOM',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SBPAY/TECNOCOM',
'Ruta del sftp donde se sube archivo Outgoing TECNOCOM',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'PATH_UP_OUT_TECNOCOM',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/TECNOCOM',
'Path donde se dejara el archivo Outgoing TECNOCOM para ser subido',
'admin',
to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),
'CRON_SBPAY',
'RUT_COM_OUT_TECNOCOM',
'96.623.540-3',
'RUT SBPAY',
'admin',
to_date(sysdate,'DD-MON-RR'),null);



Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_VISA_SBPAY','22','Puerto del servidor SFTP desde donde se descargara el archivo VISA .','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_VISA_SBPAY','sigcron','USER del servidor FTP donde se descarga archivo visa','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_VISA_SBPAY','G116st3lth','PASS del servidor FTP donde se descarga archivo visa','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_VISA_SBPAY','172.25.7.169','IP del servidor FTP donde se descarga archivo visa','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DWNLD_VISA_SBPAY','/home/sigcron/SBPAY_SFTP/IN/VISA/','Ruta en el servidor de FTP  desde donde se descargan los archivos visa','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_OUT_VISA_CTR','D2','Extension del archivo de control Incoming VISA','admin',to_date(sysdate,'DD-MON-RR'),to_date(sysdate,'DD-MON-RR'));



commit;
