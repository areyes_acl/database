
--------------------------------------------------------
--  File created - Monday-November-02-2020   
--------------------------------------------------------
REM INSERTING into SIG.TBL_ROL
SET DEFINE OFF;
Insert into SIG.TBL_ROL (SID,DESCRIPCION,XKEY,ACTIVO,NOMBRE_ROL,FECHA_ING,SID_USR_ING,FECHA_MOD,SID_USR_MOD,ESTADO) values ((select max(sid)+1 from TBL_ROL),'Administrador SGI','ADM',1,'Administrador SGI',to_date('02-NOV-20','DD-MON-RR'),null,null,null,'1');
Insert into SIG.TBL_ROL (SID,DESCRIPCION,XKEY,ACTIVO,NOMBRE_ROL,FECHA_ING,SID_USR_ING,FECHA_MOD,SID_USR_MOD,ESTADO) values ((select max(sid)+1 from TBL_ROL),'Usuario Gestion','USU_MOD',1,'Usuario Gestion',to_date('02-NOV-20','DD-MON-RR'),null,null,null,'1');
Insert into SIG.TBL_ROL (SID,DESCRIPCION,XKEY,ACTIVO,NOMBRE_ROL,FECHA_ING,SID_USR_ING,FECHA_MOD,SID_USR_MOD,ESTADO) values ((select max(sid)+1 from TBL_ROL),'Usuario Consulta','USU_CONS',1,'Usuario Consulta',to_date('02-NOV-20','DD-MON-RR'),null,null,null,'1');
Insert into SIG.TBL_ROL (SID,DESCRIPCION,XKEY,ACTIVO,NOMBRE_ROL,FECHA_ING,SID_USR_ING,FECHA_MOD,SID_USR_MOD,ESTADO) values ((select max(sid)+1 from TBL_ROL),'Administrador de usuarios','ADM_USR',1,'Administrador de usuarios',to_date('02-NOV-20','DD-MON-RR'),null,null,null,'1');

COMMIT;