CREATE OR REPLACE PROCEDURE SP_SIG_OUTGOING_TECNOCOMM ( 																
																	warning        OUT VARCHAR2,
                                                                    cod_error      OUT NUMBER,
                                                                    l_ind_Ok_Ko    OUT NUMBER)
IS
    l_contador                                   NUMBER;
    l_suma_footer                                NUMBER(30);
    l_format_contador                            VARCHAR(22);
    l_format_suma_footer                         VARCHAR(22);
    l_datos_adicionales1                         VARCHAR(999);
    out_fecha                                    VARCHAR(6);
    l_cod_acl                                    VARCHAR(50);
    prfCursor                                    Sys_RefCursor:=null;
    voCursor                                     Sys_RefCursor:=null ;
    NO_DATA                                      EXCEPTION;


BEGIN
DECLARE
                        l_sid                                        SIG.TMP_OUTGOING.SID%TYPE                           ;
                        l_mit                                        SIG.TMP_OUTGOING.MIT%TYPE                           ;
                        l_nro_tarjeta                                SIG.TMP_OUTGOING.NRO_TARJETA%TYPE                   ;
                        l_codigo_procesamiento                       SIG.TMP_OUTGOING.CODIGO_PROCESAMIENTO%TYPE          ;
                        l_monto_transac                              SIG.TMP_OUTGOING.MONTO_TRANSAC%TYPE                 ;
                        l_monto_conciliacion                         SIG.TMP_OUTGOING.MONTO_CONCILIACION%TYPE            ;
                        l_monto_facturacion                          SIG.TMP_OUTGOING.MONTO_FACTURACION%TYPE             ;
                        l_tasa_convenio_conciliacion                 SIG.TMP_OUTGOING.TASA_CONVENIO_CONCILIACION%TYPE    ;
                        l_tasa_convenio_facturacion                  SIG.TMP_OUTGOING.TASA_CONVENIO_FACTURACION%TYPE     ;
                        l_fecha_hr_trasac                            SIG.TMP_OUTGOING.FECHA_HR_TRASAC%TYPE               ;
                        l_fecha_vencimiento_tarjeta                  SIG.TMP_OUTGOING.FECHA_VENCIMIENTO_TARJETA%TYPE     ;
                        l_cod_datos_punto_servicio                   SIG.TMP_OUTGOING.COD_DATOS_PUNTO_SERVICIO%TYPE      ;
                        l_nro_secuencia_tarjeta                      SIG.TMP_OUTGOING.NRO_SECUENCIA_TARJETA%TYPE         ;
                        l_codigo_funcion                             SIG.TMP_OUTGOING.CODIGO_FUNCION%TYPE                ;
                        l_cod_motivo_mensaje                         SIG.TMP_OUTGOING.COD_MOTIVO_MENSAJE%TYPE            ;
                        l_mcc                                        SIG.TMP_OUTGOING.MCC%TYPE                           ;
                        l_montos_originales                          SIG.TMP_OUTGOING.MONTOS_ORIGINALES%TYPE             ;
                        l_datos_referencia_adquirente                SIG.TMP_OUTGOING.DATOS_REFERENCIA_ADQUIRENTE%TYPE   ;
                        l_cod_ident_adq                              SIG.TMP_OUTGOING.COD_IDENTIFICACION_ADQUIRENTE%TYPE ;
                        l_cod_ident_inst_envia                       SIG.TMP_OUTGOING.COD_IDENT_INST_ENVIA%TYPE          ;
                        l_nro_referencia_recuperacion                SIG.TMP_OUTGOING.NRO_REFERENCIA_RECUPERACION%TYPE   ;
                        l_codigo_autorizacion                        SIG.TMP_OUTGOING.CODIGO_AUTORIZACION%TYPE           ;
                        l_codigo_servicio                            SIG.TMP_OUTGOING.CODIGO_SERVICIO%TYPE               ;
                        l_ident_term_acep_tarjeta                    SIG.TMP_OUTGOING.IDENT_TERM_ACEP_TARJETA%TYPE       ;
                        l_cod_ident_acep_tarjeta                     SIG.TMP_OUTGOING.COD_IDENT_ACEP_TARJETA%TYPE        ;
                        l_nombre_ubic_acep_tarjeta                   SIG.TMP_OUTGOING.NOMBRE_UBIC_ACEP_TARJETA%TYPE      ;
                        l_datos_adicionales_1                        SIG.TMP_OUTGOING.DATOS_ADICIONALES_1%TYPE           ;
                        l_cod_moneda_transac                         SIG.TMP_OUTGOING.COD_MONEDA_TRANSAC%TYPE            ;
                        l_cod_moneda_conciliacion                    SIG.TMP_OUTGOING.COD_MONEDA_CONCILIACION%TYPE       ;
                        l_cod_moneda_facturacion                     SIG.TMP_OUTGOING.COD_MONEDA_FACTURACION%TYPE        ;
                        l_montos_adicionales                         SIG.TMP_OUTGOING.MONTOS_ADICIONALES%TYPE            ;
                        l_icc                                        SIG.TMP_OUTGOING.ICC%TYPE                           ;
                        l_datos_adicionales_2                        SIG.TMP_OUTGOING.DATOS_ADICIONALES_2%TYPE           ;
                        l_ident_ciclo_duracion_transac               SIG.TMP_OUTGOING.IDENT_CICLO_DURACION_TRANSAC%TYPE  ;
                        l_nro_mensaje                                SIG.TMP_OUTGOING.NRO_MENSAJE%TYPE                   ;
                        l_registro_de_datos                          SIG.TMP_OUTGOING.REGISTRO_DE_DATOS%TYPE             ;
                        l_fecha_accion                               SIG.TMP_OUTGOING.FECHA_ACCION%TYPE                  ;
                        l_cod_ident_inst_dest_transac                SIG.TMP_OUTGOING.COD_IDENT_INST_DEST_TRANSAC%TYPE   ;
                        l_cod_ident_inst_orig_transac                SIG.TMP_OUTGOING.COD_IDENT_INST_ORIG_TRANSAC%TYPE   ;
                        l_datos_ref_emisor                           SIG.TMP_OUTGOING.DATOS_REF_EMISOR%TYPE              ;
                        l_cod_ident_inst_recibe                      SIG.TMP_OUTGOING.COD_IDENT_INST_RECIBE%TYPE         ;
                        l_monto_rec_x_cnv_mon                        SIG.TMP_OUTGOING.MONTO_RECARGO_X_CONVER_MONEDA%TYPE ;
                        l_datos_adicionales_3                        SIG.TMP_OUTGOING.DATOS_ADICIONALES_3%TYPE           ;
                        l_datos_adicionales_4                        SIG.TMP_OUTGOING.DATOS_ADICIONALES_4%TYPE           ;
                        l_datos_adicionales_5                        SIG.TMP_OUTGOING.DATOS_ADICIONALES_5%TYPE           ;
                        out_fecha_cab                                varchar(6);
                        vmonto_original_aux                          VARCHAR2(24):='';
                        vmonto_transac_aux                           VARCHAR2(12):='';
                        vmonto_conciliacion_aux                      VARCHAR2(12):='';
                        l_acc_trans_aux                              NUMBER(8);

     BEGIN

                   l_ind_Ok_Ko:=0;
                   l_contador:=1;
                   l_suma_footer:=0;


     --INCLUIR VALOR 2 EN PDS_0191
       -- l_P0191 := '2';     
					--CURSOR DE LAS TRANSACCIONES Q SE VAN A OUTGOING
                   OPEN  voCursor FOR
                   SELECT  /*+ INDEX (TBL_GESTION_TRANSAC INDEX_ESTADO_PROCESO)*/
                            TGT.SID,
                            TGT.ACCION_TRANSAC,
                            TGT.MIT,
                            TGT.NRO_TARJETA,
                            TGT.CODIGO_PROCESAMIENTO,
                            TGT.MONTO_TRANSAC,
                            TGT.MONTO_CONCILIACION,
                            TGT.MONTO_FACTURACION,
                            TGT.TASA_CONVENIO_CONCILIACION,
                            TGT.TASA_CONVENIO_FACTURACION,
                            TO_CHAR(TGT.FECHA_HR_TRASAC,'YYMMDDHH24MISS'),
                            TGT.FECHA_VENCIMIENTO_TARJETA,
                            TGT.COD_DATOS_PUNTO_SERVICIO,
                            TGT.NRO_SECUENCIA_TARJETA,
                            TGT.CODIGO_FUNCION,
                            TGT.COD_MOTIVO_MENSAJE,
                            TGT.MCC,
                            TGT.MONTOS_ORIGINALES,
                            TGT.DATOS_REFERENCIA_ADQUIRENTE,
                            TGT.COD_IDENTIFICACION_ADQUIRENTE,
                            TGT.COD_IDENT_INST_ENVIA,
                            TGT.NRO_REFERENCIA_RECUPERACION,
                            TGT.CODIGO_AUTORIZACION,
                            TGT.CODIGO_SERVICIO,
                            TGT.IDENT_TERM_ACEP_TARJETA,
                            TGT.COD_IDENT_ACEP_TARJETA,
                            TGT.NOMBRE_UBIC_ACEP_TARJETA,
                            TGT.DATOS_ADICIONALES_1,
                            TGT.COD_MONEDA_TRANSAC,
                            TGT.COD_MONEDA_CONCILIACION,
                            TGT.COD_MONEDA_FACTURACION,
                            TGT.MONTOS_ADICIONALES,
                            TGT.ICC,
                            TGT.DATOS_ADICIONALES_2,
                            TGT.IDENT_CICLO_DURACION_TRANSAC,
                            TGT.NRO_MENSAJE,
                            TGT.REGISTRO_DE_DATOS,
                            TGT.FECHA_ACCION,
                            TGT.COD_IDENT_INST_DEST_TRANSAC,
                            TGT.COD_IDENT_INST_ORIG_TRANSAC,
                            TGT.DATOS_REF_EMISOR,
                            TGT.COD_IDENT_INST_RECIBE,
                            TGT.MONTO_RECARGO_X_CONVER_MONEDA,
                            TGT.DATOS_ADICIONALES_3,
                            TGT.DATOS_ADICIONALES_4,
                            TGT.DATOS_ADICIONALES_5
                   FROM SIG.TBL_GESTION_TRANSAC TGT
                   JOIN SIG.TBL_PDS TP ON (TGT.transaccion = TP.transaccion)
				    JOIN SIG.TBL_INCOMING TI ON (TGT.transaccion = TI.SID)
				  LEFT JOIN SIG.TBL_OUTGOING TBO ON (TGT.SID = TBO.SID)
                    WHERE TGT.ESTADO_PROCESO IN (SELECT SID  FROM SIG.TBL_ESTADO_PROCESO  WHERE XKEY = 'OU_PEN')  AND TBO.SID IS NULL;
                    --AND TI.OPERADOR = operador;
					--AND TGT.COD_MONEDA_TRANSAC = '152';--SE AGREGA EN DURO EL CODIGO DE MONEDA NACIONAL
                    prfCursor:=voCursor;

			   --limpiar la tabla antes de comenzar a insertar por si quedo basura de algun proceso anterior
          execute immediate 'truncate table SIG.TMP_OUTGOING';

			   LOOP
                    FETCH prfCursor

               INTO     l_sid,
                        l_acc_trans_aux,
                        l_mit ,
                        l_nro_tarjeta ,
                        l_codigo_procesamiento ,
                        l_monto_transac ,
                        l_monto_conciliacion ,
                        l_monto_facturacion ,
                        l_tasa_convenio_conciliacion ,
                        l_tasa_convenio_facturacion ,
                        l_fecha_hr_trasac ,
                        l_fecha_vencimiento_tarjeta ,
                        l_cod_datos_punto_servicio ,
                        l_nro_secuencia_tarjeta ,
                        l_codigo_funcion ,
                        l_cod_motivo_mensaje ,
                        l_mcc ,
                        l_montos_originales ,
                        l_datos_referencia_adquirente ,
                        l_cod_ident_adq ,
                        l_cod_ident_inst_envia ,
                        l_nro_referencia_recuperacion ,
                        l_codigo_autorizacion ,
                        l_codigo_servicio ,
                        l_ident_term_acep_tarjeta ,
                        l_cod_ident_acep_tarjeta ,
                        l_nombre_ubic_acep_tarjeta ,
                        l_datos_adicionales_1 ,
                        l_cod_moneda_transac ,
                        l_cod_moneda_conciliacion ,
                        l_cod_moneda_facturacion ,
                        l_montos_adicionales ,
                        l_icc ,
                        l_datos_adicionales_2 ,
                        l_ident_ciclo_duracion_transac ,
                        l_nro_mensaje ,
                        l_registro_de_datos ,
                        l_fecha_accion ,
                        l_cod_ident_inst_dest_transac ,
                        l_cod_ident_inst_orig_transac ,
                        l_datos_ref_emisor ,
                        l_cod_ident_inst_recibe ,
                        l_monto_rec_x_cnv_mon ,
                        l_datos_adicionales_3 ,
                        l_datos_adicionales_4 ,
                        l_datos_adicionales_5;

             EXIT WHEN prfCursor%NOTFOUND;
            --insercion en TMP_OUTGOING


      --INCLUIR VALOR 61000000 EN DE09 -> CONTRA-CARGOS Y COBROS DE CARGO
      --   IF l_mit = '1442' OR l_mit = '1740' OR l_mit = '15' THEN
      --      l_tasa_convenio_conciliacion := '61000000';
      --   END IF;


      --INCLUIR VALOR 152000 EN PDS_0149 -> CONTRA-CARGOS, PV Y COBROS DE CARGO
      --   IF l_mit = '1442' OR l_mit = '1644' OR l_mit = '1740' OR l_mit = '15' OR l_mit = '52' THEN
      --     l_P0149 := '152000';
      --   ELSE
      --      l_P0149 := '';
      --   END IF;


      --INCLUIR VALOR 1 EN PDS_0262 -> CONTRA-CARGOS
      --   IF l_mit = '1442' OR l_mit = '15'  THEN
      --      l_P0262 := '1';
      --   ELSE
      --      l_P0262 := '';
     --    END IF;

    --variables creadas para outgoing

        --Corresponde a DE71 - secuencial del archivo outgoing
        l_contador:=l_contador+1;

       --corresponde a la suma de los montos de transaccion DE04 para el footer
       -- l_suma_footer:=l_suma_footer+l_monto_transac;

      --INSERCION EN TABLA TBL_OUTGOING PARA PREPARACION DE SALIDA OUTGOING SID ESTADO 1 PENDIENTE DE SALIDA
        INSERT INTO SIG.TBL_OUTGOING (SID,
                                          ESTADO)
                                   VALUES ( l_sid ,
                                              1  );
		UPDATE SIG.TBL_GESTION_TRANSAC TGT SET TGT.ESTADO_PROCESO = (SELECT SID  FROM SIG.TBL_ESTADO_PROCESO  WHERE XKEY = 'OU_OK') WHERE TGT.SID = l_sid;
       --Conversiones
            SELECT LPAD(l_monto_transac,12, '0') INTO vmonto_transac_aux FROM DUAL;
            SELECT LPAD(l_monto_conciliacion,12, '0') INTO vmonto_conciliacion_aux FROM DUAL;

       --Ajuste de Valores salida
            l_monto_transac      := vmonto_transac_aux;
            l_monto_conciliacion := vmonto_conciliacion_aux;


       --variables cabecera
            vmonto_original_aux := (vmonto_transac_aux || vmonto_conciliacion_aux);
            l_montos_originales := vmonto_original_aux;


            INSERT INTO  SIG.TMP_OUTGOING (SID,
                                                MIT,
                                                NRO_TARJETA,
                                                CODIGO_PROCESAMIENTO,
                                                MONTO_TRANSAC,
                                                MONTO_CONCILIACION,
                                                MONTO_FACTURACION,
                                                TASA_CONVENIO_CONCILIACION,
                                                TASA_CONVENIO_FACTURACION,
                                                FECHA_HR_TRASAC,
                                                FECHA_VENCIMIENTO_TARJETA,
                                                COD_DATOS_PUNTO_SERVICIO,
                                                NRO_SECUENCIA_TARJETA,
                                                CODIGO_FUNCION,
                                                COD_MOTIVO_MENSAJE,
                                                MCC,
                                                MONTOS_ORIGINALES,
                                                DATOS_REFERENCIA_ADQUIRENTE,
                                                COD_IDENTIFICACION_ADQUIRENTE,
                                                COD_IDENT_INST_ENVIA,
                                                NRO_REFERENCIA_RECUPERACION,
                                                CODIGO_AUTORIZACION,
                                                CODIGO_SERVICIO,
                                                IDENT_TERM_ACEP_TARJETA,
                                                COD_IDENT_ACEP_TARJETA,
                                                NOMBRE_UBIC_ACEP_TARJETA,
                                                DATOS_ADICIONALES_1,
                                                COD_MONEDA_TRANSAC,
                                                COD_MONEDA_CONCILIACION,
                                                COD_MONEDA_FACTURACION,
                                                MONTOS_ADICIONALES,
                                                ICC,
                                                DATOS_ADICIONALES_2,
                                                IDENT_CICLO_DURACION_TRANSAC,
                                                NRO_MENSAJE,
                                                REGISTRO_DE_DATOS,
                                                FECHA_ACCION,
                                                COD_IDENT_INST_DEST_TRANSAC,
                                                COD_IDENT_INST_ORIG_TRANSAC,
                                                DATOS_REF_EMISOR,
                                                COD_IDENT_INST_RECIBE,
                                                MONTO_RECARGO_X_CONVER_MONEDA,
                                                DATOS_ADICIONALES_3,
                                                DATOS_ADICIONALES_4,
                                                DATOS_ADICIONALES_5)
                                      VALUES ( SEQ_TMP_OUTGOING_TBK.NEXTVAL,
                                                l_mit ,
                                                l_nro_tarjeta ,
                                                l_codigo_procesamiento ,
                                                l_monto_transac ,
                                                l_monto_conciliacion ,
                                                l_monto_facturacion ,
                                                l_tasa_convenio_conciliacion ,
                                                l_tasa_convenio_facturacion ,
                                                l_fecha_hr_trasac ,
                                                l_fecha_vencimiento_tarjeta ,
                                                l_cod_datos_punto_servicio ,
                                                l_nro_secuencia_tarjeta ,
                                                l_codigo_funcion ,
                                                l_cod_motivo_mensaje ,
                                                l_mcc ,
                                                l_montos_originales ,
                                                l_datos_referencia_adquirente ,
                                                l_cod_ident_adq ,
                                                l_cod_ident_inst_envia ,
                                                l_nro_referencia_recuperacion ,
                                                l_codigo_autorizacion ,
                                                l_codigo_servicio ,
                                                l_ident_term_acep_tarjeta ,
                                                l_cod_ident_acep_tarjeta ,
                                                l_nombre_ubic_acep_tarjeta ,
                                                l_datos_adicionales_1 ,
                                                l_cod_moneda_transac ,
                                                l_cod_moneda_conciliacion ,
                                                l_cod_moneda_facturacion ,
                                                l_montos_adicionales ,
                                                l_icc ,
                                                l_datos_adicionales_2 ,
                                                l_ident_ciclo_duracion_transac ,
                                                l_contador ,  ---sucuencia de insercion
                                                l_registro_de_datos ,
                                                l_fecha_accion ,
                                                l_cod_ident_inst_dest_transac ,
                                                l_cod_ident_inst_orig_transac ,
                                                l_datos_ref_emisor ,
                                                l_cod_ident_inst_recibe ,
                                                l_monto_rec_x_cnv_mon ,
                                                l_datos_adicionales_3 ,
                                                l_datos_adicionales_4 ,
                                                l_datos_adicionales_5);





  END LOOP;

 END;
 DBMS_OUTPUT.PUT_LINE('salida loop');
    CLOSE prfCursor;

 DBMS_OUTPUT.PUT_LINE('variable de retorno -->'||l_ind_Ok_Ko);
 COMMIT;

EXCEPTION
        WHEN NO_DATA THEN
             warning:=SQLERRM;
             cod_error:=SQLCODE;
             --indicador outgoing no se encuentran archivos de salida outgoing
             l_ind_Ok_Ko:= 1;
              DBMS_OUTPUT.PUT_LINE('excepcion NO_DATA_FOUND -->'||l_ind_Ok_Ko);
        WHEN OTHERS THEN
             --variables de errores
             warning:=SQLERRM;
             cod_error:=SQLCODE;
             --indicador de salido no ok en caso de algun error en la ejecucion de query
             l_ind_Ok_Ko:=-1;
             DBMS_OUTPUT.PUT_LINE('excepcion others -->'||l_ind_Ok_Ko);
              DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);
             ROLLBACK;
--insercion en tabla log error en caso de algun error desconocido
INSERT INTO LOG_ERROR (SID,
                       FECHA_INSERCION,
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_OUTGOING_TECNOCOMM',
                        warning||cod_error || DBMS_UTILITY.format_error_backtrace );
                        commit;
END SP_SIG_OUTGOING_TECNOCOMM;