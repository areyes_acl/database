create or replace PROCEDURE       "SP_SIG_BUSCAR_REGISTROS_LOG_PG" ( 
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 25/09/2017
-- Versión : 1.0
-- Descripcion: Procedimiento que realiza la consulta sobre las tablas de log 
-- 
    p_fecha_busqueda    IN VARCHAR2,
    tabla_log           IN VARCHAR2,
    numPagina           IN NUMBER,
    cantReg             IN NUMBER,
    cod_error           OUT NUMBER,
    warning             OUT VARCHAR2,
    prfCursor           OUT Sys_RefCursor,
    totReg              OUT number
   )
IS
    voCursor Sys_RefCursor:=null; 
    sql_stmt VARCHAR2(7000);
    sql_fields VARCHAR2(2000);
    sql_from_relation VARCHAR2(2000);
    sql_where_condition VARCHAR2(2000);
    not_input_data_exception exception;
    v_cant NUMBER;
BEGIN
  DBMS_OUTPUT.PUT_LINE('fecha: '||p_fecha_busqueda);
  DBMS_OUTPUT.PUT_LINE('nombre_tabla: '||tabla_log);
  cod_error := 0;
  warning := 'Se ha ejecutado SP_SIG_BUSCAR_REGISTROS_LOG_PG correctamete.';

  -- validaciones de campos
   IF( tabla_log = '' OR tabla_log is NULL)
    THEN
      raise not_input_data_exception;
  END IF;


  -- busqueda sin fechas
  IF( tabla_log = 'LOG_ERROR') THEN
      sql_fields :=  'SID,FECHA_INSERCION,NOMBRE_SP,MSG_ERROR ';
      sql_from_relation := 'from LOG_ERROR';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA_INSERCION) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';


  ELSIF ( tabla_log = 'TBL_EXCEPT_INCOMING') THEN                             
      sql_fields :='SID, LOG_CARGA, FECHA_INCOMING, MIT, NRO_TARJETA, CODIGO_PROCESAMIENTO, MONTO_TRANSAC, MONTO_CONCILIACION,
                            MONTO_FACTURACION, TASA_CONVENIO_CONCILIACION, TASA_CONVENIO_FACTURACION,
                            FECHA_HR_TRASAC, FECHA_VENCIMIENTO_TARJETA, COD_DATOS_PUNTO_SERVICIO, NRO_SECUENCIA_TARJETA,
                             CODIGO_FUNCION, COD_MOTIVO_MENSAJE, MCC, MONTOS_ORIGINALES, DATOS_REFERENCIA_ADQUIRENTE,
                             COD_IDENTIFICACION_ADQUIRENTE, COD_IDENT_INST_ENVIA, NRO_REFERENCIA_RECUPERACION,
                             CODIGO_AUTORIZACION, CODIGO_SERVICIO, IDENT_TERM_ACEP_TARJETA,
                             COD_IDENT_ACEP_TARJETA, NOMBRE_UBIC_ACEP_TARJETA, DATOS_ADICIONALES_1, COD_MONEDA_TRANSAC,
                             COD_MONEDA_CONCILIACION, COD_MONEDA_FACTURACION, MONTOS_ADICIONALES, ICC, DATOS_ADICIONALES_2,
                             IDENT_CICLO_DURACION_TRANSAC, NRO_MENSAJE, REGISTRO_DE_DATOS, FECHA_ACCION, COD_IDENT_INST_DEST_TRANSAC,
                             COD_IDENT_INST_ORIG_TRANSAC, DATOS_REF_EMISOR, COD_IDENT_INST_RECIBE, MONTO_RECARGO_X_CONVER_MONEDA,
                             DATOS_ADICIONALES_3, DATOS_ADICIONALES_4, DATOS_ADICIONALES_5 ';
      sql_from_relation := 'from tbl_except_incoming';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA_INCOMING) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_BOL') THEN
      sql_fields :=  'SID, FECHA, FILENAME, FILE_FLAG, FILE_TS_DOWNLOAD, FILE_TS_UPLOAD ';
      sql_from_relation := 'from TBL_LOG_BOL';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_CARG_ABO') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_CARG_ABO';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';      

  ELSIF ( tabla_log = 'TBL_LOG_COMISION') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_COMISION';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_CONTABLE') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_CONTABLE';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_CPAGO') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_CPAGO';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_INC') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_INC';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_ONUS') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_ONUS';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_OUT') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_OUT';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_OUT_ONUS') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_OUT_ONUS';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_OUT_VISA') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_OUT_VISA';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_RCH') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_RCH';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_TRANSAC_AUTOR') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_TRANSAC_AUTOR';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_VISA') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_VISA';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_VISA_INT') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_VISA_INT';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')';

  ELSIF ( tabla_log = 'TBL_LOG_OUT_VISA_INT') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_OUT_VISA_INT';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')'; 

  ELSIF ( tabla_log = 'TBL_LOG_AUTOR_SBPAY') THEN
      sql_fields :=  'SID, FECHA, FILE_NAME, FILE_FLAG, FILE_TS ';
      sql_from_relation := 'from TBL_LOG_AUTOR_SBPAY';
      sql_where_condition := chr(10)|| ' WHERE TRUNC(FECHA) = to_date(:FECHA_BUSQUEDA,''DD/MM/YYYY'')'; 
      
  END IF;

      -- QUERY FINAL CONCATENACION
    sql_stmt := ' SELECT * FROM ( SELECT ROWNUM  AS registro,t.* FROM ('
              || ' SELECT ' 
                 || sql_fields
                 || sql_from_relation
                 || sql_where_condition
                 || ') t ) '
                 || chr(10)
                 ||' WHERE registro BETWEEN (:NUM_PAGINA - 1) * :CANT_REG + 1'
                 || ' AND  :NUM_PAGINA * :CANT_REG ';

      -- IMPRIME LA QUERY           
      DBMS_OUTPUT.PUT_LINE (sql_stmt);

      -- EJECUTA LA QUERY DE BUSQUEDA DE REGISTROS
      OPEN voCursor FOR sql_stmt USING p_fecha_busqueda, numPagina, cantReg, numPagina, cantReg;


    -- QUERY PARA CONTAR EL TOTAL DE REGISTROS ENCONTRADOS
    sql_stmt:= 'SELECT COUNT(*)  AS cantidad
                      FROM  (SELECT ROWNUM  AS registro,t.* FROM ( ' 
                            ||' SELECT ' 
                            || sql_fields
                            || sql_from_relation
                            || sql_where_condition || ' ) t) ';

    -- EJECUTA QUERY DE CUENTA DE REGISTROS ENCONTRADOS
    DBMS_OUTPUT.PUT_LINE (sql_stmt);
      EXECUTE IMMEDIATE sql_stmt INTO v_cant USING p_fecha_busqueda ;


      totReg:=v_cant; 
      prfCursor:=voCursor;

    --DBMS_OUTPUT.PUT_LINE ('Salida Correcta');

EXCEPTION

      WHEN not_input_data_exception THEN 
        cod_error := -1;
        warning := 'No se han encontrado parametros para realizar la busqueda';
        DBMS_OUTPUT.PUT_LINE (DBMS_UTILITY.format_error_backtrace);
        DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);


      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No data');
        cod_error := SQLCODE;
        warning := SQLERRM;

        DBMS_OUTPUT.PUT_LINE (DBMS_UTILITY.format_error_backtrace);
        DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);


END SP_SIG_BUSCAR_REGISTROS_LOG_PG;

