create or replace PROCEDURE       "SP_SIG_BUSCAR_CARGOABO_PEND_PG" ( 
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/06/2016
-- Versión : 1.0
-- Descripcion: Consulta los cargos y abonos del sistema segun filtros ingresados                
-- * fecha desde y fecha hasta: rango de fechas por el cual se buscaran los cargos y abonos
-- * estado : 6 - pendiente o 7 - enviado por sol60, o ambos.
-- nro_tarjeta : opcional 
-- *tipo_gestion : cargo , abono o ambos.
-- *p_username = nombre de usuario, para buscar si es gestion o administrador

    p_fechaDesde         IN VARCHAR2,
    p_fechaHasta         IN VARCHAR2,
    p_sid_estado         IN NUMBER,
    p_sid_tipo_gestion   IN NUMBER,
    p_nro_tarjeta        IN VARCHAR2,
    p_sid_user           IN VARCHAR2,
    numPagina            IN NUMBER,
    cantReg              IN NUMBER,
    warning              OUT VARCHAR2,
    cod_error            OUT NUMBER,
    totReg               OUT NUMBER,
    prfCursor            OUT Sys_RefCursor
)
IS
    voCursor Sys_RefCursor:=null;
    sql_stmt VARCHAR2(7000); 
    sql_fields VARCHAR2(2000); 
    sql_from_relation VARCHAR2(2000);
    sql_where_condition VARCHAR2(2000);
    sql_dinamic_condition VARCHAR2(500);
    v_cant NUMBER;
    v_estado    NUMBER;
    v_estado2    NUMBER;
    v_xkey_rol_admin NUMBER;
    v_tipo_user VARCHAR (15);
    v_check_user  NUMBER;
    v_sid_user NUMBER;
    NO_DATA EXCEPTION;

BEGIN
sql_dinamic_condition := '';
 warning := 'Se ha ejecutado SP_SIG_BUSCAR_CARGOABO_PEND, correctamenteS';


    -- CAMPOS PARA TODAS LAS QUERYS
    sql_fields :='CA.SID                                                                          AS SID_CARGO_ABONO,
                  INC.NRO_TARJETA                                                                 AS NRO_TARJETA,
                  TO_CHAR(INC.FECHA_HR_TRASAC, ''DD/MM/YYYY'')                                    AS FECHATRANSAC,
                  REGEXP_SUBSTR(INC.NOMBRE_COMERCIO,''[^\]+'',1,1)                       AS COMERCIO,
                  INC.MONTO_TRANSAC                                                               AS MONTO_ORIG,
                  CA.MONTO_CARGO_ABONO                                                            AS MONTO_CARGO_ABONO,
                  CA.ESTADO_PROCESO                                                               AS ESTADO_CARG_ABO,
                  TR.NUM_INCIDENTE                                                                AS NUM_INCIDENTE,
                  CONCAT(NVL(US.RUT,''''),NVL(US.DV,''''))                                        AS USUARIO,
                  CA.TBL_CARGO_ABONO                                                              AS TIPO,
                  TO_CHAR(CA.FECHA_CARGO_ABONO, ''DD/MM/YYYY hh24:mi:ss'')                        AS FECHA_CARGO_ABONO ';

    -- TABLAS DE LA RELACION    (PARA TODAS LAS QUERYS)
    sql_from_relation := chr(10)||' FROM (SIG.TBL_GESTION_CARGO_ABONO CA 
                          LEFT JOIN TBL_CORE_AUTOR INC ON CA.TRANSACCION = INC.SID)
                          LEFT JOIN TBL_USUARIO_IG US ON US.SID = CA.USUARIO
                          LEFT JOIN TBL_TRANSACCIONES TR ON TR.SID = CA.TRANSACCION ';

    -- CONDICION DE QUERY                
     sql_where_condition := chr(10)||
                                ' WHERE 
                TRUNC(CA.FECHA_CARGO_ABONO) between to_date(:FECHA_DESDE,''DD/MM/YYYY'') AND to_date(:FECHA_HASTA,''DD/MM/YYYY'')' ;

      -- FILTRA POR USUARIO SI ES GESTION, SI ES ADMIN NO FILTRA POR USER
      IF( p_sid_user <>0)
        THEN
          DBMS_OUTPUT.PUT_LINE ('FILTRA USUARIO GESTION ');
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)||' AND  US.SID   =  '''||p_sid_user||''' ';
      END IF;


      -- FILTRA POR NUMERO DE TARJETA
      IF(p_nro_tarjeta <> 0)
        THEN
          DBMS_OUTPUT.PUT_LINE ('FILTRA POR NUMERO DE TARJETA');
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND   INC.NRO_TARJETA      =  '''||p_nro_tarjeta||''' ';
      END IF;


      -- FILTRA POR TIPO GESTION : CARGO O ABONO
      IF(p_sid_tipo_gestion <> 0)
        THEN 
           DBMS_OUTPUT.PUT_LINE ('FILTRA POR TIPO DE GESTION: CARGO/ABONO');
           sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND    CA.TBL_CARGO_ABONO  =  '''||p_sid_tipo_gestion||''' ';
      END IF;


      -- FILTRA POR ESTADO PENDIENTE-ENVIADA DE SOL60
      IF(p_sid_estado <> 0)
        THEN 
           DBMS_OUTPUT.PUT_LINE ('FILTRA POR TIPO DE GESTION: CARGO/ABONO');
           sql_dinamic_condition := sql_dinamic_condition ||chr(10)||' AND   CA.ESTADO_PROCESO   =   '''||p_sid_estado||''' ';
      END IF;


      -- CONCATENA LAS CONDICIONES DINAMICAS A LA CLAUSULA WHERE
      sql_where_condition :=sql_where_condition || sql_dinamic_condition;


    -- QUERY FINAL CONCATENACION
    sql_stmt := ' SELECT * FROM ( SELECT ROWNUM  AS registro,t.* FROM ('
              || ' SELECT ' 
                 || sql_fields
                 || sql_from_relation
                 || sql_where_condition
                 || ') t ) '
                 || chr(10)
                 ||' WHERE registro BETWEEN (:NUM_PAGINA - 1) * :CANT_REG + 1'
                 || ' AND  :NUM_PAGINA * :CANT_REG ';

      -- IMPRIME LA QUERY           
      DBMS_OUTPUT.PUT_LINE (sql_stmt);

      -- EJECUTA LA QUERY DE BUSQUEDA DE REGISTROS
      OPEN voCursor FOR sql_stmt USING p_fechaDesde, p_fechaHasta, numPagina, cantReg, numPagina, cantReg;


        -- QUERY PARA CONTAR EL TOTAL DE PAGINAS 
        sql_stmt:= 'SELECT COUNT(*)  AS cantidad
                      FROM  (SELECT ROWNUM  AS registro,t.* FROM ( ' 
                            ||' SELECT ' 
                            || sql_fields
                            || sql_from_relation
                            || sql_where_condition
                            ||' order by FECHATRANSAC ASC ) t) ';

      DBMS_OUTPUT.PUT_LINE (chr(10)||'PAGINACION :' ||sql_stmt );                    

      -- EJECUTA QUERY DE PAGINACION
      EXECUTE IMMEDIATE sql_stmt INTO v_cant USING p_fechaDesde, p_fechaHasta ;

   prfCursor:=voCursor;
   totReg:=v_cant;


 EXCEPTION
     WHEN NO_DATA_FOUND THEN
        warning:='NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS: ' ||SQLERRM;    
        cod_error:=SQLCODE;  
        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || warning);

     WHEN OTHERS THEN
       DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);
       warning:=SQLERRM;    
       cod_error:=SQLCODE;  

    INSERT INTO LOG_ERROR (SID, 
                       FECHA_INSERCION, 
                       NOMBRE_SP, 
                       MSG_ERROR) 
               VALUES ( SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_BUSCAR_CARGOABO_PEND',
                        warning||cod_error );
                        commit;

END SP_SIG_BUSCAR_CARGOABO_PEND_PG;

