create or replace PROCEDURE SP_SIG_CLEAN_TMP_CARGO_ABONO
( 
warning   OUT VARCHAR2,
cod_error OUT NUMBER 
)
IS
  vocursor  SYS_REFCURSOR := NULL;
  prfcursor SYS_REFCURSOR := NULL;
BEGIN
    DECLARE
    BEGIN
	--tabla SIG TMP_CONTABLE SE LIMPIA    
    EXECUTE IMMEDIATE 'truncate table SIG.TMP_SALIDA_CARGO_ABONO';

    COMMIT;
END;
EXCEPTION
  WHEN OTHERS THEN                                                                      
                cod_error:=SQLCODE;
                warning:=SQLERRM;
                DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);                                                            
                DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);

                INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CLEAN_TMP_CONTABLE', cod_error || '-' || warning);
                COMMIT; 

END;