  CREATE TABLE "SIG"."TBL_LOG_PREUNIC" 
   (	"SID" NUMBER NOT NULL ENABLE, 
	"FILE_TS" TIMESTAMP (6), 
	"FILE_NAME" VARCHAR2(50 BYTE), 
	"FILE_FLAG" VARCHAR2(2 BYTE), 
	"FECHA" DATE, 
	 CONSTRAINT "TBL_LOG_PREUNIC_PK" PRIMARY KEY ("SID") ENABLE
   );

   COMMENT ON COLUMN "SIG"."TBL_LOG_PREUNIC"."SID" IS 'Corresponde al id de la tabla TBL_LOG_PREUNIC';
   COMMENT ON COLUMN "SIG"."TBL_LOG_PREUNIC"."FILE_TS" IS 'Corresponde a la estampa de tiempo del proceso del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_PREUNIC"."FILE_NAME" IS 'Corresponde al nombre del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_PREUNIC"."FILE_FLAG" IS 'Corresponde al flag de procesamiento creado 0:subido 1:-1:error';
   COMMENT ON COLUMN "SIG"."TBL_LOG_PREUNIC"."FECHA" IS 'Corresponde a la fecha de Procesamiento del archivo';
COMMIT;