create or replace PROCEDURE       "SP_SIG_TRANSACOBJRECGESTTRANPG" (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/07/2013
-- Versión : 1.0
-- Descripcion: Consulta las transacciones de presentacion de acuerdo a los parametros de entrada.
-- Modificaciones: ACH - Se agregó parametros para paginación por SP.
    fechaInicio         IN VARCHAR2,
    fechaTermino        IN VARCHAR2,
    xkeyAtr             IN VARCHAR2,
    numPagina           IN NUMBER,
    cantReg             IN NUMBER,
    warning             OUT VARCHAR2,
    cod_error           OUT NUMBER,
    prfCursor           OUT Sys_RefCursor,
    totReg              OUT number
)
IS
    voCursor Sys_RefCursor:=null;

    v_var1 varchar2(10);
    v_var2 varchar2(10);
    v_cant NUMBER;
    NO_DATA EXCEPTION;

BEGIN
  DBMS_OUTPUT.PUT_LINE('Fecha inicio: '||fechaInicio);
  DBMS_OUTPUT.PUT_LINE('Fecha Fin: '||fechaTermino);
  DBMS_OUTPUT.PUT_LINE('xkeyAtr: '||xkeyAtr); 

  if xkeyAtr <> 'OUT' and xkeyAtr <> 'INC' then
    DBMS_OUTPUT.PUT_LINE ('FLAG');
    --Cursos
    OPEN voCursor FOR    
      SELECT * FROM (
        select
          ROW_NUMBER() OVER (ORDER BY T.FECHA_HR_TRASAC ASC) registro,
          --
          T.MIT,
          T.CODIGO_FUNCION,
          T.NRO_TARJETA,
          TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                AS FECHATRANSAC,
          REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                        AS COMERCIOPAIS,
         TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
          TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
          NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
          NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
          T.CODIGO_AUTORIZACION,
          TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
          NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
          NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
          NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
          UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
          NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
          NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
          NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
          NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
          NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
          NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
          T.SID                                                               AS SID,
          T.GLOSA_GENERAL                                                     AS GLOSAGENERAL,
          NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
          TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
         NVL(P.P0023, '-')                                                AS  PATPASS,
         I.OPERADOR                                                        AS IDOPERADOR,
         TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))      AS OPERADOR
        from SIG.TBL_GESTION_TRANSAC T LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.TRANSACCION = P.TRANSACCION), SIG.TBL_INCOMING I, SIG.TBL_ESTADO_TRANSAC E    where
                --T.FECHA_HR_TRASAC between to_date(fechaInicio,'DD/MM/YYYY') and to_date(fechaTermino,'DD/MM/YYYY') +  (1-1/86399) and T.ACCION_TRANSAC =
                T.FECHA_INSERCION between to_date(fechaInicio,'DD/MM/YYYY') and to_date(fechaTermino,'DD/MM/YYYY') +  (1-1/86399) and T.ACCION_TRANSAC =
                (select ATR.SID from SIG.TBL_ACCION_TRANSAC ATR where ATR.XKEY = xkeyAtr) and
                T.ESTADO_TRANSAC= E.SID and T.TRANSACCION = I.SID


       )
      WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
      AND  numPagina * cantReg order by     FECHATRANSAC;

      --Cantidad
      SELECT
        count(T.SID) AS cantidad into v_cant
       from SIG.TBL_GESTION_TRANSAC T, SIG.TBL_ESTADO_TRANSAC E where
                T.FECHA_INSERCION between to_date(fechaInicio,'DD/MM/YYYY') and to_date(fechaTermino,'DD/MM/YYYY') +  (1-1/86399) and T.ACCION_TRANSAC =
                (select ATR.SID from SIG.TBL_ACCION_TRANSAC ATR where ATR.XKEY = xkeyAtr) AND
                T.ESTADO_TRANSAC= E.SID;

    else
       DBMS_OUTPUT.PUT_LINE ('Recuperacion combo de cargos');
       BEGIN  
          if xkeyAtr = 'INC' then
              v_var1 := 'ING';
              v_var2 := '';
          else
              v_var1 := 'OU_PEN';
              v_var2 := 'OU_OK';
          end if;

          DBMS_OUTPUT.PUT_LINE ('v_var1: '||v_var1||'  v_var2: '||v_var2);

      --Cursor 
        OPEN voCursor FOR
          SELECT * FROM (
            select
              ROW_NUMBER() OVER (ORDER BY T.FECHA_HR_TRASAC ASC) registro,
              --
              T.MIT,
              T.CODIGO_FUNCION,
              T.NRO_TARJETA,
              TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                AS FECHATRANSAC,
              REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                        AS COMERCIOPAIS,
			  TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC, 
              TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
              NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
              NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
              T.CODIGO_AUTORIZACION,
              TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
              NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
              NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
              NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
              UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
              NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
              NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
              NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
              NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
              NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
              NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
              T.SID                                                               AS SID,
              T.GLOSA_GENERAL                                                     AS GLOSAGENERAL,
              NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
			  TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
         NVL(P.P0023, '-')                                                AS  PATPASS,
         I.OPERADOR                                                        AS IDOPERADOR,
         TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))      AS OPERADOR
         
          from SIG.TBL_GESTION_TRANSAC T  LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.TRANSACCION = P.TRANSACCION), SIG.TBL_INCOMING I, SIG.TBL_ESTADO_TRANSAC E where
                T.FECHA_INSERCION between to_date(fechaInicio,'DD/MM/YYYY') and to_date(fechaTermino,'DD/MM/YYYY') +  (1-1/86399)  and T.ACCION_TRANSAC in
                (select ATR.SID from SIG.TBL_ACCION_TRANSAC ATR where ATR.XKEY = '1740_700' or ATR.XKEY = '1740_780' or ATR.XKEY = '1740_781' or ATR.XKEY = '1740_781') AND
                T.ESTADO_TRANSAC= E.SID and T.TRANSACCION = I.SID AND
                T.ESTADO_PROCESO in (select SID from TBL_ESTADO_PROCESO where XKEY in v_var1 or XKEY = v_var2)

              )
          WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
          AND  numPagina * cantReg;


        SELECT
          count(T.SID) AS cantidad into v_cant
        from SIG.TBL_GESTION_TRANSAC T, SIG.TBL_ESTADO_TRANSAC E where
                T.FECHA_INSERCION between to_date(fechaInicio,'DD/MM/YYYY') and to_date(fechaTermino,'DD/MM/YYYY') +  (1-1/86399)  and T.ACCION_TRANSAC in
                (select ATR.SID from SIG.TBL_ACCION_TRANSAC ATR where ATR.XKEY = '1740_700' or ATR.XKEY = '1740_780' or ATR.XKEY = '1740_781' or ATR.XKEY = '1740_781') AND
                T.ESTADO_TRANSAC= E.SID AND
                T.ESTADO_PROCESO in (select SID from TBL_ESTADO_PROCESO where XKEY in v_var1 or XKEY = v_var2);

          END;
  END IF;

    prfCursor := voCursor;

    DBMS_OUTPUT.PUT_LINE('Cantidad registros: '||v_cant);

    totReg:=v_cant;

    DBMS_OUTPUT.PUT_LINE ('Salida Correcta');
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No data');
        cod_error := SQLCODE;
        warning := SQLERRM;

        DBMS_OUTPUT.PUT_LINE (DBMS_UTILITY.format_error_backtrace);
        DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);

      WHEN NO_DATA THEN
        DBMS_OUTPUT.PUT_LINE ('No data');
        cod_error := 100;
       warning := 'No hay data';

        DBMS_OUTPUT.PUT_LINE (DBMS_UTILITY.format_error_backtrace);
        DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);
END SP_SIG_TRANSACOBJRECGESTTRANPG;