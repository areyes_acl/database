create or replace PROCEDURE       "SP_SIG_CARGA_VCORE_GEST_MASIVO" (
sidConci in NUMBER,
comentario in VARCHAR2,
gestion in NUMBER,
idUser in NUMBER,
warning   OUT VARCHAR2,
cod_error OUT NUMBER
) IS 
contador NUMBER:=0;
BEGIN

  SELECT COUNT(*) INTO contador FROM TBL_CONC_VISA_CORE_GEST WHERE SID_CONC = sidConci AND ESTADO = 1;

      IF(contador = 0)THEN
          INSERT INTO TBL_CONC_VISA_CORE_GEST(SID, SID_CONC, FECHA, SID_USER, COMENTARIO, ESTADO)
          VALUES (SEQ_TBL_CONC_VISA_CORE_GEST.NEXTVAL, sidConci, SYSDATE, idUser, comentario,gestion);
          cod_error := 0;
      ELSE 
            cod_error := 1;
            DBMS_OUTPUT.PUT_LINE('YA EXISTE REGISTRO');
      END IF;
       COMMIT; 
    EXCEPTION 
      WHEN NO_DATA_FOUND     THEN
          cod_error:=SQLCODE;
          warning:=SQLERRM;
          INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CARGA_VCORE_GEST_MASIVO', cod_error || '-' || warning);
          DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
      WHEN OTHERS            THEN
          cod_error:=SQLCODE;
          warning:=SQLERRM;
          INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CARGA_VCORE_GEST_MASIVO', cod_error || '-' || warning);
          DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);

END SP_SIG_CARGA_VCORE_GEST_MASIVO;