create or replace PROCEDURE "SP_SIG_CARGA_ENTRADA_SERVIPAG" (warning OUT VARCHAR2, cod_error OUT NUMBER)
IS   
        voCursor  Sys_RefCursor:=null;
        prfCursor Sys_RefCursor:=null;

-- Proyecto: SIG
-- Nombre Objeto:sp_Sig_CARGA_ENTRADA_SERVIPAG
-- By: Developer || ACL PTM | 14/12/2020
-- Versi�n : 1.1
-- Descripcion:carga TBL_PAGOS_REALIZADOS      desde TMP_ENTRADA_SERVIPAG
-- Modificaci�n ROBERTO CARLOS GONZ�LEZ REYES
-- Descripci�n se agrega carga PDS Internacional.

BEGIN
      DECLARE                                 
           FECHA_PAGO               SIG.TMP_ENTRADA_SERVIPAG.FEC_PAGO%TYPE                  ; 
           HORA_PAGO                SIG.TMP_ENTRADA_SERVIPAG.HORA_PAGO%TYPE                 ; 
           TRANSACCION_ID           SIG.TMP_ENTRADA_SERVIPAG.IDENTIFICADOR%TYPE             ; 
           LOCAL_PAGO               SIG.TMP_ENTRADA_SERVIPAG.ID_TICKET%TYPE                 ; 
           CLIENTE_NUMERO_CUENTA    SIG.TMP_ENTRADA_SERVIPAG.ID_PAPEL%TYPE                  ; 
           ESTRUCTURA_PAGO          SIG.TMP_ENTRADA_SERVIPAG.MEDIO_DE_PAGO%TYPE             ; 
           MONTO_FINAL              SIG.TMP_ENTRADA_SERVIPAG.MONTO_TOTAL%TYPE               ; 
           OFICINA                  SIG.TMP_ENTRADA_SERVIPAG.OFICINA%TYPE                   ; 
           NUM_DOCUMENTO            SIG.TMP_ENTRADA_SERVIPAG.NUM_DOCUMENTO%TYPE             ; 
           RUT_PAGADOR              SIG.TMP_ENTRADA_SERVIPAG.RUT%TYPE                       ; 
           FECHA_CONTABLE           SIG.TMP_ENTRADA_SERVIPAG.FEC_CONTABLE%TYPE              ; 
           OPERADOR                 SIG.TMP_ENTRADA_SERVIPAG.OPERADOR%TYPE                  ;
           SEQ_TBL_PAGOS            SIG.TBL_PAGOS_RECIBIDOS.sid%TYPE := NULL                ;
           CAN_ID                   SIG.TMP_ENTRADA_SERVIPAG.CAN_ID%TYPE                    ;
           ID_PAGO                  SIG.TMP_ENTRADA_SERVIPAG.ID_PAGO%TYPE                   ;
           TIPO_TRANSACCION         SIG.TMP_ENTRADA_SERVIPAG.TIPO_TRANSACCION%TYPE          ;
           CLIENTE_NOMBRE_BANCO      SIG.TMP_ENTRADA_SERVIPAG.BANCO%TYPE                  ; 


     BEGIN

      dbms_output.enable(NULL);

                OPEN voCursor FOR
                SELECT
                        FEC_PAGO, 
                        HORA_PAGO, 
                        IDENTIFICADOR, 
                        ID_TICKET, 
                        ID_PAPEL, 
                        MEDIO_DE_PAGO,
                        MONTO_TOTAL, 
                        OFICINA,
                        NUM_DOCUMENTO,
                        FEC_CONTABLE,                        
                        OPERADOR,
                        CAN_ID,
                        ID_PAGO,
                        TIPO_TRANSACCION,
                        BANCO
                 FROM  SIG.TMP_ENTRADA_SERVIPAG;
                    prfCursor:=voCursor;

        LOOP                                                                                           
                    FETCH prfCursor                                                                              
                    INTO  FECHA_PAGO, 
                          HORA_PAGO, 
                          TRANSACCION_ID, 
                          LOCAL_PAGO, 
                          CLIENTE_NUMERO_CUENTA, 
                          ESTRUCTURA_PAGO, 
                          MONTO_FINAL,
                          OFICINA,
                          NUM_DOCUMENTO,
                          FECHA_CONTABLE,
                          OPERADOR,
                          CAN_ID,
                          ID_PAGO,
                          TIPO_TRANSACCION,
                          CLIENTE_NOMBRE_BANCO;
                    EXIT WHEN prfCursor%NOTFOUND; 
                    
                    SELECT SEQ_TBL_PAGOS_RECIBIDOS.NEXTVAL
                    INTO   SEQ_TBL_PAGOS
                    FROM   DUAL;
                    
                    INSERT INTO SIG.TBL_PAGOS_RECIBIDOS (
                          SID,
                          FECHA_PAGO, 
                          HORA_PAGO, 
                          TRANSACCION_ID, 
                          RUT,
                          LOCAL_PAGO, 
                          CLIENTE_NUMERO_CUENTA, 
                          ESTRUCTURA_PAGO, 
                          MONTO_FINAL,
                          OFICINA,
                          NUM_DOCUMENTO,
                          FECHA_CONTABLE,
                          OPERADOR,
                          CAN_ID,
                          ID_PAGO,
                          TIPO_TRANSACCION,
                          CLIENTE_NOMBRE_BANCO
                    ) VALUES ( 
                          SEQ_TBL_PAGOS,  
                          FECHA_PAGO, 
                          HORA_PAGO, 
                          TRANSACCION_ID, 
                          TRANSACCION_ID,
                          LOCAL_PAGO, 
                          CLIENTE_NUMERO_CUENTA, 
                          ESTRUCTURA_PAGO, 
                          MONTO_FINAL,
                          OFICINA,
                          NUM_DOCUMENTO,
                          FECHA_CONTABLE, 
                          OPERADOR,
                          CAN_ID,
                          ID_PAGO,
                          TIPO_TRANSACCION,
                          CLIENTE_NOMBRE_BANCO
                    );

                COMMIT;

              --SOLO PRUEBAS
           --DBMS_OUTPUT.PUT_LINE('salida fetch');

              --FIN PRUEBAS 
                END LOOP;
                --DBMS_OUTPUT.PUT_LINE('salida loop');
                
               -- DELETE FROM TMP_ENTRADA_SERVIPAG;
                
                CLOSE prfCursor;
               -- DBMS_OUTPUT.PUT_LINE('cierre cursor');
     END;            

  --tabla SIG.TMP_INCOMING SE LIMPIA
 execute immediate 'truncate table SIG.TMP_ENTRADA_SERVIPAG';
 COMMIT;
EXCEPTION
WHEN  NO_DATA_FOUND     THEN
            DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS EN TABLA SIG.TMP_ENTRADA_SERVIPAG');
WHEN  OTHERS THEN
      warning:=SQLERRM;   
      cod_error:=SQLCODE;
      DBMS_OUTPUT.PUT_LINE('ERROR : '||warning||' SQLCODE : '||cod_error);   
--insercion en tabla log error en caso de algun error desconocido          
     INSERT INTO LOG_ERROR (SID, 
                       FECHA_INSERCION, 
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_CARGA_ENTRADA_SERVIPAG',
                        cod_error||warning );
END SP_SIG_CARGA_ENTRADA_SERVIPAG;