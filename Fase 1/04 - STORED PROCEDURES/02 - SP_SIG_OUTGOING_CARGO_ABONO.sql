CREATE OR REPLACE PROCEDURE SP_SIG_OUTGOING_CARGO_ABONO
(
  warning             OUT VARCHAR2,
  cod_error           OUT NUMBER,
  prfCursor           OUT SYS_REFCURSOR
)
AS
  voCursor SYS_REFCURSOR:=null;
BEGIN
OPEN voCursor FOR
  SELECT 
  TIPO_REGISTRO						     AS TIPOREGISTRO,                
  COD_EMISOR                             AS CODEMISOR,                 
  TIPO_SOLICITUD                         AS TIPOSOLICITUD,     
  COD_TARJETA                            AS CODTARJETA,   
  DESCRIPCION_TBL_CARGOS_ABONOS          AS DESCRIPCIONTBLCARGOSABONOS,   
  NRO_CUOTAS                             AS NROCUOTAS,  
  MONTO_TRANSAC_CARGO_ABONO              AS MONTOTRANSACCARGOABONO,   
  FECHA_EFECTIVA                         AS FECHAEFECTIVA,               
  SUCURSAL_RECAUDADORA                   AS SUCURSALRECAUDADORA,               
  TRANSACTION_CODE                       AS TRANSACTIONCODE,            
  HORA_TRANSACCION                       AS HORATRANSACCION,
  MERCHANT_NUMBER                        AS MERCHANTNUMBER,
  PAYMENT_MODE                           AS PAYMENTMODE,
  PAYMENT_DETAILS                        AS PAYMENTDETAILS,
  TRANSACTION_CURRENCY                   AS TRANSACTIONCURRENCY,
  TASA_INTERES                           AS TASAINTERES,
  FILTER1                                AS FILTER1,
  FILTER2                                AS FILTER2,
  PAGO_DIFERIDO                          AS PAGODIFERIDO,
  TIPO_MONEDA                            AS TIPOMONEDA
  FROM TMP_SALIDA_CARGO_ABONO;
prfCursor:=voCursor;
EXCEPTION                                                                               
  WHEN OTHERS THEN                                                                      
  DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);                                                            
  DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);     
  warning:=SQLERRM;    
  cod_error:=SQLCODE;  
  INSERT INTO LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) 
  VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_OUTGOING_CARGO_ABONO', warning||cod_error );
  commit;
END;