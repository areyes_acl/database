create or replace PROCEDURE       "SP_SIG_CARGAINCOMING" (warning       OUT VARCHAR2,
                                             cod_error      OUT NUMBER
                                                       
)
IS
   
        voCursor  Sys_RefCursor:=null;
        prfCursor Sys_RefCursor:=null;

-- Proyecto: SIG
-- Nombre Objeto:sp_Sig_CargaIncoming
-- By: Developer || ACL PTM | 10/07/2013
-- Versión : 1.1
-- Descripcion:carga tbl_incoming      desde tmp_incoming
-- Modificación Alejandro Trigo
-- Descripción se agrega carga PDS Internacional.

BEGIN
       DBMS_OUTPUT.PUT_LINE('salida fetch1');
      DECLARE
           MIT                                       SIG.TMP_INCOMING.MIT%TYPE                           ; 
           NRO_TARJETA                               SIG.TMP_INCOMING.NRO_TARJETA%TYPE                   ; 
           CODIGO_PROCESAMIENTO                      SIG.TMP_INCOMING.CODIGO_PROCESAMIENTO%TYPE          ; 
           MONTO_TRANSAC                             SIG.TMP_INCOMING.MONTO_TRANSAC%TYPE                 ; 
           MONTO_CONCILIACION                        SIG.TMP_INCOMING.MONTO_CONCILIACION%TYPE            ; 
           MONTO_FACTURACION                         SIG.TMP_INCOMING.MONTO_FACTURACION%TYPE             ; 
           TASA_CONVENIO_CONCILIACION                SIG.TMP_INCOMING.TASA_CONVENIO_CONCILIACION%TYPE    ; 
           TASA_CONVENIO_FACTURACION                 SIG.TMP_INCOMING.TASA_CONVENIO_FACTURACION%TYPE     ; 
           FECHA_HR_TRASAC                           SIG.TMP_INCOMING.FECHA_HR_TRASAC%TYPE               ; 
           FECHA_VENCIMIENTO_TARJETA                 SIG.TMP_INCOMING.FECHA_VENCIMIENTO_TARJETA%TYPE     ; 
           COD_DATOS_PUNTO_SERVICIO                  SIG.TMP_INCOMING.COD_DATOS_PUNTO_SERVICIO%TYPE      ; 
           NRO_SECUENCIA_TARJETA                     SIG.TMP_INCOMING.NRO_SECUENCIA_TARJETA%TYPE         ; 
           CODIGO_FUNCION                            SIG.TMP_INCOMING.CODIGO_FUNCION%TYPE                ; 
           COD_MOTIVO_MENSAJE                        SIG.TMP_INCOMING.COD_MOTIVO_MENSAJE%TYPE            ;  
           MCC                                       SIG.TMP_INCOMING.MCC%TYPE                           ; 
           MONTOS_ORIGINALES                         SIG.TMP_INCOMING.MONTOS_ORIGINALES%TYPE             ; 
           DATOS_REFERENCIA_ADQUIRENTE               SIG.TMP_INCOMING.DATOS_REFERENCIA_ADQUIRENTE%TYPE   ; 
           COD_IDENTIFICACION_ADQUIRENTE             SIG.TMP_INCOMING.COD_IDENTIFICACION_ADQUIRENTE%TYPE ; 
           COD_IDENT_INST_ENVIA                      SIG.TMP_INCOMING.COD_IDENT_INST_ENVIA%TYPE          ; 
           NRO_REFERENCIA_RECUPERACION               SIG.TMP_INCOMING.NRO_REFERENCIA_RECUPERACION%TYPE   ; 
           CODIGO_AUTORIZACION                       SIG.TMP_INCOMING.CODIGO_AUTORIZACION%TYPE           ; 
           CODIGO_SERVICIO                           SIG.TMP_INCOMING.CODIGO_SERVICIO%TYPE               ; 
           IDENT_TERM_ACEP_TARJETA                   SIG.TMP_INCOMING.IDENT_TERM_ACEP_TARJETA%TYPE       ; 
           COD_IDENT_ACEP_TARJETA                    SIG.TMP_INCOMING.COD_IDENT_ACEP_TARJETA%TYPE        ; 
           NOMBRE_UBIC_ACEP_TARJETA                  SIG.TMP_INCOMING.NOMBRE_UBIC_ACEP_TARJETA%TYPE      ; 
           DATOS_ADICIONALES_1                       SIG.TMP_INCOMING.DATOS_ADICIONALES_1%TYPE           ; 
           COD_MONEDA_TRANSAC                        SIG.TMP_INCOMING.COD_MONEDA_TRANSAC%TYPE            ; 
           COD_MONEDA_CONCILIACION                   SIG.TMP_INCOMING.COD_MONEDA_CONCILIACION%TYPE       ; 
           COD_MONEDA_FACTURACION                    SIG.TMP_INCOMING.COD_MONEDA_FACTURACION%TYPE        ; 
           MONTOS_ADICIONALES                        SIG.TMP_INCOMING.MONTOS_ADICIONALES%TYPE            ; 
           ICC                                       SIG.TMP_INCOMING.ICC%TYPE                           ; 
           DATOS_ADICIONALES_2                       SIG.TMP_INCOMING.DATOS_ADICIONALES_2%TYPE           ; 
           IDENT_CICLO_DURACION_TRANSAC              SIG.TMP_INCOMING.IDENT_CICLO_DURACION_TRANSAC%TYPE  ; 
           NRO_MENSAJE                               SIG.TMP_INCOMING.NRO_MENSAJE%TYPE                   ; 
           REGISTRO_DE_DATOS                         SIG.TMP_INCOMING.REGISTRO_DE_DATOS%TYPE             ; 
           FECHA_ACCION                              SIG.TMP_INCOMING.FECHA_ACCION%TYPE                  ; 
           COD_IDENT_INST_DEST_TRANSAC               SIG.TMP_INCOMING.COD_IDENT_INST_DEST_TRANSAC%TYPE   ; 
           COD_IDENT_INST_ORIG_TRANSAC               SIG.TMP_INCOMING.COD_IDENT_INST_ORIG_TRANSAC%TYPE   ; 
           DATOS_REF_EMISOR                          SIG.TMP_INCOMING.DATOS_REF_EMISOR%TYPE              ; 
           COD_IDENT_INST_RECIBE                     SIG.TMP_INCOMING.COD_IDENT_INST_RECIBE%TYPE         ; 
           MONTO_RECARGO_X_CONVER_MONEDA             SIG.TMP_INCOMING.MONTO_RECARGO_X_CONVER_MONEDA%TYPE ; 
           DATOS_ADICIONALES_3                       SIG.TMP_INCOMING.DATOS_ADICIONALES_3%TYPE           ; 
           DATOS_ADICIONALES_4                       SIG.TMP_INCOMING.DATOS_ADICIONALES_4%TYPE           ; 
           DATOS_ADICIONALES_5                       SIG.TMP_INCOMING.DATOS_ADICIONALES_5%TYPE           ; 
           DATOS_ADICIONALES_6                      SIG.TMP_INCOMING.DATOS_ADICIONALES_6%TYPE           ; 
		   DATOS_ADICIONALES_7                      SIG.TMP_INCOMING.DATOS_ADICIONALES_7%TYPE           ; 
		   DATOS_ADICIONALES_8                      SIG.TMP_INCOMING.DATOS_ADICIONALES_8%TYPE           ; 
           P0005                                        SIG.TMP_INCOMING.P0005%TYPE                         ; 
           P0023                                        SIG.TMP_INCOMING.P0023%TYPE                         ;  
           P0025                                        SIG.TMP_INCOMING.P0025%TYPE                         ;
           P0026                                        SIG.TMP_INCOMING.P0026%TYPE                         ;
           P0044                                        SIG.TMP_INCOMING.P0044%TYPE                         ;
           P0052                                        SIG.TMP_INCOMING.P0052%TYPE                         ; 
           P0057                                        SIG.TMP_INCOMING.P0057%TYPE                         ;
           P0071                                        SIG.TMP_INCOMING.P0071%TYPE                         ;
           P0105                                        SIG.TMP_INCOMING.P0105%TYPE                         ;
           P0122                                        SIG.TMP_INCOMING.P0122%TYPE                         ;
           P0137                                        SIG.TMP_INCOMING.P0137%TYPE                         ;
           P0146                                        SIG.TMP_INCOMING.P0146%TYPE                         ;
           P0148                                        SIG.TMP_INCOMING.P0148%TYPE                         ;
           P0149                                        SIG.TMP_INCOMING.P0149%TYPE                         ;
           P0158                                        SIG.TMP_INCOMING.P0158%TYPE                         ; 
            P0159                                        SIG.TMP_INCOMING.P0159%TYPE                         ; 
           P0165                                        SIG.TMP_INCOMING.P0165%TYPE                         ; 
           P0170                                        SIG.TMP_INCOMING.P0170%TYPE                         ;
           P0176                                        SIG.TMP_INCOMING.P0176%TYPE                         ;
           P0177                                        SIG.TMP_INCOMING.P0177%TYPE                         ;
           P0189                                        SIG.TMP_INCOMING.P0189%TYPE                         ;
           P0191                                        SIG.TMP_INCOMING.P0191%TYPE                         ;
           P0228                                        SIG.TMP_INCOMING.P0228%TYPE                         ;
           P0230                                        SIG.TMP_INCOMING.P0230%TYPE                         ;
           P0262                                        SIG.TMP_INCOMING.P0262%TYPE                         ;
           P0264                                        SIG.TMP_INCOMING.P0264%TYPE                         ;
           P0280                                        SIG.TMP_INCOMING.P0280%TYPE                         ;
           P0300                                        SIG.TMP_INCOMING.P0300%TYPE                         ;
           P0301                                        SIG.TMP_INCOMING.P0301%TYPE                         ;
           P0302                                        SIG.TMP_INCOMING.P0302%TYPE                         ;
           P0306                                        SIG.TMP_INCOMING.P0306%TYPE                         ;
           P0358                                        SIG.TMP_INCOMING.P0358%TYPE                         ;
           P0359                                        SIG.TMP_INCOMING.P0359%TYPE                         ;
           P0370                                        SIG.TMP_INCOMING.P0370%TYPE                         ;
           P0372                                        SIG.TMP_INCOMING.P0372%TYPE                         ;
           P0374                                        SIG.TMP_INCOMING.P0374%TYPE                         ;
           P0378                                        SIG.TMP_INCOMING.P0378%TYPE                         ;
           P0380                                        SIG.TMP_INCOMING.P0380%TYPE                         ;
           P0381                                        SIG.TMP_INCOMING.P0381%TYPE                         ;
           P0384                                        SIG.TMP_INCOMING.P0384%TYPE                         ;
           P0390                                        SIG.TMP_INCOMING.P0390%TYPE                         ;
           P0391                                        SIG.TMP_INCOMING.P0391%TYPE                         ;
           P0392                                        SIG.TMP_INCOMING.P0392%TYPE                         ;
           P0393                                        SIG.TMP_INCOMING.P0393%TYPE                         ;
           P0394                                        SIG.TMP_INCOMING.P0394%TYPE                         ;
           P0395                                        SIG.TMP_INCOMING.P0395%TYPE                         ;
           P0396                                        SIG.TMP_INCOMING.P0396%TYPE                         ;
           P0400                                        SIG.TMP_INCOMING.P0400%TYPE                         ;
           P0401                                        SIG.TMP_INCOMING.P0401%TYPE                         ;
           P0402                                        SIG.TMP_INCOMING.P0402%TYPE                         ;
           P1000                                        SIG.TMP_INCOMING.P1000%TYPE                         ; 
           P1001                                        SIG.TMP_INCOMING.P1001%TYPE                         ; 
           P1003                                        SIG.TMP_INCOMING.P1003%TYPE                         ;    
           P1004                                        SIG.TMP_INCOMING.P1004%TYPE                         ;  
           P1005                                        SIG.TMP_INCOMING.P1005%TYPE                         ; 
           P1006                                        SIG.TMP_INCOMING.P1006%TYPE                         ; 
           P1007                                        SIG.TMP_INCOMING.P1007%TYPE                         ; 
           P1008                                        SIG.TMP_INCOMING.P1008%TYPE                         ; 
           P1009                                        SIG.TMP_INCOMING.P1009%TYPE                         ; 
           P1010                                        SIG.TMP_INCOMING.P1010%TYPE                         ; 
           P1011                                        SIG.TMP_INCOMING.P1011%TYPE                         ;        
           P1012                                        SIG.TMP_INCOMING.P1012%TYPE                         ; 
           OPERADOR                                     SIG.TMP_INCOMING.OPERADOR%TYPE                      ;
           getBinData  NUMBER;

     BEGIN
        
           DBMS_OUTPUT.PUT_LINE('Entrando');
           dbms_output.enable(1);

                OPEN voCursor FOR
                SELECT
                         MIT, 
                          NRO_TARJETA, 
                          CODIGO_PROCESAMIENTO, 
                         MONTO_TRANSAC, 
                          MONTO_CONCILIACION, 
                          MONTO_FACTURACION, 
                         TASA_CONVENIO_CONCILIACION, 
                          TASA_CONVENIO_FACTURACION, 
                          FECHA_HR_TRASAC, 
                         FECHA_VENCIMIENTO_TARJETA, 
                         COD_DATOS_PUNTO_SERVICIO, 
                         NRO_SECUENCIA_TARJETA, 
                         CODIGO_FUNCION, 
                         COD_MOTIVO_MENSAJE, 
                         MCC, 
                         MONTOS_ORIGINALES, 
                         DATOS_REFERENCIA_ADQUIRENTE, 
                         COD_IDENTIFICACION_ADQUIRENTE, 
                         COD_IDENT_INST_ENVIA, 
                         NRO_REFERENCIA_RECUPERACION,
                         CODIGO_AUTORIZACION, 
                         CODIGO_SERVICIO, 
                         IDENT_TERM_ACEP_TARJETA, 
                         COD_IDENT_ACEP_TARJETA, 
                         NOMBRE_UBIC_ACEP_TARJETA, 
                         DATOS_ADICIONALES_1, 
                         COD_MONEDA_TRANSAC, 
                         COD_MONEDA_CONCILIACION, 
                         COD_MONEDA_FACTURACION, 
                         MONTOS_ADICIONALES, 
                         ICC, 
                         DATOS_ADICIONALES_2, 
                         IDENT_CICLO_DURACION_TRANSAC, 
                         NRO_MENSAJE, 
                         REGISTRO_DE_DATOS, 
                         FECHA_ACCION, 
                         COD_IDENT_INST_DEST_TRANSAC, 
                         COD_IDENT_INST_ORIG_TRANSAC, 
                         DATOS_REF_EMISOR, 
                         COD_IDENT_INST_RECIBE, 
                         MONTO_RECARGO_X_CONVER_MONEDA, 
                         DATOS_ADICIONALES_3, 
                         DATOS_ADICIONALES_4, 
                         DATOS_ADICIONALES_5,
                         DATOS_ADICIONALES_6,
						 DATOS_ADICIONALES_7,
						 DATOS_ADICIONALES_8,
                          P0005              ,    
                         P0023              , 
                         P0025              ,
                         P0026              ,
                         P0044              ,      
                         P0052              ,
                         P0057              ,
                         P0071              ,
                         P0105              , 
                         P0122              , 
                         P0137              ,
                         P0146              ,      
                         P0148              ,  
                         P0149              ,     
                         P0158              ,
                         P0159              ,          
                         P0165              ,   
                         P0170              , 
                         P0176              , 
                         P0177              , 
                         P0189              , 
                         P0191              , 
                         P0228              ,    
                         P0230              ,  
                         P0262              ,
                         P0264              ,
                         P0280              ,
                         P0300              , 
                         P0301              ,
                         P0302              ,
                         P0306              ,
                         P0358              ,
                         P0359              ,
                         P0370              ,
                         P0372              ,
                         P0374              ,
                         P0378              ,
                         P0380              ,
                         P0381              ,
                         P0384              ,
                         P0390              ,
                         P0391              ,
                         P0392              ,
                         P0393              ,
                         P0394              ,
                         P0395              ,
                         P0396              ,  
                         P0400              ,
                         P0401              ,
                         P0402              ,
                         P1000,   
                         P1001              ,             
                         P1003              ,              
                         P1004              ,               
                         P1005              ,                
                         P1006              ,                 
                         P1007                ,                  
                         P1008               ,                   
                         P1009              ,                    
                         P1010                 ,                     
                         P1011               ,                      
                         P1012               ,
                         OPERADOR
                 FROM  SIG.TMP_INCOMING;
                    prfCursor:=voCursor;

        LOOP                                                                                           
                    FETCH prfCursor                                                                              
                    INTO  MIT, 
                          NRO_TARJETA, 
                          CODIGO_PROCESAMIENTO, 
                          MONTO_TRANSAC, 
                          MONTO_CONCILIACION, 
                          MONTO_FACTURACION, 
                          TASA_CONVENIO_CONCILIACION, 
                          TASA_CONVENIO_FACTURACION, 
                          FECHA_HR_TRASAC, 
                          FECHA_VENCIMIENTO_TARJETA, 
                          COD_DATOS_PUNTO_SERVICIO, 
                          NRO_SECUENCIA_TARJETA, 
                          CODIGO_FUNCION, 
                          COD_MOTIVO_MENSAJE, 
                          MCC, 
                          MONTOS_ORIGINALES, 
                          DATOS_REFERENCIA_ADQUIRENTE, 
                          COD_IDENTIFICACION_ADQUIRENTE, 
                          COD_IDENT_INST_ENVIA, 
                          NRO_REFERENCIA_RECUPERACION,
                          CODIGO_AUTORIZACION, 
                          CODIGO_SERVICIO, 
                          IDENT_TERM_ACEP_TARJETA, 
                          COD_IDENT_ACEP_TARJETA, 
                          NOMBRE_UBIC_ACEP_TARJETA, 
                          DATOS_ADICIONALES_1, 
                          COD_MONEDA_TRANSAC, 
                          COD_MONEDA_CONCILIACION, 
                          COD_MONEDA_FACTURACION, 
                          MONTOS_ADICIONALES, 
                          ICC, 
                          DATOS_ADICIONALES_2, 
                          IDENT_CICLO_DURACION_TRANSAC, 
                          NRO_MENSAJE, 
                          REGISTRO_DE_DATOS, 
                          FECHA_ACCION, 
                          COD_IDENT_INST_DEST_TRANSAC, 
                          COD_IDENT_INST_ORIG_TRANSAC, 
                          DATOS_REF_EMISOR, 
                          COD_IDENT_INST_RECIBE, 
                          MONTO_RECARGO_X_CONVER_MONEDA, 
                          DATOS_ADICIONALES_3, 
                          DATOS_ADICIONALES_4, 
                          DATOS_ADICIONALES_5,
                          DATOS_ADICIONALES_6,
						  DATOS_ADICIONALES_7,
						  DATOS_ADICIONALES_8,
                          P0005              ,    
                         P0023              , 
                         P0025              ,
                         P0026              ,
                         P0044              ,      
                         P0052              ,
                         P0057              ,
                         P0071              ,
                         P0105              , 
                         P0122              , 
                         P0137              ,
                         P0146              ,      
                         P0148              ,  
                         P0149              ,       
                         P0158              ,
                         P0159              ,          
                         P0165              ,   
                         P0170              , 
                         P0176              , 
                         P0177              , 
                         P0189              , 
                         P0191              , 
                         P0228              ,    
                         P0230              ,  
                         P0262              ,
                         P0264              ,
                         P0280              ,
                         P0300              , 
                         P0301              ,
                         P0302              ,
                         P0306              ,
                         P0358              ,
                         P0359              ,
                         P0370              ,
                         P0372              ,
                         P0374              ,
                         P0378              ,
                         P0380              ,
                         P0381              ,
                         P0384              ,
                         P0390              ,
                         P0391              ,
                         P0392              ,
                         P0393              ,
                         P0394              ,
                         P0395              ,
                         P0396              ,  
                         P0400              ,
                        P0401              ,
                        P0402              ,
                         P1000              ,            
                         P1001              ,             
                         P1003              ,              
                         P1004              ,               
                         P1005              ,                
                         P1006              ,                 
                         P1007                ,                  
                         P1008               ,                   
                         P1009              ,                    
                         P1010                 ,                     
                         P1011               ,                      
                         P1012              ,
                         OPERADOR           ;

                    EXIT WHEN prfCursor%NOTFOUND; 

              --SOLO PRUEBAS
           DBMS_OUTPUT.PUT_LINE('salida fetch');
           DBMS_OUTPUT.PUT_LINE('COD_IDENT_INST_ENVIA ' || COD_IDENT_INST_ENVIA);
           select case 
               when exists(SELECT SID FROM TBL_BINES_NACIONALES WHERE OBIN = COD_IDENT_INST_ENVIA)
               then 2
               else 3
             end  into OPERADOR
           from dual;
--           
--           SELECT SID into getBinData FROM TBL_OPERADOR WHERE OBIN = COD_IDENT_INST_ENVIA;
           DBMS_OUTPUT.PUT_LINE('OPERADOR ' || OPERADOR);
              --FIN PRUEBAS   
                  if OPERADOR = 1 then
                    DBMS_OUTPUT.PUT_LINE('llamada SP_SIG_REGLASDEDATOS');
                    SP_SIG_REGLASDEDATOS( MIT
                                     ,NRO_TARJETA                   
                                     ,CODIGO_PROCESAMIENTO          
                                     ,MONTO_TRANSAC                 
                                     ,MONTO_CONCILIACION            
                                     ,MONTO_FACTURACION             
                                     ,TASA_CONVENIO_CONCILIACION    
                                     ,TASA_CONVENIO_FACTURACION     
                                     ,FECHA_HR_TRASAC               
                                     ,FECHA_VENCIMIENTO_TARJETA     
                                     ,COD_DATOS_PUNTO_SERVICIO      
                                     ,NRO_SECUENCIA_TARJETA         
                                     ,CODIGO_FUNCION                
                                     ,COD_MOTIVO_MENSAJE            
                                     ,MCC                           
                                     ,MONTOS_ORIGINALES             
                                     ,DATOS_REFERENCIA_ADQUIRENTE   
                                     ,COD_IDENTIFICACION_ADQUIRENTE 
                                     ,COD_IDENT_INST_ENVIA          
                                     ,NRO_REFERENCIA_RECUPERACION   
                                     ,CODIGO_AUTORIZACION           
                                     ,CODIGO_SERVICIO               
                                     ,IDENT_TERM_ACEP_TARJETA       
                                     ,COD_IDENT_ACEP_TARJETA        
                                     ,NOMBRE_UBIC_ACEP_TARJETA      
                                     ,DATOS_ADICIONALES_1           
                                     ,COD_MONEDA_TRANSAC            
                                     ,COD_MONEDA_CONCILIACION       
                                     ,COD_MONEDA_FACTURACION        
                                     ,MONTOS_ADICIONALES            
                                     ,ICC                           
                                     ,DATOS_ADICIONALES_2           
                                     ,IDENT_CICLO_DURACION_TRANSAC  
                                     ,NRO_MENSAJE                   
                                     ,REGISTRO_DE_DATOS             
                                     ,FECHA_ACCION                  
                                     ,COD_IDENT_INST_DEST_TRANSAC   
                                     ,COD_IDENT_INST_ORIG_TRANSAC   
                                     ,DATOS_REF_EMISOR              
                                     ,COD_IDENT_INST_RECIBE         
                                     ,MONTO_RECARGO_X_CONVER_MONEDA 
                                     ,DATOS_ADICIONALES_3           
                                     ,DATOS_ADICIONALES_4           
                                     ,DATOS_ADICIONALES_5
                                     ,DATOS_ADICIONALES_6
                                     ,DATOS_ADICIONALES_7
                                     ,DATOS_ADICIONALES_8
                                      ,  P0005              ,    
                                     P0023              , 
                                     P0025              ,
                                     P0026              ,
                                     P0044              ,      
                                     P0052              ,
                                     P0057              ,
                                     P0071              ,
                                     P0105              , 
                                     P0122              , 
                                     P0137              ,
                                     P0146              ,      
                                     P0148              ,
                                     P0149              ,          
                                     P0158              ,
                                     P0159              ,          
                                     P0165              ,   
                                     P0170              , 
                                     P0176              , 
                                     P0177              , 
                                     P0189              , 
                                     P0191              , 
                                     P0228              ,    
                                     P0230              ,  
                                     P0262              ,
                                     P0264              ,
                                     P0280              ,
                                     P0300              , 
                                     P0301              ,
                                     P0302              ,
                                     P0306              ,
                                     P0358              ,
                                     P0359              ,
                                     P0370              ,
                                     P0372              ,
                                     P0374              ,
                                     P0378              ,
                                     P0380              ,
                                     P0381              ,
                                     P0384              ,
                                     P0390              ,
                                     P0391              ,
                                     P0392              ,
                                     P0393              ,
                                     P0394              ,
                                     P0395              ,
                                     P0396              ,  
                                     P0400              ,
                                     P0401              ,
                                     P0402              ,
                                     P1000,            
                                     P1001              ,             
                                     P1003              ,              
                                     P1004              ,               
                                     P1005              ,                
                                     P1006              ,                 
                                     P1007                ,                  
                                     P1008               ,                   
                                     P1009              ,                    
                                     P1010                 ,                     
                                     P1011               ,                      
                                     P1012              ,
                                     OPERADOR
                                     );  
                        ELSE
                 if OPERADOR = 2 or OPERADOR = 3 then
                    DBMS_OUTPUT.PUT_LINE('llamada SP_SIG_REGLASDEDATOS_VISA');
                    SP_SIG_REGLASDEDATOS_VISA( MIT
                                     ,NRO_TARJETA                   
                                     ,CODIGO_PROCESAMIENTO          
                                     ,MONTO_TRANSAC                 
                                     ,MONTO_CONCILIACION            
                                     ,MONTO_FACTURACION             
                                     ,TASA_CONVENIO_CONCILIACION    
                                     ,TASA_CONVENIO_FACTURACION     
                                     ,FECHA_HR_TRASAC               
                                     ,FECHA_VENCIMIENTO_TARJETA     
                                     ,COD_DATOS_PUNTO_SERVICIO      
                                     ,NRO_SECUENCIA_TARJETA         
                                     ,CODIGO_FUNCION                
                                     ,COD_MOTIVO_MENSAJE            
                                     ,MCC                           
                                     ,MONTOS_ORIGINALES             
                                     ,DATOS_REFERENCIA_ADQUIRENTE   
                                     ,COD_IDENTIFICACION_ADQUIRENTE 
                                     ,COD_IDENT_INST_ENVIA          
                                     ,NRO_REFERENCIA_RECUPERACION   
                                     ,CODIGO_AUTORIZACION           
                                     ,CODIGO_SERVICIO               
                                     ,IDENT_TERM_ACEP_TARJETA       
                                     ,COD_IDENT_ACEP_TARJETA        
                                     ,NOMBRE_UBIC_ACEP_TARJETA      
                                     ,DATOS_ADICIONALES_1           
                                     ,COD_MONEDA_TRANSAC            
                                     ,COD_MONEDA_CONCILIACION       
                                     ,COD_MONEDA_FACTURACION        
                                     ,MONTOS_ADICIONALES            
                                     ,ICC                           
                                     ,DATOS_ADICIONALES_2           
                                     ,IDENT_CICLO_DURACION_TRANSAC  
                                     ,NRO_MENSAJE                   
                                     ,REGISTRO_DE_DATOS             
                                     ,FECHA_ACCION                  
                                     ,COD_IDENT_INST_DEST_TRANSAC   
                                     ,COD_IDENT_INST_ORIG_TRANSAC   
                                     ,DATOS_REF_EMISOR              
                                     ,COD_IDENT_INST_RECIBE         
                                     ,MONTO_RECARGO_X_CONVER_MONEDA 
                                     ,DATOS_ADICIONALES_3           
                                     ,DATOS_ADICIONALES_4           
                                     ,DATOS_ADICIONALES_5   
                                     ,DATOS_ADICIONALES_6
									 ,DATOS_ADICIONALES_7
									 ,DATOS_ADICIONALES_8
                                      ,  P0005              ,    
                                     P0023              , 
                                     P0025              ,
                                     P0026              ,
                                     P0044              ,      
                                     P0052              ,
                                     P0057              ,
                                     P0071              ,
                                     P0105              , 
                                     P0122              , 
                                     P0137              ,
                                     P0146              ,      
                                     P0148              ,
                                     P0149              ,          
                                     P0158              ,
                                     P0159              ,          
                                     P0165              ,   
                                     P0170              , 
                                     P0176              , 
                                     P0177              , 
                                     P0189              , 
                                     P0191              , 
                                     P0228              ,    
                                     P0230              ,  
                                     P0262              ,
                                     P0264              ,
                                     P0280              ,
                                     P0300              , 
                                     P0301              ,
                                     P0302              ,
                                     P0306              ,
                                     P0358              ,
                                     P0359              ,
                                     P0370              ,
                                     P0372              ,
                                     P0374              ,
                                     P0378              ,
                                     P0380              ,
                                     P0381              ,
                                     P0384              ,
                                     P0390              ,
                                     P0391              ,
                                     P0392              ,
                                     P0393              ,
                                     P0394              ,
                                     P0395              ,
                                     P0396              ,  
                                     P0400              ,
                                     P0401              ,
                                     P0402              ,
                                     P1000,            
                                     P1001              ,             
                                     P1003              ,              
                                     P1004              ,               
                                     P1005              ,                
                                     P1006              ,                 
                                     P1007                ,                  
                                     P1008               ,                   
                                     P1009              ,                    
                                     P1010                 ,                     
                                     P1011               ,                      
                                     P1012              ,
                                     OPERADOR
                                     );  
                         END IF;
                          END IF;
                END LOOP;
                DBMS_OUTPUT.PUT_LINE('salida loop');
                CLOSE prfCursor;
                DBMS_OUTPUT.PUT_LINE('cierre cursor');
     END;            

  --tabla SIG.TMP_INCOMING SE LIMPIA
 execute immediate 'truncate table SIG.TMP_INCOMING';
 COMMIT;
EXCEPTION
WHEN  NO_DATA_FOUND     THEN
            DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS EN TABLA SIG.TMP_INCOMING');
WHEN  OTHERS THEN
      warning:=SQLERRM;   
      cod_error:=SQLCODE;
      DBMS_OUTPUT.PUT_LINE('ERROR : '||warning||' SQLCODE : '||cod_error);   
--insercion en tabla log error en caso de algun error desconocido          
     INSERT INTO LOG_ERROR (SID, 
                       FECHA_INSERCION, 
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_CARGAINCOMING',
                        cod_error||warning );
END SP_SIG_CARGAINCOMING;