create or replace PROCEDURE       "SP_SIG_TRANSACPRESENTACION" (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/07/2013
-- Versión : 1.0
-- Descripcion: Consulta las transacciones de presentacion de acuerdo a los parametros de entrada.
                    numeroTarjeta  IN VARCHAR2,
                    fechaInicio    IN VARCHAR2,
                    fechaTermino   IN VARCHAR2,
					codTransac			   IN VARCHAR2,
					codigoFuncion  IN VARCHAR2,
                    warning        OUT VARCHAR2,
                    cod_error      OUT NUMBER,
                    prfCursor      OUT Sys_RefCursor
                )

IS
    voCursor Sys_RefCursor:=null;
BEGIN

            BEGIN
                DBMS_OUTPUT.PUT_LINE(numeroTarjeta || ' ' || fechaInicio || ' ' || fechaTermino);
                IF (numeroTarjeta<>'0' and codTransac <>'00') THEN
                BEGIN
                        OPEN voCursor FOR 
                            SELECT   
                            --  I.MIT, 
                              --  I.CODIGO_FUNCION, 
								 Substr(I.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(I.NRO_TARJETA, 13, 16) AS NRO_TARJETA,
                                TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                 --REPLACE(I.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                                REGEXP_SUBSTR(I.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                TRIM(REGEXP_SUBSTR(I.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)) AS PAIS,  
                                CASE WHEN I.MONTO_TRANSAC is null THEN '' ELSE TO_CHAR(I.MONTO_TRANSAC) END     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN I.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_FACTURACION,1,LENGTH(I.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_FACTURACION,LENGTH(I.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(I.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                --I.CODIGO_AUTORIZACION,
                                --TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                'PENDIENTE'                                                         AS ESTADOTRX,
                                --NVL(I.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                --NVL(I.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                --NVL(I.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(I.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(I.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                --I.SID                                                               AS SID,
                                --'GLOSA GENERAL'                                                     AS GLOSAGENERAL,
                                --NVL(I.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                --NVL(I.MONTO_CONCILIACION, 0)                                        AS MONTOCONCILIACION                     
                                A.DESCRIPCION                                                       AS GLOSAGENERAL,
								'' AS NUM_INCIDENTE,   
                NVL(P.P0023, '-')                                                AS  PATPASS,
                TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))    AS OPERADOR
                            FROM SIG.TBL_INCOMING I
                              LEFT OUTER  JOIN SIG.TBL_PDS P ON (I.SID     = P.TRANSACCION),  SIG.TBL_ESTADO_PROCESO E, SIG.TBL_ACCION_TRANSAC A
                            WHERE     I.MIT                               = codTransac             AND 
                                I.CODIGO_FUNCION                         = codigoFuncion             AND
                                I.NRO_TARJETA                           =  numeroTarjeta    AND
                                TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio      AND  
                                TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
                                I.ESTADO_PROCESO = E.SID                                    AND
                                E.XKEY = 'IN_AC' AND I.MIT = A.COD_TRANSAC AND I.CODIGO_FUNCION = A.COD_FUNCION order by FECHATRANSAC ASC;

                    END;
				ELSIF (numeroTarjeta<>'0' and codTransac='00') THEN
					BEGIN
						OPEN voCursor FOR 
							SELECT   
                            --  I.MIT, 
                              --  I.CODIGO_FUNCION, 
								 Substr(I.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(I.NRO_TARJETA, 13, 16) AS NRO_TARJETA,
                                TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                 --REPLACE(I.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                                REGEXP_SUBSTR(I.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                TRIM(REGEXP_SUBSTR(I.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)) AS PAIS,    
                                CASE WHEN I.MONTO_TRANSAC is null THEN '' ELSE TO_CHAR(I.MONTO_TRANSAC) END     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN I.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_FACTURACION,1,LENGTH(I.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_FACTURACION,LENGTH(I.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(I.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                --I.CODIGO_AUTORIZACION,
                                --TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                'PENDIENTE'                                                         AS ESTADOTRX,
                                --NVL(I.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                --NVL(I.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                --NVL(I.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(I.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(I.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                --I.SID                                                               AS SID,
                                --'GLOSA GENERAL'                                                     AS GLOSAGENERAL,
                                --NVL(I.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                --NVL(I.MONTO_CONCILIACION, 0)                                        AS MONTOCONCILIACION                     
                                A.DESCRIPCION                                                       AS GLOSAGENERAL,
								'' AS NUM_INCIDENTE,
                 NVL(P.P0023, '-')                                                AS  PATPASS,
                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))       AS OPERADOR
                            FROM SIG.TBL_INCOMING I   LEFT OUTER  JOIN SIG.TBL_PDS P ON (I.SID     = P.TRANSACCION), SIG.TBL_ESTADO_PROCESO E, SIG.TBL_ACCION_TRANSAC A
                            WHERE     
                                I.NRO_TARJETA                           =  numeroTarjeta    AND
                                TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio      AND  
                                TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
                                I.ESTADO_PROCESO = E.SID                                    AND
                                E.XKEY = 'IN_AC' AND 
                                I.MIT = A.COD_TRANSAC AND 
                                I.CODIGO_FUNCION = A.COD_FUNCION 
                                order by FECHATRANSAC ASC;
					END;
                ELSE
                    BEGIN
                        OPEN voCursor FOR 
                            SELECT 
                             --  I.MIT, 
                              --  I.CODIGO_FUNCION, 
                                Substr(I.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(I.NRO_TARJETA, 13, 16) AS NRO_TARJETA,
                                TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                --REPLACE(I.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                                REGEXP_SUBSTR(I.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                TRIM(REGEXP_SUBSTR(I.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)) AS PAIS,  
                                CASE WHEN I.MONTO_TRANSAC is null THEN '' ELSE TO_CHAR(I.MONTO_TRANSAC) END     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN I.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_FACTURACION,1,LENGTH(I.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_FACTURACION,LENGTH(I.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(I.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                --I.CODIGO_AUTORIZACION,
                                --TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                'PENDIENTE'                                                         AS ESTADOTRX,
                                --NVL(I.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                --NVL(I.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                --NVL(I.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(I.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(I.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                --I.SID                                                               AS SID,
                                --'GLOSA GENERAL'                                                     AS GLOSAGENERAL,
                                --NVL(I.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                --NVL(I.MONTO_CONCILIACION, 0)                                        AS MONTOCONCILIACION 
								A.DESCRIPCION                                                       AS GLOSAGENERAL,
                '' AS NUM_INCIDENTE,
                 NVL(P.P0023, '-')                                                AS  PATPASS,
                TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))       AS  OPERADOR
                            FROM    SIG.TBL_INCOMING I   LEFT OUTER  JOIN SIG.TBL_PDS P ON (I.SID     = P.TRANSACCION), SIG.TBL_ESTADO_PROCESO E,SIG.TBL_ACCION_TRANSAC A
                            WHERE     I.MIT                             = codTransac                 AND 
                                I.CODIGO_FUNCION                         = codigoFuncion             AND
                                TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio      AND  
                                TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
                                I.ESTADO_PROCESO = E.SID                                    AND
                                E.XKEY = 'IN_AC' AND 
                                I.MIT = A.COD_TRANSAC AND 
                                I.CODIGO_FUNCION = A.COD_FUNCION 
                                order by FECHATRANSAC ASC;

                    END;
                END IF;
                prfCursor:=voCursor;

            EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_TRANSACPRESENTACION', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
            END;

END SP_SIG_TRANSACPRESENTACION;