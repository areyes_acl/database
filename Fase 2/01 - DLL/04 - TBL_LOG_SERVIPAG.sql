CREATE TABLE "SIG"."TBL_LOG_SERVIPAG" 
   (	
   "SID" NUMBER NOT NULL, 
	"FILE_TS" TIMESTAMP (6), 
	"FILE_NAME" VARCHAR2(50 BYTE), 
	"FILE_FLAG" VARCHAR2(2 BYTE), 
	"FECHA" DATE, 
	 CONSTRAINT TBL_LOG_SERVIPAG_PK PRIMARY KEY 
	  (
		SID 
	  )
	  ENABLE 
	);

   COMMENT ON COLUMN "SIG"."TBL_LOG_SERVIPAG"."SID" IS 'Corresponde al id de la tabla TBL_LOG_SERVIPAG';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SERVIPAG"."FILE_TS" IS 'Corresponde a la estampa de tiempo del proceso del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SERVIPAG"."FILE_NAME" IS 'Corresponde al nombre del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SERVIPAG"."FILE_FLAG" IS 'Corresponde al flag de procesamiento creado 0:subido 1:-1:error';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SERVIPAG"."FECHA" IS 'Corresponde a la fecha de Procesamiento del archivo';
   COMMIT;