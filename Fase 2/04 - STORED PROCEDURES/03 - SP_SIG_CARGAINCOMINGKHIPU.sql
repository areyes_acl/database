create or replace PROCEDURE       "SP_SIG_CARGAINCOMINGKHIPU" (warning       OUT VARCHAR2,
                                             cod_error      OUT NUMBER
                                                       
)
IS
   
        voCursor  Sys_RefCursor:=null;
        prfCursor Sys_RefCursor:=null;

-- Proyecto: SIG
-- Nombre Objeto:SP_SIG_CARGAINCOMINGKHIPU
-- By: Developer || ACL PTM | 10/07/2013
-- Versión : 1.1
-- Descripcion:carga tbl_incoming      desde TMP_ENTRADA_KHIPU
-- Modificación Alejandro Trigo
-- Descripción se agrega carga PDS Internacional.

BEGIN
       DBMS_OUTPUT.PUT_LINE('salida fetch1');
      DECLARE
           FECHA_PAGO                                SIG.TMP_ENTRADA_KHIPU.FECHA_PAGO%TYPE                           ; 
           HORA_PAGO                                 SIG.TMP_ENTRADA_KHIPU.HORA_PAGO%TYPE                   ; 
           RUT                      				 SIG.TMP_ENTRADA_KHIPU.RUT%TYPE          ; 
           TRANSACCION_ID                            SIG.TMP_ENTRADA_KHIPU.TRANSACCION_ID%TYPE                 ; 
           DESCRIPCION                        		 SIG.TMP_ENTRADA_KHIPU.DESCRIPCION%TYPE            ; 
           LOCAL_PAGO                         		 SIG.TMP_ENTRADA_KHIPU.LOCAL_PAGO%TYPE             ; 
           CLIENTE_NOMBRE                			 SIG.TMP_ENTRADA_KHIPU.CLIENTE_NOMBRE%TYPE    ; 
           CLIENTE_EMAIL                 			 SIG.TMP_ENTRADA_KHIPU.CLIENTE_EMAIL%TYPE     ; 
           CLIENTE_NOMBRE_BANCO                      SIG.TMP_ENTRADA_KHIPU.CLIENTE_NOMBRE_BANCO%TYPE               ; 
           CLIENTE_NUMERO_CUENTA                     SIG.TMP_ENTRADA_KHIPU.CLIENTE_NUMERO_CUENTA%TYPE     ; 
           ESTRUCTURA_PAGO                           SIG.TMP_ENTRADA_KHIPU.ESTRUCTURA_PAGO%TYPE      ; 
           MONTO_PAGO                                SIG.TMP_ENTRADA_KHIPU.MONTO_PAGO%TYPE         ; 
           MONTO_FINAL                               SIG.TMP_ENTRADA_KHIPU.MONTO_FINAL%TYPE                ; 
           MONTO_MINIMO                              SIG.TMP_ENTRADA_KHIPU.MONTO_MINIMO%TYPE            ;  
           DESCUENTO                                 SIG.TMP_ENTRADA_KHIPU.DESCUENTO%TYPE                           ; 
           COMISION                                  SIG.TMP_ENTRADA_KHIPU.COMISION%TYPE             ; 
           COMISION_INTEGRADA                        SIG.TMP_ENTRADA_KHIPU.COMISION_INTEGRADA%TYPE   ; 
           CUSTOM                                    SIG.TMP_ENTRADA_KHIPU.CUSTOM%TYPE ; 
           OPERADOR                                  SIG.TMP_ENTRADA_KHIPU.OPERADOR%TYPE          ; 
           FECHA_CONTABLE                            SIG.TMP_ENTRADA_KHIPU.FECHA_CONTABLE%TYPE          ; 
         

     BEGIN
        
           DBMS_OUTPUT.PUT_LINE('Entrando');
           dbms_output.enable(1);

                OPEN voCursor FOR
                SELECT
                         FECHA_PAGO, 
                         HORA_PAGO, 
                         RUT, 
                         TRANSACCION_ID, 
                         DESCRIPCION, 
                         LOCAL_PAGO, 
                         CLIENTE_NOMBRE, 
                         CLIENTE_EMAIL, 
                         CLIENTE_NOMBRE_BANCO, 
                         CLIENTE_NUMERO_CUENTA, 
                         ESTRUCTURA_PAGO, 
                         MONTO_PAGO, 
                         MONTO_FINAL, 
                         MONTO_MINIMO, 
                         DESCUENTO, 
                         COMISION, 
                         COMISION_INTEGRADA, 
                         CUSTOM, 
                         OPERADOR,
                         FECHA_CONTABLE
                 FROM  SIG.TMP_ENTRADA_KHIPU;
                    prfCursor:=voCursor;

        LOOP                                                                                           
                    FETCH prfCursor                                                                              
                    INTO  FECHA_PAGO, 
                          HORA_PAGO, 
                          RUT, 
                          TRANSACCION_ID, 
                          DESCRIPCION, 
                          LOCAL_PAGO, 
                          CLIENTE_NOMBRE, 
                          CLIENTE_EMAIL, 
                          CLIENTE_NOMBRE_BANCO, 
                          CLIENTE_NUMERO_CUENTA, 
                          ESTRUCTURA_PAGO, 
                          MONTO_PAGO, 
                          MONTO_FINAL, 
                          MONTO_MINIMO, 
                          DESCUENTO, 
                          COMISION, 
                          COMISION_INTEGRADA, 
                          CUSTOM, 
                          OPERADOR,
                          FECHA_CONTABLE;

                    EXIT WHEN prfCursor%NOTFOUND; 

              --SOLO PRUEBAS
            DBMS_OUTPUT.PUT_LINE('salida fetch');
            DBMS_OUTPUT.PUT_LINE('OPERADOR ' || OPERADOR);
             
                
			DBMS_OUTPUT.PUT_LINE('llamada SP_SIG_REGLASDEDATOS');
			INSERT INTO TBL_PAGOS_RECIBIDOS (SID, FECHA_PAGO, 
                         HORA_PAGO, 
                         RUT, 
                         TRANSACCION_ID, 
                         DESCRIPCION, 
                         LOCAL_PAGO, 
                         CLIENTE_NOMBRE, 
                         CLIENTE_EMAIL, 
                         CLIENTE_NOMBRE_BANCO, 
                         CLIENTE_NUMERO_CUENTA, 
                         ESTRUCTURA_PAGO, 
                         MONTO_PAGO, 
                         MONTO_FINAL, 
                         MONTO_MINIMO, 
                         DESCUENTO, 
                         COMISION, 
                         COMISION_INTEGRADA, 
                         CUSTOM, 
                         OPERADOR,
                         FECHA_CONTABLE)
            VALUES     ( SEQ_TBL_PAGOS_RECIBIDOS.nextval,

                         FECHA_PAGO, 
                         HORA_PAGO, 
                         RUT, 
                         TRANSACCION_ID, 
                         DESCRIPCION, 
                         LOCAL_PAGO, 
                         CLIENTE_NOMBRE, 
                         CLIENTE_EMAIL, 
                         CLIENTE_NOMBRE_BANCO, 
                         CLIENTE_NUMERO_CUENTA, 
                         ESTRUCTURA_PAGO, 
                         MONTO_PAGO, 
                         MONTO_FINAL, 
                         MONTO_MINIMO, 
                         DESCUENTO, 
                         COMISION, 
                         COMISION_INTEGRADA, 
                         CUSTOM, 
                         OPERADOR,

                         FECHA_CONTABLE);
            COMMIT;
			                      
		END LOOP;
		DBMS_OUTPUT.PUT_LINE('salida loop');
		CLOSE prfCursor;
		DBMS_OUTPUT.PUT_LINE('cierre cursor');
     END;            

  --tabla SIG.TMP_ENTRADA_KHIPU SE LIMPIA
    execute immediate 'truncate table SIG.TMP_ENTRADA_KHIPU';
 
EXCEPTION
WHEN  NO_DATA_FOUND     THEN
            DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS EN TABLA SIG.TMP_ENTRADA_KHIPU');
WHEN  OTHERS THEN
      warning:=SQLERRM;   
      cod_error:=SQLCODE;
      DBMS_OUTPUT.PUT_LINE('ERROR : '||warning||' SQLCODE : '||cod_error);   
--insercion en tabla log error en caso de algun error desconocido          
     INSERT INTO LOG_ERROR (SID, 
                       FECHA_INSERCION, 
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_CARGAINCOMINGKHIPU',
                        cod_error||warning );
END SP_SIG_CARGAINCOMINGKHIPU;