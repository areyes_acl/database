create or replace PROCEDURE       "SP_SIG_BUSCAR_TRX_PG" (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/06/2016
-- Versión : 1.0
-- Descripcion: Consulta las transacciones de presentacion de acuerdo a los parametros de entrada.
--              La busqueda puede ser realizada por usuario = ADM para ver todas las trx
--                                            o por usuario <>ADM para ver solo las que hizo el usuario.
--              La búsqueda puede ser realizara por estados en que se encuentren las gestiones de trx.
--              Pendiente de incoming, Enviada a incoming , o ambas.
--              La búsqueda puede ser realiza por numero de tarjeta o no.
--              La búsqueda puede ser realizada por tipo de transaccion.


    p_fechaDesde         IN VARCHAR2,
    p_fechaHasta         IN VARCHAR2,
    p_xkey_estado        IN VARCHAR2,
    p_nro_tarjeta        IN VARCHAR2,
    p_tipo_transac       IN VARCHAR2,
    p_sid_user           IN VARCHAR2,
    numPagina            IN NUMBER,
    cantReg              IN NUMBER,
    FILTRO_OPERADOR      IN VARCHAR2,
    warning              OUT VARCHAR2,
    cod_error            OUT NUMBER,
    totReg               OUT number,
    prfCursor            OUT Sys_RefCursor
)
IS
    voCursor Sys_RefCursor:=null;
    v_cant NUMBER;
    v_estado    NUMBER;
    v_estado2    NUMBER;
    v_estado3    NUMBER;
    v_sid_rol_user  NUMBER;
    v_sid_user NUMBER; 
    sql_stmt VARCHAR2(7000); 
    sql_fields VARCHAR2(2000); 
    sql_from_relation VARCHAR2(2000);
    sql_where_condition VARCHAR2(2000);
    sql_dinamic_condition VARCHAR2(2000);
    v_check_user  NUMBER;
    v_tipo_user VARCHAR (15);
    NO_DATA EXCEPTION;

BEGIN

sql_dinamic_condition := '';
warning := 'Se ha ejecutado SP_SIG_BUSCAR_TRX, correctamente';

     DBMS_OUTPUT.PUT_LINE('v_sid_user : ' || p_sid_user);
     DBMS_OUTPUT.PUT_LINE('p_nro_tarjeta : ' || p_nro_tarjeta);


      -- CAMPOS PARA TODAS LAS QUERYS
    sql_fields :=' GT.TRANSACCION AS SID_TRANSACCION,
                   GT.NRO_TARJETA,
                   TO_CHAR(GT.FECHA_HR_TRASAC, ''DD/MM/YYYY'')                           AS FECHATRANSAC,
                   REGEXP_SUBSTR(GT.NOMBRE_UBIC_ACEP_TARJETA,''[^\]+'',1,1)              AS COMERCIO,
                   REGEXP_SUBSTR(GT.NOMBRE_UBIC_ACEP_TARJETA,''[^\]+'',1,3)              AS PAIS,                 
                   GT.MONTO_TRANSAC,
                   NVL(GT.COD_MOTIVO_MENSAJE, '' '')                                     AS CODRAZON,
                   (SELECT XKEY FROM TBL_ACCION_TRANSAC WHERE SID = GT.ACCION_TRANSAC)   AS TIPO,
                   GT.ACCION_TRANSAC AS DOLOR,
                   EP.SID                                                                AS ESTADO_PROCESO,
                   EP.DESCRIPCION                                                        AS DESC_ESTADO,
                   T.NUM_INCIDENTE														                           AS NUM_INCIDENTE,								
                   CONCAT(NVL(US.RUT,''''),NVL(US.DV,''''))                              AS USUARIO,
                   TO_CHAR(GT.FECHA_ACTUALIZACION, ''DD/MM/YYYY hh24:mi:ss'')            AS FECHA_GESTION,
                   AT.DESCRIPCION                                                        AS TIPO_TRANSACCION,
                   NVL(P.P0023, 0)                                                       AS  PATPASS,
                   I.OPERADOR                                                          AS IDOPERADOR,
                   TRIM((CASE WHEN I.OPERADOR = 2 THEN ''VN'' ELSE ''VI'' END))             AS OPERADOR';


    -- TABLAS DE LA RELACION    (PARA TODAS LAS QUERYS)
    sql_from_relation := chr(10)
                      ||' FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION)
                                LEFT JOIN TBL_USUARIO_IG US ON GT.USUARIO = US.SID                               
                                LEFT JOIN TBL_ACCION_TRANSAC AT ON T.ACCION_TRANSAC = AT.SID
                                LEFT JOIN TBL_ESTADO_PROCESO EP ON GT.ESTADO_PROCESO = EP.SID
                                LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID = P.TRANSACCION)
                                LEFT JOIN TBL_INCOMING I ON GT.TRANSACCION = I.SID';

     -- CONDICION DE QUERY
     sql_where_condition := chr(10)
                          ||' WHERE
                                   TRUNC(GT.FECHA_ACTUALIZACION) between to_date(:FECHA_DESDE,''DD/MM/YYYY'') AND to_date(:FECHA_HASTA,''DD/MM/YYYY'') AND GT.ACCION_TRANSAC = T.ACCION_TRANSAC  ';



     -- INCORPORA FILTRO POR TIPO DE USUARIO
     IF( v_tipo_user = 'GESTION')
        THEN
         DBMS_OUTPUT.PUT_LINE ('FILTRA POR NUMERO DE TARJETA');
         sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND GT.USUARIO  = '''||p_sid_user||''' ';
     END IF;

    -- INCORPORA FILTRO POR NRO DE TARJETA
     IF( p_nro_tarjeta <> '0')
        THEN
         DBMS_OUTPUT.PUT_LINE ('FILTRA POR NUMERO DE TARJETA');
         sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND  T.NRO_TARJETA  = '''||p_nro_tarjeta||''' ';
     END IF;

      -- INCORPORA FILTRO POR TIPO DE OPERADOR
    IF( FILTRO_OPERADOR > 0)
       THEN
        DBMS_OUTPUT.PUT_LINE ('FILTRA POR TIPO DE OPERADOR');
         sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND  I.OPERADOR  = '''||FILTRO_OPERADOR||''' ';
        else if(FILTRO_OPERADOR = 0)then
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND  I.OPERADOR IS NOT NULL ';
         end if;
    END IF;


     -- FILTRA POR ESTADO
    IF( p_xkey_estado = 'TDO')
      THEN
        DBMS_OUTPUT.PUT_LINE ('FILTRA POR ESTADO');
        SELECT SID INTO v_estado FROM SIG.TBL_ESTADO_PROCESO WHERE xkey = 'OU_PEN';
        SELECT SID INTO v_estado2 FROM SIG.TBL_ESTADO_PROCESO WHERE xkey = 'OU_OK';
         SELECT SID INTO v_estado3 FROM SIG.TBL_ESTADO_PROCESO WHERE xkey = 'ING';

        -- sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND  GT.ESTADO_PROCESO IN ('''||v_estado||''','''||v_estado2||''','''||v_estado3||''') ';
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| 'AND ( GT.ESTADO_PROCESO IN ('''||v_estado||''','''||v_estado2||''')';
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)||'  OR ( GT.ESTADO_PROCESO IN ('''||v_estado3||''')' ; 
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)||' AND 0=(SELECT COUNT(SID)  FROM TBL_GESTION_TRANSAC GT2 WHERE  GT2.TRANSACCION = GT.TRANSACCION AND GT2.ACCION_TRANSAC IN  (26,27,28))';
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)||' AND 0=(SELECT COUNT(SID)  FROM TBL_PERDIDAS P WHERE  GT.TRANSACCION = P.SID_INCOMING)';
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)||' AND 0=(SELECT COUNT(SID)  FROM TBL_GESTION_CARGO_ABONO GCA WHERE  GT.TRANSACCION = GCA.TRANSACCION)';
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)||')';
        sql_dinamic_condition := sql_dinamic_condition ||chr(10)||')'; 

    ELSIF(p_xkey_estado <> 'TDO')
      THEN
        SELECT SID INTO v_estado FROM SIG.TBL_ESTADO_PROCESO WHERE xkey = p_xkey_estado;
        DBMS_OUTPUT.PUT_LINE ('FILTRA POR ESTADO : ' || v_estado);
        IF(v_estado = 3) THEN
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND  GT.ESTADO_PROCESO IN ('''||v_estado||''')'; 
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| 'AND 0=(SELECT COUNT(SID)  FROM TBL_GESTION_TRANSAC GT2 WHERE  GT2.TRANSACCION = GT.TRANSACCION AND GT2.ACCION_TRANSAC IN  (26,27,28))';
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| 'AND 0=(SELECT COUNT(SID)  FROM TBL_PERDIDAS P WHERE  GT.TRANSACCION = P.SID_INCOMING)';
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| 'AND 0=(SELECT COUNT(SID)  FROM TBL_GESTION_CARGO_ABONO GCA WHERE  GT.TRANSACCION = GCA.TRANSACCION)';
       ELSE
          sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND  GT.ESTADO_PROCESO IN ('''||v_estado||''') ';
        END IF;
    END IF;



    -- FILTRO TIPO DE TRANSACCION (00_00: TODOS, 15_00: 1º CONTRACARGO, 52_00: PET VALE, 15_205 : 2º CONTRACARGO)
    IF( p_tipo_transac <> '00_00')
        THEN
         DBMS_OUTPUT.PUT_LINE ('FILTRA POR TIPO DE TRANSACCION');
         sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND   GT.ACCION_TRANSAC   =  (SELECT SID FROM TBL_ACCION_TRANSAC WHERE XKEY = '''||p_tipo_transac||''') ';
     ELSE
         DBMS_OUTPUT.PUT_LINE ('SE MUESTRAN LAS TRANSACCIONES DISPONIBLES');
         sql_dinamic_condition := sql_dinamic_condition ||chr(10)|| ' AND GT.ACCION_TRANSAC IN (26, 27, 28)';
     END IF;


       -- CONCATENA LAS CONDICIONES DINAMICAS A LA CLAUSULA WHERE
      sql_where_condition :=sql_where_condition || sql_dinamic_condition;


    -- QUERY FINAL CONCATENACION
    sql_stmt := ' SELECT * FROM ( SELECT ROWNUM  AS registro,t.* FROM ('
              || ' SELECT ' 
                 || sql_fields
                 || sql_from_relation
                 || sql_where_condition
                 || ') t ) '
                 || chr(10)
                 ||' WHERE registro BETWEEN (:NUM_PAGINA - 1) * :CANT_REG + 1'
                 || ' AND  :NUM_PAGINA * :CANT_REG ';

       -- IMPRIME LA QUERY           
      DBMS_OUTPUT.PUT_LINE (sql_stmt);

      -- EJECUTA LA QUERY DE BUSQUEDA DE REGISTROS
      OPEN voCursor FOR sql_stmt USING p_fechaDesde, p_fechaHasta, numPagina, cantReg, numPagina, cantReg;


      -- QUERY PARA CONTAR EL TOTAL DE PAGINAS 
        sql_stmt:= 'SELECT COUNT(*)  AS cantidad
                      FROM  (SELECT ROWNUM  AS registro,t.* FROM ( ' 
                            ||' SELECT ' 
                            || sql_fields
                            || sql_from_relation
                            || sql_where_condition
                            ||' order by FECHATRANSAC ASC ) t) ';

      DBMS_OUTPUT.PUT_LINE (chr(10)||'PAGINACION :' ||sql_stmt );                    

      -- EJECUTA QUERY DE PAGINACION
      EXECUTE IMMEDIATE sql_stmt INTO v_cant USING p_fechaDesde, p_fechaHasta ;



   prfCursor:=voCursor;
   totReg:=v_cant;


    DBMS_OUTPUT.PUT_LINE ('Salida Correcta');
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.PUT_LINE ('No data');
        cod_error := SQLCODE;
        warning := DBMS_UTILITY.format_error_backtrace || '-' || SQLERRM ;

        DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);

      WHEN OTHERS THEN
        DBMS_OUTPUT.PUT_LINE ('OTHERS');
        cod_error := SQLCODE;
        warning := DBMS_UTILITY.format_error_backtrace ||'-'|| SQLERRM;

        DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);
END SP_SIG_BUSCAR_TRX_PG;

