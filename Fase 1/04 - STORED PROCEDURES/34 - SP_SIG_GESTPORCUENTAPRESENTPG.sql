create or replace PROCEDURE       "SP_SIG_GESTPORCUENTAPRESENTPG" (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/07/2013
-- Versi?n : 1.0
-- Descripcion: Consulta las presentaciones que tienen movimiento en tbl_gestion_transac segun los parametros de entrada.
                    numeroTarjeta  IN VARCHAR2,
                    fechaInicio    IN VARCHAR2,
                    fechaTermino   IN VARCHAR2,
                    numPagina      IN NUMBER,
                    cantReg        IN NUMBER,
                    warning        OUT VARCHAR2,
                    cod_error      OUT NUMBER,
                    prfCursor      OUT Sys_RefCursor,
                    totReg         OUT number
                )

IS
    voCursor Sys_RefCursor:=null;
    v_cant NUMBER;
BEGIN
    cod_error := 0;
    warning := 'Se ha ejecutado SP_SIG_GESTPORCUENTAPRESENTPG correctamente';
            BEGIN
                DBMS_OUTPUT.PUT_LINE(numeroTarjeta || ' ' || fechaInicio || ' ' || fechaTermino);
                IF (numeroTarjeta<>'0') THEN
                BEGIN
                    OPEN voCursor FOR 
                        SELECT * FROM (
                          SELECT ROWNUM AS registro, t.* FROM (
                            SELECT DISTINCT
                                GT.TRANSACCION                                                       AS SIDINCOMMING,
                                T.MIT                                                                AS MIT, 
                                T.CODIGO_FUNCION                                                     AS CODIGOFUNCION, 
                                T.NRO_TARJETA                                                        AS NUMEROTARJETA, 
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                  AS FECHATRANSAC,
                                REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                              	TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTOTRANSACCION,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTOFACTURACION,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTOCONCILIACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                       AS CODIGORAZON,
                                NVL(T.COD_MONEDA_TRANSAC, ' ')                                       AS CODMONEDATRX,
                                EP.DESCRIPCION                                                       AS ESTATUS,
                                
                                 AT.DESCRIPCION   AS GLOSATIPO,

                               DECODE (T.COD_MONEDA_CONCILIACION ,             
                                            '152',' $',
                                            '840',' USD',
                                            'SIN MONEDA'     
                               ) AS GLOSATIPOMONEDA,
                              TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                    AS FECHA_GESTION,
                               NVL(P.P0023, '-')                                                AS  PATPASS,
                               I.OPERADOR                                                       AS IOPERADOR,
                               O.OPERADOR                                                       AS OPERADOR,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                T.CODIGO_AUTORIZACION                                               AS CODIGOAUTORIZACION,
                                NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRIENTE,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4
                            FROM  SIG.TBL_ESTADO_TRANSAC EP,
                                 (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION  LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID = P.TRANSACCION) ),
                                 (SIG.TBL_TRANSACCIONES TE LEFT JOIN TBL_ACCION_TRANSAC AT ON (TE.MIT = AT.COD_TRANSAC  AND TE.CODIGO_FUNCION = AT.COD_FUNCION)),
                                 TBL_INCOMING I, TBL_OPERADOR O
                            WHERE 
                                T.NRO_TARJETA = numeroTarjeta                                        AND
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ESTADO_TRANSAC                         = EP.SID                AND
                                GT.TRANSACCION                           = I.SID                 AND
                                I.OPERADOR                               = O.SID                 AND
                                GT.ESTADO_PROCESO                        IN (3,4,5)               AND
                                EP.SID IN (1, 2 )                                 
                            GROUP BY
                                GT.TRANSACCION, 
                                T.MIT, 
                                T.CODIGO_FUNCION ,
                                T.NRO_TARJETA,
                                T.FECHA_HR_TRASAC,
                                T.NOMBRE_UBIC_ACEP_TARJETA, 
                                T.MONTO_TRANSAC,
                                T.MONTO_FACTURACION,
                                T.MONTO_CONCILIACION,
                                T.COD_MOTIVO_MENSAJE,
                                T.COD_MONEDA_TRANSAC,
                                EP.DESCRIPCION,
                                AT.DESCRIPCION,
                                T.COD_MONEDA_CONCILIACION,
                                GT.FECHA_ACTUALIZACION,
                                P.P0023,
                                I.OPERADOR, 
                                O.OPERADOR,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' '),
                                T.CODIGO_AUTORIZACION,
                                T.CODIGO_PROCESAMIENTO,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' '),
                                T.TASA_CONVENIO_CONCILIACION,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' '),
                                T.COD_DATOS_PUNTO_SERVICIO,
                                T.TASA_CONVENIO_FACTURACION,
                                T.MCC
                                )t)
      WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
      AND  numPagina * cantReg;


      SELECT count(registro) AS cantidad into v_cant
      FROM (SELECT ROWNUM AS registro, t.* FROM (
                      SELECT DISTINCT
                                GT.TRANSACCION                                                       AS SIDINCOMMING,
                              TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                    AS FECHA_GESTION
                            FROM  SIG.TBL_ESTADO_TRANSAC EP,
                                 (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION  LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID   = P.TRANSACCION) ) 
                            WHERE     
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaTermino          AND  
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ESTADO_TRANSAC                         = EP.SID                AND
                                GT.ESTADO_PROCESO                        IN (3,4,5)               AND
                                 EP.SID   IN (1,2)                                      
                            GROUP BY
                                GT.TRANSACCION, 
                                GT.FECHA_ACTUALIZACION                         
                                )t);                  
                    END;
                ELSE
                    BEGIN
                        OPEN voCursor FOR 
                           SELECT * FROM (
                SELECT ROWNUM AS registro, t.* FROM (
                      SELECT DISTINCT
                                GT.TRANSACCION                                                       AS SIDINCOMMING,
                                T.MIT                                                                AS MIT, 
                                T.CODIGO_FUNCION                                                     AS CODIGOFUNCION, 
                                T.NRO_TARJETA                                                        AS NUMEROTARJETA, 
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                  AS FECHATRANSAC,
                                REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                              	TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTOTRANSACCION,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTOFACTURACION,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTOCONCILIACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                       AS CODIGORAZON,                                
                                NVL(T.COD_MONEDA_TRANSAC, ' ')                                       AS CODMONEDATRX,
                                EP.DESCRIPCION                                                       AS ESTATUS,
                                
                                AT.DESCRIPCION   AS GLOSATIPO,

                               DECODE (T.COD_MONEDA_CONCILIACION ,             
                                            '152',' $',
                                            '840',' USD',
                                            'SIN MONEDA'     
                               ) AS GLOSATIPOMONEDA,
                              TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                    AS FECHA_GESTION,
                                NVL(P.P0023, '-')                                                AS  PATPASS,
                                I.OPERADOR                                                       AS  IOPERADOR,
                                O.OPERADOR                                                       AS  OPERADOR,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                T.CODIGO_AUTORIZACION                                               AS CODIGOAUTORIZACION,
                                NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRIENTE,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4
                            FROM  SIG.TBL_ESTADO_TRANSAC EP,
                                 (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION  LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID = P.TRANSACCION) ),
                                 (SIG.TBL_TRANSACCIONES TE LEFT JOIN TBL_ACCION_TRANSAC AT ON (TE.MIT = AT.COD_TRANSAC  AND TE.CODIGO_FUNCION = AT.COD_FUNCION)),
                                 TBL_INCOMING I, TBL_OPERADOR O
                            WHERE     
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ESTADO_TRANSAC                         = EP.SID                AND
                                GT.TRANSACCION                           = I.SID                 AND
                                I.OPERADOR                               = O.SID                 AND
                                GT.ESTADO_PROCESO                        IN (3,4,5)               AND
                                EP.SID   IN (1,2)                               
                            GROUP BY
                                GT.TRANSACCION, 
                                T.MIT, 
                                T.CODIGO_FUNCION ,
                                T.NRO_TARJETA,
                                T.FECHA_HR_TRASAC,
                                T.NOMBRE_UBIC_ACEP_TARJETA, 
                                T.MONTO_TRANSAC,
                                T.MONTO_FACTURACION,
                                T.MONTO_CONCILIACION,
                                T.COD_MOTIVO_MENSAJE,
                                T.COD_MONEDA_TRANSAC,
                                EP.DESCRIPCION,
                                AT.DESCRIPCION,
                                T.COD_MONEDA_CONCILIACION,
                                GT.FECHA_ACTUALIZACION,
                                P.P0023,
                                I.OPERADOR,
                                O.OPERADOR,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' '),
                                T.CODIGO_AUTORIZACION,
                                T.CODIGO_PROCESAMIENTO,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' '),
                                T.TASA_CONVENIO_CONCILIACION,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' '),
                                T.COD_DATOS_PUNTO_SERVICIO,
                                T.TASA_CONVENIO_FACTURACION,
                                T.MCC
                                ORDER BY    FECHA_GESTION) t)
      WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
      AND  numPagina * cantReg;

      SELECT
        count(registro) AS cantidad into v_cant
        FROM (SELECT ROWNUM AS registro, t.* FROM (
                      SELECT DISTINCT
                              GT.TRANSACCION                                                       AS SIDINCOMMING,
                              TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                    AS FECHA_GESTION
                            FROM  SIG.TBL_ESTADO_TRANSAC EP,
                                 (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID  = P.TRANSACCION)) 
                            WHERE     
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ESTADO_TRANSAC                         = EP.SID                AND
                                GT.ESTADO_PROCESO                        IN (3,4,5)               AND
                                 EP.SID   IN (1,2)                                    
                            GROUP BY
                                GT.TRANSACCION, 
                                GT.FECHA_ACTUALIZACION                         
                                ORDER BY    FECHA_GESTION)t);      

                    END;

                END IF;
                prfCursor:=voCursor;
                totReg:=v_cant;

            EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');

                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_GESTPORCUENTAPRESENTPG', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
            END;

END SP_SIG_GESTPORCUENTAPRESENTPG;

