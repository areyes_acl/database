create or replace PROCEDURE SP_SIG_GET_REPORTE_COMPENS_VI 
(
  desde               IN VARCHAR2,
  hasta               IN VARCHAR2,
  desdePDS            IN VARCHAR2,
  hastaPDS            IN VARCHAR2,
  filtro_operador      IN NUMBER,
  warning             OUT VARCHAR2,
  cod_error           OUT NUMBER,
  prfCursor           OUT SYS_REFCURSOR
)
AS
  voCursor SYS_REFCURSOR:=null;
BEGIN

  cod_error := 0;
  warning := 'Se ha ejecutado correctamente el SP_SIG_GET_REPORTE_COMPENS_VI';
  DBMS_OUTPUT.PUT_LINE('desde: '||desde); 
      DBMS_OUTPUT.PUT_LINE('hasta : '  || hasta );
         DBMS_OUTPUT.PUT_LINE('desdePDS : '  || desdePDS );
            DBMS_OUTPUT.PUT_LINE('hastaPDS : ' ||  hastaPDS );
  OPEN voCursor FOR
        
        SELECT 
            INCOMING.MIT as mit,
            INCOMING.NRO_TARJETA as nro_tarjeta,
            INCOMING.CODIGO_AUTORIZACION as codigo_autorizacion,
            substr(INCOMING.NOMBRE_UBIC_ACEP_TARJETA,0,30)as comercio,
            INCOMING.MCC as mcc,
            substr(INCOMING.DATOS_REFERENCIA_ADQUIRENTE, 2,6)as adquirente,
            INCOMING.COD_MONEDA_TRANSAC as moneda_origen,
            INCOMING.COD_MONEDA_CONCILIACION as moneda_conc,
            INCOMING.COD_MONEDA_FACTURACION as moneda_fact,
            INCOMING.MONTO_TRANSAC as monto_origen,
            INCOMING.MONTO_CONCILIACION as monto_conc,
            INCOMING.MONTO_FACTURACION as monto_fact,
            INCOMING.FECHA_INCOMING as fecha_incoming,
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,35,2) as respuesta_autorizac,
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,20,12) monto_autorizado, 
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,50,12) as monto_total_autor,
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,92,15) as comision_intercambio,
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,107,1) as cred_deb,
            TO_NUMBER(SUBSTR(INCOMING.DATOS_ADICIONALES_2,108,2)) as indicador_tasa_origen,
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,110,6) as tasa_conversion_moneda_origen,
            TO_NUMBER(SUBSTR(INCOMING.DATOS_ADICIONALES_2,116,2)) as ind_conversion_usd_clp,
            SUBSTR(INCOMING.DATOS_ADICIONALES_2,118,6) as tasa_conversion_usd_clp,            
            CONVERSION.DIVISOR as divisor
        FROM TBL_INCOMING INCOMING 
        INNER JOIN TBL_PDS PDS on (INCOMING.SID = PDS.TRANSACCION)
        LEFT JOIN TBL_CONVERSION_COMISIONES CONVERSION on 
        (CASE WHEN INCOMING.COD_MONEDA_TRANSAC = '840' 
            THEN TO_NUMBER(SUBSTR(INCOMING.DATOS_ADICIONALES_2,116,2)) 
            ELSE TO_NUMBER(SUBSTR(INCOMING.DATOS_ADICIONALES_2,108,2)) 
            END = CONVERSION.FACTOR
        )
        WHERE INCOMING.COD_MONEDA_TRANSAC != '152' 
        AND INCOMING.MIT IN ('05','06','07','25','26','27')
        AND INCOMING.FECHA_INCOMING >=TO_DATE(desdePDS, 'YY/MM/DD')                  
        AND INCOMING.FECHA_INCOMING <=TO_DATE(hastaPDS, 'YY/MM/DD');
        
        
    prfCursor:=voCursor;


EXCEPTION 

  WHEN OTHERS THEN                                             
    DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);                                                            
    DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || SQLCODE || ' ' || SQLERRM);     
    warning:=SQLERRM || DBMS_UTILITY.format_error_backtrace;    
    cod_error:=SQLCODE;  
    INSERT INTO LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) 
    VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_GET_REPORTE_COMPENS_VI', warning||cod_error );
    commit;
    
END SP_SIG_GET_REPORTE_COMPENS_VI;