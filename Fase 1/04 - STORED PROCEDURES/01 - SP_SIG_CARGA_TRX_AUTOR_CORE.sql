create or replace PROCEDURE       "SP_SIG_CARGA_TRX_AUTOR_CORE" ( warning   OUT VARCHAR2,
                                                           cod_error OUT NUMBER )
IS
  vocursor  SYS_REFCURSOR := NULL;
  prfcursor SYS_REFCURSOR := NULL;

BEGIN
    DECLARE
        seqaux                              sig.TMP_CORE_AUTOR.SID%TYPE := NULL;
        fecha_incoming                      sig.TMP_CORE_AUTOR.FECHA_INCOMING%TYPE;
        mit                                 sig.TMP_CORE_AUTOR.MIT%TYPE;
        rut                                 sig.TMP_CORE_AUTOR.RUT%TYPE;
        nro_tarjeta                         sig.TMP_CORE_AUTOR.NRO_TARJETA%TYPE;
        monto_transac                       sig.TMP_CORE_AUTOR.MONTO_TRANSAC%TYPE;
        fecha_hr_trasac                     sig.TMP_CORE_AUTOR.FECHA_HR_TRASAC%TYPE;
        mcc                                 sig.TMP_CORE_AUTOR.MCC%TYPE;
        datos_referencia_adquirente         sig.TMP_CORE_AUTOR.DATOS_REFERENCIA_ADQUIRENTE%TYPE;
        cod_identificacion_adquirente       sig.TMP_CORE_AUTOR.COD_IDENTIFICACION_ADQUIRENTE%TYPE;
        codigo_autorizacion                 sig.TMP_CORE_AUTOR.CODIGO_AUTORIZACION%TYPE;
        flag_anulacion                      sig.TMP_CORE_AUTOR.FLAG_ANULACION%TYPE;
        cantidad_cuotas                     sig.TMP_CORE_AUTOR.CANTIDAD_CUOTAS%TYPE;
        tipo_cuotas                         sig.TMP_CORE_AUTOR.TIPO_CUOTAS%TYPE;
        valor_cuota                         sig.TMP_CORE_AUTOR.VALOR_CUOTA%TYPE;
        comision_comercio                   sig.TMP_CORE_AUTOR.COMISION_COMERCIO%TYPE;
        nombre_comercio                     sig.TMP_CORE_AUTOR.NOMBRE_COMERCIO%TYPE;
        ciudad_comercio                     sig.TMP_CORE_AUTOR.CIUDAD_COMERCIO%TYPE;
        pais_comercio                       sig.TMP_CORE_AUTOR.PAIS_COMERCIO%TYPE;
        origen_transac                      sig.TMP_CORE_AUTOR.ORIGEN_TRANSAC%TYPE;
        bin_origen                          sig.TMP_CORE_AUTOR.BIN_ORIGEN%TYPE;
        imp_transaccion_visa                sig.TMP_CORE_AUTOR.IMP_TRANSACCION_VISA%TYPE;
        cod_moneda_visa                     sig.TMP_CORE_AUTOR.COD_MONEDA_VISA%TYPE;
        cod_moneda_org_visa                 sig.TMP_CORE_AUTOR.COD_MONEDA_ORG_VISA%TYPE;
        cod_moneda_con_visa                 sig.TMP_CORE_AUTOR.COD_MONEDA_CON_VISA%TYPE;
        fec_transac_visa                    sig.TMP_CORE_AUTOR.FEC_TRANSAC_VISA%TYPE;
        imp_operacion                       sig.TMP_CORE_AUTOR.IMP_OPERACION%TYPE;
        imp_conciliacion                    sig.TMP_CORE_AUTOR.IMP_CONCILIACION%TYPE;

    BEGIN
        OPEN vocursor FOR
          select 
          fecha_incoming,
          mit,
          rut,
          nro_tarjeta,
          monto_transac,
          fecha_hr_trasac,
          mcc,
          datos_referencia_adquirente,
          cod_identificacion_adquirente,
          codigo_autorizacion,
          flag_anulacion,
          cantidad_cuotas,
          tipo_cuotas,
          valor_cuota,
          comision_comercio,
          nombre_comercio,
          ciudad_comercio,
          pais_comercio,
          origen_transac,
          bin_origen,
          imp_transaccion_visa,
          cod_moneda_visa,
          cod_moneda_org_visa,
          cod_moneda_con_visa,
          fec_transac_visa,
          imp_operacion,
          imp_conciliacion
          from TMP_CORE_AUTOR;

        prfcursor := vocursor;

        LOOP
            FETCH prfcursor INTO fecha_incoming, mit, rut, nro_tarjeta, monto_transac, fecha_hr_trasac, mcc, datos_referencia_adquirente, cod_identificacion_adquirente, codigo_autorizacion, flag_anulacion, cantidad_cuotas, tipo_cuotas, valor_cuota, comision_comercio, nombre_comercio, ciudad_comercio, pais_comercio, origen_transac, bin_origen, imp_transaccion_visa, cod_moneda_visa, cod_moneda_org_visa, cod_moneda_con_visa, fec_transac_visa, imp_operacion, imp_conciliacion;

            EXIT WHEN prfcursor%NOTFOUND;

            SELECT SEQ_TBL_CORE_AUTOR.NEXTVAL
            INTO   seqaux
            FROM   DUAL;

            INSERT INTO TBL_CORE_AUTOR
            VALUES      ( seqaux,
                          fecha_incoming,
                          mit,
                          rut,
                          nro_tarjeta,
                          monto_transac,
                          fecha_hr_trasac,
                          mcc,
                          datos_referencia_adquirente,
                          cod_identificacion_adquirente,
                          codigo_autorizacion,
                          flag_anulacion,
                          cantidad_cuotas,
                          tipo_cuotas,
                          valor_cuota,
                          comision_comercio,
                          nombre_comercio,
                          ciudad_comercio,
                          pais_comercio,
                          origen_transac,
                          bin_origen,
                          imp_transaccion_visa,
                          cod_moneda_visa,
                          cod_moneda_org_visa,
                          cod_moneda_con_visa,
                          fec_transac_visa,
                          imp_operacion,
                          imp_conciliacion);

            COMMIT;

        END LOOP;

        dbms_output.Put_line( 'salida loop' );

        CLOSE prfcursor;

        dbms_output.Put_line( 'cierre cursor' );
    END;

    --tabla TMP_CORE_AUTOR SE LIMPIA    
    EXECUTE IMMEDIATE 'truncate table SIG.TMP_CORE_AUTOR';

    COMMIT;

EXCEPTION
  WHEN no_data_found THEN
             dbms_output.Put_line( 'NO HAY REGISTROS EN TABLA SIG.TMP_CORE_AUTOR' ); WHEN OTHERS THEN
             warning := SQLERRM;

             cod_error := SQLCODE;

             dbms_output.Put_line( 'ERROR : '
                                   ||warning
                                   ||' SQLCODE : '
                                   ||cod_error );

             --insercion en tabla log error en caso de algun error desconocido          
             INSERT INTO LOG_ERROR
                         (sid,
                          fecha_insercion,
                          nombre_sp,
                          msg_error)
             VALUES      ( sig.seq_log_error.NEXTVAL,
                          SYSDATE,
                          'SP_SIG_CARGA_TRX_AUTOR_CORE',
                          cod_error
                          ||warning );

END;