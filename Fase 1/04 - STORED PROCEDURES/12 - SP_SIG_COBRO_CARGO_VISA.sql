create or replace PROCEDURE       "SP_SIG_COBRO_CARGO_VISA" (warning       OUT VARCHAR2,
                                             cod_error      OUT NUMBER
                                                       
)
IS
   
        voCursor  Sys_RefCursor:=null;
        prfCursor Sys_RefCursor:=null;
        countrySid    NUMBER;
-- Proyecto: SIG
-- Nombre Objeto:sp_Sig_Carga_cobro_cargo_visa
-- By: Developer || ACL PTM | 10/07/2013
-- Versión : 1.1
-- Descripcion:carga tbl_cobro_cargo_visa      desde tmp_cobro_cargo_visa


BEGIN
      DECLARE





           SID                                       SIG.TMP_COBRO_CARGO_VISA.SID%TYPE                        ;
           MIT                                       SIG.TMP_COBRO_CARGO_VISA.MIT%TYPE                        ;
           CODIGO_FUNCION                            SIG.TMP_COBRO_CARGO_VISA.CODIGO_FUNCION%TYPE             ;
           DESTINATION_BIN                           SIG.TMP_COBRO_CARGO_VISA.DESTINATION_BIN%TYPE            ;
           SOURCE_BIN                                SIG.TMP_COBRO_CARGO_VISA.SOURCE_BIN%TYPE                 ;
           REASON_CODE                               SIG.TMP_COBRO_CARGO_VISA.REASON_CODE%TYPE                ;
           COUNTRY_CODE                              SIG.TMP_COBRO_CARGO_VISA.COUNTRY_CODE%TYPE               ;
           EVENT_DATE                                SIG.TMP_COBRO_CARGO_VISA.EVENT_DATE%TYPE                 ;
           ACCOUNT_NUMBER                            SIG.TMP_COBRO_CARGO_VISA.ACCOUNT_NUMBER%TYPE             ;
           ACCOUNT_NUMBER_EXTENSION                  SIG.TMP_COBRO_CARGO_VISA.ACCOUNT_NUMBER_EXTENSION%TYPE   ;
           DESTINATION_AMOUNT                        SIG.TMP_COBRO_CARGO_VISA.DESTINATION_AMOUNT%TYPE         ;
           DESTINATION_CURRENCY_CODE                 SIG.TMP_COBRO_CARGO_VISA.DESTINATION_CURRENCY_CODE%TYPE  ;
           SOURCE_AMOUNT                             SIG.TMP_COBRO_CARGO_VISA.SOURCE_AMOUNT%TYPE              ;
           SOURCE_CURRENCY_CODE                      SIG.TMP_COBRO_CARGO_VISA.SOURCE_CURRENCY_CODE%TYPE       ;
           MESSAGE_TEXT                              SIG.TMP_COBRO_CARGO_VISA.MESSAGE_TEXT%TYPE               ;
           SETTLEMENT_FLAG                           SIG.TMP_COBRO_CARGO_VISA.SETTLEMENT_FLAG%TYPE            ;
           TRANSACTION_IDENTIFIER                    SIG.TMP_COBRO_CARGO_VISA.TRANSACTION_IDENTIFIER%TYPE     ;
           RESERVED                                  SIG.TMP_COBRO_CARGO_VISA.RESERVED%TYPE                   ;
           CENTRAL_PROCESSING_DATE                   SIG.TMP_COBRO_CARGO_VISA.CENTRAL_PROCESSING_DATE%TYPE    ;
           REIMBURSEMENT_ATTRIBUTE                   SIG.TMP_COBRO_CARGO_VISA.REIMBURSEMENT_ATTRIBUTE%TYPE    ;
           OPERADOR                                  SIG.TMP_COBRO_CARGO_VISA.OPERADOR%TYPE                   ;
           DATOS_ADICIONALES                         SIG.TMP_COBRO_CARGO_VISA.DATOS_ADICIONALES%TYPE          ;

     BEGIN

      dbms_output.enable(NULL);

                OPEN voCursor FOR
                SELECT
                       MIT,
                       CODIGO_FUNCION,
                       DESTINATION_BIN,
                       SOURCE_BIN,
                       REASON_CODE,
                       COUNTRY_CODE,
                       EVENT_DATE,
                       ACCOUNT_NUMBER,
                       ACCOUNT_NUMBER_EXTENSION,
                       DESTINATION_AMOUNT,
                       DESTINATION_CURRENCY_CODE,
                       SOURCE_AMOUNT,
                       SOURCE_CURRENCY_CODE,
                       MESSAGE_TEXT,
                       SETTLEMENT_FLAG,
                       TRANSACTION_IDENTIFIER,
                       RESERVED,
                       CENTRAL_PROCESSING_DATE,
                       REIMBURSEMENT_ATTRIBUTE,
                       OPERADOR,
                       DATOS_ADICIONALES
                 FROM  SIG.TMP_COBRO_CARGO_VISA;
                    prfCursor:=voCursor;

        LOOP                                                                                           
                    FETCH prfCursor                                                                              
                    INTO
                      MIT,
                       CODIGO_FUNCION,
                       DESTINATION_BIN,
                       SOURCE_BIN,
                       REASON_CODE,
                       COUNTRY_CODE,
                       EVENT_DATE,
                       ACCOUNT_NUMBER,
                       ACCOUNT_NUMBER_EXTENSION,
                       DESTINATION_AMOUNT,
                       DESTINATION_CURRENCY_CODE,
                       SOURCE_AMOUNT,
                       SOURCE_CURRENCY_CODE,
                       MESSAGE_TEXT,
                       SETTLEMENT_FLAG,
                       TRANSACTION_IDENTIFIER,
                       RESERVED,
                       CENTRAL_PROCESSING_DATE,
                       REIMBURSEMENT_ATTRIBUTE,
                       OPERADOR,
                       DATOS_ADICIONALES ;

                    EXIT WHEN prfCursor%NOTFOUND; 

              --SOLO PRUEBAS
           DBMS_OUTPUT.PUT_LINE('salida fetch');

              --FIN PRUEBAS             
              IF FUNC_SIG_IS_NUMBER(COUNTRY_CODE)
                THEN
                 SELECT SID INTO countrySid FROM TBL_PAIS WHERE NUM_PAIS = TO_NUMBER(COUNTRY_CODE);
                ELSE 
                 SELECT SID INTO countrySid FROM TBL_PAIS WHERE COD_PAIS = TRIM(COUNTRY_CODE);
              END IF;
              
             select case 
               when exists(SELECT SID FROM TBL_BINES_NACIONALES WHERE OBIN = SOURCE_BIN)
               then 2
               else 3
             end  into OPERADOR
             from dual;


                    DBMS_OUTPUT.PUT_LINE('INSERT A LA TBL_COBRO_CARGO_VISA');
                    INSERT INTO TBL_COBRO_CARGO_VISA (
                    SID,
                      MIT,
                      CODIGO_FUNCION,
                      DESTINATION_BIN,
                      SOURCE_BIN,
                      REASON_CODE,
                      COUNTRY_CODE,
                      EVENT_DATE,
                      ACCOUNT_NUMBER,
                      ACCOUNT_NUMBER_EXTENSION,
                      DESTINATION_AMOUNT,
                      DESTINATION_CURRENCY_CODE,
                      SOURCE_AMOUNT,
                      SOURCE_CURRENCY_CODE,
                      MESSAGE_TEXT,
                      SETTLEMENT_FLAG,
                      TRANSACTION_IDENTIFIER,
                      RESERVED,
                      CENTRAL_PROCESSING_DATE,
                      REIMBURSEMENT_ATTRIBUTE,
                      OPERADOR,
                      FECHA_INCOMING,
                      DATOS_ADICIONALES
                      ) 

                      VALUES 

                      (SEQ_TBL_COBRO_CARGO_VISA.NEXTVAL,
                      TRIM(MIT),
                      TRIM(CODIGO_FUNCION),
                      TRIM(DESTINATION_BIN),
                      TRIM(SOURCE_BIN),
                      (SELECT SID FROM TBL_COD_RAZON_VISA WHERE COD_MOTIVO_INI = TRIM(REASON_CODE) 
                      AND ACCION_TRANSAC = (SELECT SID FROM TBL_ACCION_TRANSAC WHERE XKEY = TRIM(MIT) || '_' || TRIM(CODIGO_FUNCION) )),
                      countrySid,
                      TO_DATE(EVENT_DATE, 'YYMMDD'),
                      TRIM(ACCOUNT_NUMBER),
                      TRIM(ACCOUNT_NUMBER_EXTENSION),
                      TO_NUMBER(DESTINATION_AMOUNT),
                      TRIM(DESTINATION_CURRENCY_CODE),
                      TO_NUMBER(SOURCE_AMOUNT),
                      TRIM(SOURCE_CURRENCY_CODE),
                      TRIM(MESSAGE_TEXT),
                      TRIM(SETTLEMENT_FLAG),
                      TRIM(TRANSACTION_IDENTIFIER),
                      TRIM(RESERVED),
                      TO_DATE(CENTRAL_PROCESSING_DATE, 'YYMMDD'),
                      TRIM(REIMBURSEMENT_ATTRIBUTE),
                      TO_NUMBER(OPERADOR),
                      SYSDATE,
                      TRIM(DATOS_ADICIONALES));


                END LOOP;
                DBMS_OUTPUT.PUT_LINE('salida loop');
                CLOSE prfCursor;
                DBMS_OUTPUT.PUT_LINE('cierre cursor');
     END;            

  --tabla SIG.TMP_CPAGO_VISA SE LIMPIA
 execute immediate 'truncate table SIG.TMP_COBRO_CARGO_VISA';
 COMMIT;
EXCEPTION
WHEN  NO_DATA_FOUND     THEN
            DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS EN TABLA SIG.TMP_COBRO_CARGO_VISA');
WHEN  OTHERS THEN
      warning:=SQLERRM;   
      cod_error:=SQLCODE;
      DBMS_OUTPUT.PUT_LINE('ERROR : '||warning||' SQLCODE : '||cod_error);   
--insercion en tabla log error en caso de algun error desconocido          
     INSERT INTO LOG_ERROR (SID, 
                       FECHA_INSERCION, 
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_COBRO_CARGO_VISA',
                        cod_error||warning );
END SP_SIG_COBRO_CARGO_VISA;

