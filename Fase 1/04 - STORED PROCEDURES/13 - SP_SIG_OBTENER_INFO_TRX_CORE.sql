create or replace PROCEDURE       "SP_SIG_OBTENER_INFO_TRX_CORE" (
                                                              id_sid           IN NUMBER,
                                                              warning       OUT VARCHAR2,
                                                              cod_error     OUT NUMBER,
                                                              prfcursor     OUT SYS_REFCURSOR)
IS  
/******************************************************************************
   NAME:       SP_SIG_OBTENER_INFO_TRX_AVANC
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.1        XX-12-2019   acl(PTM)       

   NOTES:  se agregan los nuevos campos solicitados a la consulta TC2K_CUENTA, COD_SUCURSAL, RUT_CLIENTE, NUM_DOCUMENTO y NUM_CUOTAS

      Object Name:     SP_SIG_OBTENER_INFO_TRX_AVANC
      Sysdate:          XX-12-2019 
******************************************************************************/
  vocursor                 SYS_REFCURSOR := NULL;
BEGIN
    cod_error := 0;
    warning := 'Proceso SP_SIG_OBTENER_INFO_TRX_CORE ejecutado correctamente ';

    BEGIN
        OPEN vocursor FOR    
          SELECT
            SID AS sid,
            NVL(TO_CHAR(FECHA_INCOMING,'DD-MON-RR'),' ') AS fechaTransafer,
            CODIGO_AUTORIZACION AS codAutor,
            DATOS_REFERENCIA_ADQUIRENTE AS numTrx,
            NRO_TARJETA AS numTrajeta,
            NVL(MONTO_TRANSAC,0) AS monto,
            COD_IDENTIFICACION_ADQUIRENTE as codigosucursal,
            RUT as rutcliente,
            CANTIDAD_CUOTAS as numcuotas
          FROM TBL_CORE_AUTOR
          WHERE SID = id_sid;

        prfcursor := vocursor;
 END;

EXCEPTION
  WHEN no_data_found THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             dbms_output.Put_line( 'NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS' ); WHEN OTHERS THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             INSERT INTO sig.LOG_ERROR
                         (sid,
                          fecha_insercion,
                          nombre_sp,
                          msg_error)
             VALUES      (sig.seq_log_error.NEXTVAL,
                          SYSDATE,
                          'SP_SIG_OBTENER_INFO_TRX_CORE',
                          cod_error
                          || '-'
                          || warning);

             dbms_output.Put_line( 'ERROR : '
                                   || ' '
                                   || cod_error
                                   || '-'
                                   || warning );

END SP_SIG_OBTENER_INFO_TRX_CORE;
