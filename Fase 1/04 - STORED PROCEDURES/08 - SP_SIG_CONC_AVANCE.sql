create or replace PROCEDURE "SP_SIG_CONC_AVANCE"  ( warning   OUT VARCHAR2,
                                                   cod_error OUT NUMBER )
IS 
/******************************************************************************
   NAME:       SP_SIG_CONC_AVANCE
   PURPOSE:

   REVISIONS:
   Ver        Date      Author Description
   ---------  --------- ------ ---------------------------------------------
   1.1.0     23-12-2019 acl    agrega funciones trim y translate para evitar errores de espacios en registros
   NOTES:
      Object Name:     SP_SIG_CONC_AVANCE
******************************************************************************/
BEGIN

    DECLARE   
          
     -- DATOS CORE
      var_sid_core NUMBER;
      codigo_autor_core NUMBER;
      datos_ref_adq VARCHAR2(20);
      monto_transac_core VARCHAR2(50);
      fecha_transac_core VARCHAR2(50);
      fecha_inc_core VARCHAR2(50);
      tarjeta_core VARCHAR2(20 BYTE);
      monto_core_avt VARCHAR2(50);
      
      --DATOS BICE
      var_sid_bice NUMBER;
      fecha_bice VARCHAR2(50);
      monto_bice NUMBER;
      numtrx_bice VARCHAR2(50);
      var_ide_oper VARCHAR2(50);
      fecha_registro_bice VARCHAR2(50 BYTE);
      fecha_insert_bice VARCHAR2(50 BYTE);
      var_auth_bice VARCHAR2(50 BYTE);
      
      sidBiceCore NUMBER;
      num_ref VARCHAR2(20);
      ide_oper_cliente VARCHAR(50 BYTE);
      monto_avt VARCHAR(50 BYTE);
      fecha_instr VARCHAR2(50);
      var_cod_auth_core number;
      var_conc_avt_bice number;
      var_conc_avt_core number;
      
      var_tarjeta_visa VARCHAR2(20 BYTE);
      estadoBice NUMBER;
      
      
      sidCoreBice   NUMBER;
      auth_core_bice VARCHAR2(50);
      var_monto_core VARCHAR2(50);
      var_rut_benef NUMBER;
      id_oper_cl VARCHAR2(50);
      var_fecha_core VARCHAR2(50);
      estadoCore NUMBER;
      var_tarjeta_core VARCHAR2(20 BYTE);
      operador_visa NUMBER;
      var_cod_aut_bice VARCHAR2(50);
      
      contador NUMBER;
      
      CURSOR cursorCore IS
       SELECT CA.SID,
        CA.DATOS_REFERENCIA_ADQUIRENTE AS REFADQ,
        TO_NUMBER(CA.MONTO_TRANSAC) AS MONTO,
        CA.FECHA_HR_TRASAC AS FEC_HR_TRX,
        TO_DATE(CA.FECHA_INCOMING, 'DD-MM-YY') AS FECHAINC,
        CA.CODIGO_AUTORIZACION AS COD_AUT
        --SUBSTR(CA.NRO_TARJETA, -4) AS NTARJETAC
        FROM TBL_CORE_AUTOR CA
        WHERE TO_CHAR(CA.FECHA_INCOMING, 'YYYYMMDD') = TO_CHAR(SYSDATE, 'YYYYMMDD')
        AND MIT = 9
        AND CA.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE EMPRESA = 'Salcobrand'or EMPRESA = 'PreUnic');
      
      
      /* Cursosr Bice */
      CURSOR cursorBice IS
        SELECT  
          AB.SID AS SID_BICE,
          TO_CHAR(AB.FECHA_INSTRUCCION, 'DD-MON-YY') AS FECHA,
          TRIM(TO_NUMBER(translate(AB.MONTO, chr(10) || chr(13) || chr(09), ' '))) AS MONTO,
          REPLACE(LTRIM(AB.NUM_OPER_PROG,'0'),' ','') AS NUM_TRX,
          AB.ID_OPER_CLIENTE AS IDE_OPER,
          TO_CHAR(AB.FECHA_REGISTRO, 'YYYYMMDD') FECHA_REGISTRO,
          TO_CHAR(AB.FECHA_INSTRUCCION,'DD-MON-RR') AS FECHA_INSERT,
          AB.ID_OPER_CLIENTE AS CODAUTH
        FROM SIG.TBL_TRANSAC_AVA_BICE AB
        WHERE TO_CHAR(AB.FECHA_REGISTRO, 'YYYYMMDD')= TO_CHAR(SYSDATE, 'YYYYMMDD') AND AB.TIPO_PRODUCTO = 1 
        AND AB.COD_ESTADO in (Select DISTINCT  trim(regexp_substr(VALOR,'[^,]+', 1, level)) FROM TBL_PRTS t WHERE t.COD_GRUPO_DATO='CRON_AVT' AND t.COD_DATO='PROCESS_BICE_ONLY_STATUS' connect by regexp_substr(VALOR, '[^,]+', 1, level) is not null);
      
            
      BEGIN
      cod_error := 0;
      warning := 'Proceso SP_SIG_CONC_AVANCE ejecutado';

       /*=====================CURSOR BICE=========================================*/
        OPEN cursorBice;
        LOOP
          fetch cursorBice into 
            var_sid_bice, 
            fecha_bice, 
            monto_bice, 
            numtrx_bice, 
            var_ide_oper,
            fecha_registro_bice, 
            fecha_insert_bice,
            var_auth_bice;
          EXIT WHEN cursorBice%NOTFOUND;
          dbms_output.Put_line('loop cursor Bice');
          sidCoreBice := 0;
          
          dbms_output.Put_line('var_sid_bice: '|| var_sid_bice ||' fecha_bice:  ' ||fecha_bice||' monto_bice: ' ||monto_bice|| ' numtrx_bice:' ||numtrx_bice|| ' var_ide_oper: '||var_ide_oper|| ' fecha_registro_bice: ' ||fecha_registro_bice || ' fecha_insert_bice : ' ||fecha_insert_bice || ' var_auth_bice: ' || var_auth_bice);
         begin
          SELECT CA.SID, CA.CODIGO_AUTORIZACION, CA.MONTO_TRANSAC, CA.FECHA_HR_TRASAC 
          INTO sidCoreBice, auth_core_bice, var_monto_core, var_fecha_core
          FROM TBL_CORE_AUTOR CA WHERE CA.MONTO_TRANSAC = monto_bice AND CA.CODIGO_AUTORIZACION = var_ide_oper AND CA.FECHA_HR_TRASAC = fecha_bice
          GROUP BY CA.SID, CA.CODIGO_AUTORIZACION, CA.MONTO_TRANSAC, CA.FECHA_HR_TRASAC;
         exception
          WHEN NO_DATA_FOUND THEN
          sidCoreBice := 0;
         end;
            ---dbms_output.Put_line('sidBiceTrebol: '||sidBiceTrebol || ' var_sid_trebol: '||var_sid_trebol || ' rut_clie: '||rut_clie || ' monto_bic: '||monto_bic || ' sid_opr_cl: '||sid_opr_cl);
           if(sidCoreBice > 0)then
              estadoCore := 1;
             elsif(sidCoreBice = 0)then
              estadoCore := 2;
               
           end if;
           
          begin
           
           dbms_output.Put_line(sidCoreBice || ' - ' || var_sid_bice);
           select  sid into var_conc_avt_core from TBL_CONC_AVANCE where (SID_CORE = sidCoreBice and sidCoreBice >0) or (SID_BICE = var_sid_bice);
           dbms_output.put_line('var_conc_avt_core: '||var_conc_avt_core);
           exception
          WHEN NO_DATA_FOUND THEN
          --dbms_output.put_line('sid_bice_trebol: '||sidBiceTrebol||'--'||'var_sid_trebol: '||var_sid_trebol);
          if(sidCoreBice>0 or var_sid_bice>0)then
            dbms_output.Put_line('delete: SID_TREBOL '||sidCoreBice||' SID_CC '||var_sid_bice);
            delete from TBL_CONC_AVANCE where ((SID_CORE = sidCoreBice and sidCoreBice >0) or (SID_BICE = var_sid_bice)) and ESTADO_CONTABLE not like '1';
            dbms_output.put_line(SQL%ROWCOUNT);
          end if;
          var_conc_avt_core := 0;
         end;
           dbms_output.Put_line('var_conc_avt_core: '||var_conc_avt_core);
            update TBL_CONC_AVANCE set  SID_CORE = sidCoreBice, SID_BICE=var_sid_bice, SID_ESTADO=estadoCore, FECHA=fecha_bice, FECHA_REGISTRO= sysdate
            where sid = var_conc_avt_core;
            if(SQL%ROWCOUNT = 0)THEN
              select count(*) into contador from TBL_CONC_AVANCE CA where SID_CORE=sidCoreBice and CA.SID_BICE=var_sid_bice;
              dbms_output.Put_line('Encuentra duplicados: '||contador);
              if(contador = 0)THEN
              
             --dbms_output.Put_line('Datos a insertar: '||sidBiceTrebol||'-'||var_sid_trebol||'-'||estadoTrebol||'-'||fecha_reg_trebol||'-'||num_ref_trans||'-'|| monto_trebol ||'-'|| rut_cliente ||'-'|| SYSDATE);
             
              
                insert into TBL_CONC_AVANCE(SID, SID_CORE, SID_BICE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO)
                values(SEQ_TBL_CONC_AVANCE.nextval,sidCoreBice,var_sid_bice,estadoCore,fecha_bice,monto_bice, SYSDATE);
                
                --dbms_output.put_line(sidBiceTrebol||'-'||var_sid_trebol||'-'||estadoTrebol||'-'||fecha_reg_trebol||'-'||num_ref_trans||'-'||monto_trebol||'-'||rut_cliente||'-'||SYSDATE);
              end if;
            end if;
          --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorBice;
      dbms_output.Put_line('#################################################### FIN CURSOR BICE');
      
      
    /*=====================CURSOR CORE=========================================*/
     OPEN cursorCore;
        LOOP
         FETCH cursorCore INTO 
         var_sid_core, 
         datos_ref_adq, 
         monto_core_avt, 
         fecha_transac_core, 
         fecha_inc_core, 
         codigo_autor_core;
         EXIT WHEN cursorCore%NOTFOUND;
         dbms_output.Put_line('loop cursor Core');
         sidBiceCore := 0;
         var_monto_core:= 0;
         dbms_output.Put_line('var_sid_core :'||var_sid_core ||'-'||'datos_ref_adq :'||datos_ref_adq ||'-'||'monto_core_avt :'||monto_core_avt||'-'||'fecha_transac_core :'||fecha_transac_core||'-'||'fecha_inc_core :'||fecha_inc_core||'-'||'codigo_autor_core :'||codigo_autor_core);
       begin
       
       
        SELECT AVA.sid, AVA.ID_OPER_CLIENTE, AVA.MONTO, AVA.FECHA_INSTRUCCION
        INTO sidBiceCore, ide_oper_cliente, monto_avt, fecha_instr
        FROM TBL_TRANSAC_AVA_BICE AVA  WHERE AVA.MONTO = monto_core_avt AND AVA.ID_OPER_CLIENTE = codigo_autor_core 
        AND to_char(AVA.FECHA_INSTRUCCION, 'DD-MON-YY') = fecha_transac_core  
        GROUP BY AVA.sid, AVA.ID_OPER_CLIENTE, AVA.MONTO, AVA.FECHA_INSTRUCCION;
        --dbms_output.Put_line('sid :'||sidTrebolBice||'-'||'monto'|| var_monto_bice||'-'||'NUM_REF_TRANS'|| id_oper_cl||'-'||'rut'|| var_rut_benef);
       exception
        WHEN NO_DATA_FOUND THEN
        sidBiceCore := 0;
        
        
       end;
         --dbms_output.Put_line('var_monto_bice: '||var_monto_bice ||' sidTrebolBice: '|| sidTrebolBice || ' id_oper_cl: '||id_oper_cl|| ' rut_beneficiario: '||var_rut_benef);      
     

       if(sidBiceCore > 0)then
          estadoCore := 1;
          
          else
              estadoCore := 3;
              operador_visa := 0;
       end if;
        --dbms_output.Put_line('estadoBice: '||estadoBice); 
        --dbms_output.Put_line('var_sid_bice: '||var_sid_bice ||'sidTrebolBice: '||sidTrebolBice);
       begin
       
       dbms_output.Put_line(sidBiceCore || ' - ' || var_sid_core);
         select  sid into var_conc_avt_bice from TBL_CONC_AVANCE 
         where (SID_BICE = sidBiceCore and sidBiceCore >0) or (SID_CORE = var_sid_core);
         exception
        WHEN NO_DATA_FOUND THEN
        var_conc_avt_bice := 0;
       end;
       
         dbms_output.Put_line('---->>>var_conc_avt_bice: '||var_conc_avt_bice);        
        update TBL_CONC_AVANCE set sid_core = var_sid_core, SID_BICE = sidBiceCore, sid_estado = estadoCore
          where sid = var_conc_avt_bice;
          if(SQL%ROWCOUNT = 0)THEN
          
           insert into TBL_CONC_AVANCE(SID, SID_CORE, SID_BICE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO)
           --values(SEQ_TBL_CONC_VISA_CORE.nextval,sidCoreVisa,var_sid_visa,estadoVisa,fecha_inc,monto_transac, SYSDATE, 0); 
           values(SEQ_TBL_CONC_AVANCE.nextval,var_sid_core,sidBiceCore,estadoBice,fecha_transac_core, monto_core_avt, SYSDATE);  
          end if;
        --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorCore;
      dbms_output.Put_line('Fin cursor Core');
      dbms_output.Put_line('##############################################################################  FIN CURSOR CORE ');
      
 /*==============================================================*/
--dbms_output.Put_line('Final del proceso');
--Sin este commit, el proceso no deja las conciliaciones registradas, no se cae, no da error, simplemente no las deja registradas
  COMMIT;
  END;
     
    EXCEPTION     
         WHEN NO_DATA_FOUND THEN
         dbms_output.Put_line( 'NO HAY REGISTROS EN TABLA' ); 
         warning := warning || '- NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS';
        WHEN OTHERS THEN                                                                      
                 warning := SQLERRM || ' - ' || DBMS_UTILITY.format_error_backtrace;
                 cod_error := SQLCODE;
                 dbms_output.Put_line( 'ERROR : ' || warning ||' SQLCODE : ' || cod_error );
                 dbms_output.Put_line( 'DESCRIPTION: ' || DBMS_UTILITY.format_error_backtrace);
                
                INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CONC_AVANCE', cod_error || '-' || warning);
                COMMIT;

END SP_SIG_CONC_AVANCE;