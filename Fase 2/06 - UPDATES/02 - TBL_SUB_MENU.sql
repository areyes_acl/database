UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') 
WHERE nombre_sub_menu = 'Rechazos diarios' OR nombre_sub_menu = 'Rechazos Diarios';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') 
WHERE nombre_sub_menu = 'Objeciones diarias' OR nombre_sub_menu = 'Objeciones Diarias';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') 
WHERE nombre_sub_menu = 'B�squeda de Gestiones' OR nombre_sub_menu = 'Busqueda de Gestiones';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') 
WHERE nombre_sub_menu = 'Gestiones Diarias' OR nombre_sub_menu = 'Gestiones diarias';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') 
WHERE nombre_sub_menu = 'Informe de Compensaciones' OR nombre_sub_menu = 'Flujos de pagos futuros';


UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas Generales') 
WHERE nombre_sub_menu = 'Cuadratura transacciones diarias' OR nombre_sub_menu = 'Cuadratura Diaria Visa';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas Generales') 
WHERE nombre_sub_menu = 'Cuadratura transacciones mensuales' OR nombre_sub_menu = 'Cuadratura Mensual Visa';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas Generales') 
WHERE nombre_sub_menu = 'Log de Procesos';


UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Gestiones VISA') 
WHERE nombre_sub_menu = 'Transacciones';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Gestiones VISA') 
WHERE nombre_sub_menu = 'Transacciones Pendientes Incoming';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Gestiones VISA') 
WHERE nombre_sub_menu = 'Cargos/Abonos Pendientes';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Gestiones VISA') 
WHERE nombre_sub_menu = 'Cobros de Cargo Visa';



UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Conciliaci�n') 
WHERE nombre_sub_menu = 'Conciliaci�n Avances con Transferencia';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Conciliaci�n') 
WHERE nombre_sub_menu = 'Conciliaci�n Visa' OR nombre_sub_menu = 'Conciliaci�n VISA';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Conciliaci�n') 
WHERE nombre_sub_menu = 'Conciliaci�n SERVIPAG' OR nombre_sub_menu = 'Conciliaci�n Servipag';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Conciliaci�n') 
WHERE nombre_sub_menu = 'Conciliaci�n SALCOBRAND/PREUNIC' OR nombre_sub_menu = 'Conciliaci�n Salcobrand/Preunic' ;

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Conciliaci�n') 
WHERE nombre_sub_menu = 'Conciliaci�n KHIPU' OR nombre_sub_menu = 'Conciliaci�n Khipu';


UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Par�metros' OR nombre_menu = 'Parametros') 
WHERE nombre_sub_menu = 'C�digos de raz�n' OR nombre_sub_menu = 'C�digos de Raz�n';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'C�digo de raz�n VISA' 
WHERE nombre_sub_menu = 'C�digos de raz�n' OR nombre_sub_menu = 'C�digos de Raz�n';


UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Pagos VISA') 
WHERE nombre_sub_menu = 'Registrar Pagos VISA' OR nombre_sub_menu = 'Registrar Pagos Visa';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Pagos VISA') 
WHERE nombre_sub_menu = 'Autorizar Pagos' OR nombre_sub_menu = 'Autorizar pagos';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Pagos VISA') 
WHERE nombre_sub_menu = 'Consultar Pagos' OR nombre_sub_menu = 'Consultar pagos';


UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Conciliaci�n VISA' 
WHERE nombre_sub_menu = 'Conciliaci�n Visa' OR nombre_sub_menu = 'Conciliacion Visa';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Cobros de Cargo VISA' 
WHERE nombre_sub_menu = 'Cobros de Cargo Visa';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Cuadratura Diaria Visa' 
WHERE nombre_sub_menu = 'Cuadratura transacciones diarias';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Cuadratura Mensual Visa' 
WHERE nombre_sub_menu = 'Cuadratura transacciones mensuales';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Compensaciones de Pagos VISA' 
WHERE nombre_sub_menu = 'Compensaciones de Pagos Visa';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Registrar Pagos Visa' 
WHERE nombre_sub_menu = 'Registrar Pagos';

UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Compensaciones de VISA' WHERE nombre_sub_menu = 'Compensaciones de Pagos VISA';
UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Pagos VISA' WHERE nombre_sub_menu = 'Pagos Visa';

UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') WHERE nombre_sub_menu = 'Cuadratura Diaria Visa' ;
UPDATE TBL_SUB_MENU SET sid_menu = (SELECT SID FROM TBL_MENU WHERE nombre_menu = 'Consultas VISA') WHERE nombre_sub_menu = 'Cuadratura Mensual Visa' ;
UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Cuadratura Diaria' WHERE nombre_sub_menu = 'Cuadratura Diaria Visa' ;
UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Cuadratura Mensual' WHERE nombre_sub_menu = 'Cuadratura Mensual Visa' ;
UPDATE TBL_SUB_MENU SET nombre_sub_menu = 'Registrar Pagos' WHERE nombre_sub_menu = 'Registrar Pagos Visa' ;

COMMIT;