 CREATE TABLE "SIG"."TBL_LOG_OUT_PREUNIC" 
   (	"SID" NUMBER(8,0) NOT NULL ENABLE, 
	"FECHA" DATE NOT NULL ENABLE, 
	"FILE_NAME" VARCHAR2(50 BYTE), 
	"FILE_FLAG" VARCHAR2(2 BYTE), 
	"FILE_TS" TIMESTAMP (6), 
	 CONSTRAINT "PK_TBL_LOG_OUT_PREUNIC" PRIMARY KEY ("SID") ENABLE
   );

   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_PREUNIC"."SID" IS 'Corresponde al id de la tabla TBL_LOG_OUT_PREUNIC';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_PREUNIC"."FECHA" IS 'Corresponde a la fecha de Procesamiento del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_PREUNIC"."FILE_NAME" IS 'Corresponde al nombre del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_PREUNIC"."FILE_FLAG" IS 'Corresponde al flag de procesamiento creado 0:subido 1:-1:error';
   COMMENT ON COLUMN "SIG"."TBL_LOG_OUT_PREUNIC"."FILE_TS" IS 'Corresponde a la estampa de tiempo del proceso del archivo';
   COMMIT;