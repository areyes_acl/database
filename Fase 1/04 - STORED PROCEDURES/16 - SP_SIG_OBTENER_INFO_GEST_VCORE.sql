create or replace PROCEDURE       "SP_SIG_OBTENER_INFO_GEST_VCORE" (
                                                              id_sid           IN NUMBER,
                                                              warning       OUT VARCHAR2,
                                                              cod_error     OUT NUMBER,
                                                              prfcursor     OUT SYS_REFCURSOR)
IS  
/******************************************************************************
   NAME:       SP_SIG_OBTENER_INFO_GEST_VCORE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        XX-11-2018   acl(PTM)       

   NOTES:

      Object Name:     SP_SIG_OBTENER_INFO_GEST_VCORE
      Sysdate:          XX-11-2018 
******************************************************************************/
  vocursor                 SYS_REFCURSOR := NULL;
BEGIN
    cod_error := 0;
    warning := 'Proceso SP_SIG_OBTENER_INFO_GEST_VCORE ejecutado correctamente ';

    BEGIN
        OPEN vocursor FOR    
          SELECT 
                G.SID AS sid,
                G.SID_CONC as sidConci,
                G.FECHA as fecha,
                CONCAT(U.NOMBRE,CONCAT(' ',U.APEPAT)) as sidUser,
                G.COMENTARIO as comentario,
                G.ESTADO as estado
          FROM TBL_CONC_VISA_CORE_GEST G
          LEFT JOIN TBL_USUARIO_IG U ON U.SID = G.SID_USER
          WHERE G.SID_CONC = id_sid
          ORDER BY SID DESC;

        prfcursor := vocursor;
 END;

EXCEPTION
  WHEN no_data_found THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             dbms_output.Put_line( 'NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS' ); WHEN OTHERS THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             INSERT INTO sig.LOG_ERROR
                         (sid,
                          fecha_insercion,
                          nombre_sp,
                          msg_error)
             VALUES      (sig.seq_log_error.NEXTVAL,
                          SYSDATE,
                          'SP_SIG_OBTENER_INFO_GEST_VCORE',
                          cod_error
                          || '-'
                          || warning);

             dbms_output.Put_line( 'ERROR : '
                                   || ' '
                                   || cod_error
                                   || '-'
                                   || warning );

END SP_SIG_OBTENER_INFO_GEST_VCORE;