create or replace PROCEDURE       "SP_SIG_CONC_PAGO_SALCOBRAND" ( warning   OUT VARCHAR2,
                                                   cod_error OUT NUMBER )
IS 
/******************************************************************************
   NAME:       SP_SIG_CONC_PAGO_SALCOBRAND
   PURPOSE:

   REVISIONS:
   Ver        Date      Author Description
   ---------  --------- ------ ---------------------------------------------
   1.0     13-10-2020 acl    SP de conciliacion de transacciones salcobrand y autorizaciones del core SBPAY
   NOTES:
      Object Name:     SP_SIG_CONC_PAGO_SALCOBRAND
******************************************************************************/
BEGIN

    DECLARE   
          
       -- DATOS VISA
       -- Datos necesarios de la tbl_incoming para salcobrand
      var_sid_visa NUMBER;
      cod_autorizacion VARCHAR2(20);
      cod_ref_adq NUMBER; 
      monto_final_transac NUMBER;
      fecha_transac VARCHAR2(50);   
      fecha_inc VARCHAR2(50);
      var_operador number;
      tarjeta_visa VARCHAR2(20 BYTE);
      cod_mon VARCHAR2(10 BYTE);
      rut_cliente VARCHAR2(10 BYTE);
      
      -- DATOS CORE
      -- Datos necesarios de la tbl_core_autor
      var_sid_core NUMBER;
      codigo_autor_core NUMBER;
      datos_ref_adq VARCHAR2(20);
      monto_transac_core VARCHAR2(50);
      fecha_transac_core VARCHAR2(50);
      fecha_inc_core VARCHAR2(50);
      tarjeta_core VARCHAR2(20 BYTE);
      monto_org NUMBER;
      mon_org NUMBER;
      rut_autor_core VARCHAR2(10 BYTE);
      
      --variables conciliacion
      
      sidVisaCore NUMBER;
      num_ref VARCHAR2(20);
      aut_core NUMBER;
      monto_core NUMBER;
      fecha_core VARCHAR2(50);
      var_cod_auth_core number;
      var_conc_visa_core number;
      var_conc_visa_inc number;
      var_tarjeta_visa VARCHAR2(20 BYTE);
      estadoVisa NUMBER;
      rut_core VARCHAR2(20);
      
      
      sidCoreVisa   NUMBER;
      var_monto_core NUMBER;
      var_rut_benef NUMBER;
      estadoBice   NUMBER;
      id_oper_cl VARCHAR2(50);
      var_fecha_inc VARCHAR2(50);
      estadoCore NUMBER;
      var_tarjeta_core VARCHAR2(20 BYTE);
      operador_sb NUMBER;
      
      contador NUMBER;
      monx NUMBER;
      
      /* Cursor pagos Salcobrand */
      CURSOR cursorSalcobrand IS
        select 
        SID as sidpago,
        FECHA_PAGO as fecha_pago,
        TO_DATE(FECHA_CONTABLE) as fecha_inc,
        TO_NUMBER(MONTO_FINAL)as monto,
        SUBSTR(NUM_TARJETA, -4) AS NTARJETA,
        CODIGO_AUTORIZACION as codauth,
        operador as operador,
        RUT AS RUT
        from sig.tbl_pagos_recibidos where OPERADOR = 4
        and
        TO_CHAR(TO_DATE(FECHA_CONTABLE), 'YYYYMMDD')= TO_CHAR(SYSDATE, 'YYYYMMDD');
      
      
      /* Cursor CORE */
      CURSOR cursorCore IS
        SELECT CA.SID,
        CA.DATOS_REFERENCIA_ADQUIRENTE AS REFADQ,
        TO_NUMBER(CA.MONTO_TRANSAC) AS MONTO,
        to_char(to_date(CA.FECHA_HR_TRASAC, 'DD/MM/YY'), 'DD-MON-YY', 'NLS_DATE_LANGUAGE = english') AS FEC_HR_TRX,
        TO_DATE(CA.FECHA_INCOMING, 'DD-MM-YY') AS FECHAINC,
        CA.CODIGO_AUTORIZACION AS COD_AUT,
        SUBSTR(CA.NRO_TARJETA, -4) AS NTARJETAC,
        CA.IMP_TRANSACCION_VISA AS MONTO_ORIGEN,
        CA.COD_MONEDA_ORG_VISA AS MON_ORIGEN,
        CA.RUT AS RUT
        FROM TBL_CORE_AUTOR CA
        WHERE TO_CHAR(CA.FECHA_INCOMING, 'YYYYMMDD') = TO_CHAR(SYSDATE, 'YYYYMMDD')
        AND CA.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE EMPRESA = 'Salcobrand')
        AND MIT=8;  
            
      BEGIN
      cod_error := 0;
      warning := 'Proceso SP_SIG_CONC_PAGO_SALCOBRAND ejecutado';

       /*=====================CURSOR VISA=========================================*/
        OPEN cursorSalcobrand;
        LOOP
          fetch cursorSalcobrand into 
            var_sid_visa, 
            fecha_transac, 
            fecha_inc,
            monto_final_transac, 
            tarjeta_visa,
            cod_autorizacion,   
            var_operador,
            rut_cliente;
          EXIT WHEN cursorSalcobrand%NOTFOUND;
          --dbms_output.Put_line('loop cursor Visa');
          sidCoreVisa := 0;
          monx := monto_final_transac;
          -- 1 query para identificar las que no califican por hora
          -- 2 insertarlas con estado de no conciliadas por limite de hora
          -- 3 seguir proceso normal
          --dbms_output.Put_line('var_sid_visa: '|| var_sid_visa ||' cod_ref_adq:  ' ||cod_ref_adq||' monto_transac: ' ||monto_transac|| ' fecha_hora_transac:' ||fecha_hora_transac|| ' fecha_inc: '||fecha_inc|| ' cod_autorizacion: ' ||cod_autorizacion || 'tarjeta_visa' || tarjeta_visa);
         begin
         
          
             select  AC.SID, REPLACE(LTRIM(AC.CODIGO_AUTORIZACION,'0'),' ',''), AC.MONTO_TRANSAC, REPLACE(LTRIM(AC.FECHA_HR_TRASAC,'0'),' ',''), SUBSTR(AC.NRO_TARJETA,-4), TRIM(AC.RUT)  
             into sidCoreVisa, aut_core, monto_core, fecha_core, var_tarjeta_core,rut_core
             from TBL_CORE_AUTOR AC where 
             AC.MONTO_TRANSAC = (monx) 
             AND AC.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE EMPRESA = 'Salcobrand') 
             AND SUBSTR(AC.NRO_TARJETA,-4) = tarjeta_visa
             AND AC.RUT=(rut_cliente)
             AND AC.SID = (SELECT MIN(SID) from TBL_CORE_AUTOR AC where 
             AC.MONTO_TRANSAC = (monx) 
             AND AC.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE EMPRESA = 'Salcobrand') 
             AND SUBSTR(AC.NRO_TARJETA,-4) = tarjeta_visa AND AC.RUT=(rut_cliente))
  
             group by sid, AC.CODIGO_AUTORIZACION, AC.MONTO_TRANSAC, AC.FECHA_HR_TRASAC, AC.NRO_TARJETA,AC.RUT ;
          
         
           
         exception
          WHEN NO_DATA_FOUND THEN
          sidCoreVisa := 0;
         end;
            ---dbms_output.Put_line('sidBiceTrebol: '||sidBiceTrebol || ' var_sid_trebol: '||var_sid_trebol || ' rut_clie: '||rut_clie || ' monto_bic: '||monto_bic || ' sid_opr_cl: '||sid_opr_cl);
           if(sidCoreVisa > 0)then
              estadoVisa := 1;
             elsif(sidCoreVisa = 0)then
              estadoVisa := 2;
               
           end if;
           
          begin
           
           --dbms_output.Put_line(sidCoreVisa || ' - ' || var_sid_visa);
           select  sid into var_conc_visa_core from TBL_CONC_SALCOBRAND where (SID_CORE = sidCoreVisa and sidCoreVisa > 0 ) or (SID_SALCOBRAND = var_sid_visa);
           --dbms_output.put_line('var_conc_visa_core: '||var_conc_visa_core);
           exception
          WHEN NO_DATA_FOUND THEN
          --dbms_output.put_line('sid_bice_trebol: '||sidBiceTrebol||'--'||'var_sid_trebol: '||var_sid_trebol);
          if(sidCoreVisa>0 or var_sid_visa>0)then
            --dbms_output.Put_line('delete: SID_TREBOL '||sidCoreVisa||' SID_CC '||var_sid_visa);
            delete from TBL_CONC_SALCOBRAND where ((SID_CORE = sidCoreVisa and sidCoreVisa>0) or (SID_SALCOBRAND = var_sid_visa)) and ESTADO_CONTABLE not like '1';
            --dbms_output.put_line(SQL%ROWCOUNT);
          end if;
          var_conc_visa_core := 0;
         end;
           --dbms_output.Put_line('var_conc_visa_core: '||var_conc_visa_core);
            update TBL_CONC_SALCOBRAND set  SID_CORE = sidCoreVisa, SID_SALCOBRAND=var_sid_visa, SID_ESTADO=estadoVisa, FECHA=fecha_inc, FECHA_REGISTRO= sysdate,
            MONTO = monto_final_transac, ORIGEN = 'P'
            where sid = var_conc_visa_core;
            if(SQL%ROWCOUNT = 0)THEN
              select count(*) into contador from TBL_CONC_SALCOBRAND VC where SID_CORE=sidCoreVisa and VC.SID_SALCOBRAND=var_sid_visa;
              --dbms_output.Put_line('Encuentra duplicados: '||contador);
              if(contador = 0)THEN
              
             --dbms_output.Put_line('Datos a insertar: '||sidBiceTrebol||'-'||var_sid_trebol||'-'||estadoTrebol||'-'||fecha_reg_trebol||'-'||num_ref_trans||'-'|| monto_trebol ||'-'|| rut_cliente ||'-'|| SYSDATE);
             
              
                insert into TBL_CONC_SALCOBRAND(SID, SID_SALCOBRAND, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, OPERADOR, ORIGEN)
                values(SEQ_TBL_CONC_SALCOBRAND.nextval,var_sid_visa,sidCoreVisa,estadoVisa,fecha_inc,monto_final_transac, SYSDATE, var_operador, 'P'); 
                
                --dbms_output.put_line(sidBiceTrebol||'-'||var_sid_trebol||'-'||estadoTrebol||'-'||fecha_reg_trebol||'-'||num_ref_trans||'-'||monto_trebol||'-'||rut_cliente||'-'||SYSDATE);
              end if;
            end if;
          --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorSalcobrand;
      --dbms_output.Put_line('#################################################### FIN CURSOR VISA');
      
      
    /*=====================CURSOR CORE=========================================*/
     OPEN cursorCore;
        LOOP
         FETCH cursorCore INTO 
         var_sid_core, 
         datos_ref_adq, 
         monto_core, 
         fecha_transac_core, 
         fecha_inc_core, 
         codigo_autor_core,
         tarjeta_core,
         monto_org,
         mon_org,
         rut_autor_core;
         EXIT WHEN cursorCore%NOTFOUND;
         --dbms_output.Put_line('loop cursor Core');
         sidVisaCore := 0;
         var_monto_core:= 0;
       --  dbms_output.Put_line('var_sid_core :'||var_sid_core ||'-'||'datos_ref_adq :'||datos_ref_adq ||'-'||'monto_core :'||monto_core||'-'||'fecha_transac_core :'||fecha_transac_core||'-'||'fecha_inc_core :'||fecha_inc_core||'-'||'codigo_autor_core :'||codigo_autor_core);
       begin
       
      /* warning := 'SELECT I.sid, I.CODIGO_AUTORIZACION, TO_NUMBER(I.MONTO_FINAL), I.FECHA_PAGO, I.OPERADOR, SUBSTR(I.NUM_TARJETA, -4)
       INTO sidVisaCore, var_cod_auth_core, var_monto_core, var_fecha_inc, operador_sb, var_tarjeta_visa
        FROM sig.tbl_pagos_recibidos I  WHERE TO_NUMBER(I.MONTO_FINAL) = '||monto_core||' AND I.CODIGO_AUTORIZACION = '||codigo_autor_core||' AND I.FECHA_PAGO = '||fecha_transac_core||'
        AND SUBSTR(I.NUM_TARJETA, -4) = '||tarjeta_core||' AND I.OPERADOR = 4
        AND I.SID = (SELECT MIN(SID) FROM sig.tbl_pagos_recibidos I  WHERE TO_NUMBER(I.MONTO_FINAL) =
        '||monto_core||' AND I.FECHA_PAGO = '||fecha_transac_core ||'
        AND SUBSTR(I.NUM_TARJETA, -4) = '||tarjeta_core||' AND I.OPERADOR = 4) 
        GROUP BY I.sid, I.CODIGO_AUTORIZACION, I.MONTO_FINAL, I.FECHA_PAGO, I.OPERADOR, I.NUM_TARJETA;';*/
       
       
       SELECT I.sid,  TO_NUMBER(I.MONTO_FINAL), I.FECHA_PAGO, I.OPERADOR, SUBSTR(I.NUM_TARJETA, -4)
       INTO sidVisaCore,  var_monto_core, var_fecha_inc, operador_sb, var_tarjeta_visa
        FROM sig.tbl_pagos_recibidos I 
        WHERE TO_NUMBER(I.MONTO_FINAL) = monto_core 
        AND I.FECHA_PAGO = fecha_transac_core
        AND SUBSTR(I.NUM_TARJETA, -4) = tarjeta_core
        AND I.OPERADOR = 4
        AND I.RUT=rut_autor_core
        AND I.SID = (SELECT MIN(SID) FROM sig.tbl_pagos_recibidos I  WHERE TO_NUMBER(I.MONTO_FINAL) = monto_core
        AND I.FECHA_PAGO = fecha_transac_core 
        AND SUBSTR(I.NUM_TARJETA, -4) = tarjeta_core 
        AND I.OPERADOR = 4
        AND I.RUT=rut_autor_core) 
        GROUP BY I.sid, I.MONTO_FINAL, I.FECHA_PAGO, I.OPERADOR, I.NUM_TARJETA;
       
       
        --SELECT I.sid, I.CODIGO_AUTORIZACION, I.MONTO_TRANSAC, I.FECHA_HR_TRASAC, I.OPERADOR, SUBSTR(I.NRO_TARJETA, -4)
        --INTO sidVisaCore, var_cod_auth_core, var_monto_core, var_fecha_inc, operador_sb, var_tarjeta_visa
        --FROM TBL_INCOMING I  WHERE I.MONTO_TRANSAC = monto_core AND I.CODIGO_AUTORIZACION = codigo_autor_core AND I.FECHA_HR_TRASAC = fecha_transac_core  
        --AND SUBSTR(I.NRO_TARJETA, -4) = tarjeta_core AND I.OPERADOR = 4
        --AND I.SID = (SELECT MIN(SID) FROM TBL_INCOMING I  WHERE I.MONTO_TRANSAC = monto_core AND I.CODIGO_AUTORIZACION = codigo_autor_core AND I.FECHA_HR_TRASAC = fecha_transac_core  
        --AND SUBSTR(I.NRO_TARJETA, -4) = tarjeta_core AND I.OPERADOR = 4) 
        --GROUP BY I.sid, I.CODIGO_AUTORIZACION, I.MONTO_TRANSAC, I.FECHA_HR_TRASAC, I.OPERADOR, I.NRO_TARJETA;
       
        
        
        --dbms_output.Put_line('sid :'||sidTrebolBice||'-'||'monto'|| var_monto_bice||'-'||'NUM_REF_TRANS'|| id_oper_cl||'-'||'rut'|| var_rut_benef);
       exception
        WHEN NO_DATA_FOUND THEN
        sidVisaCore := 0;
        
        
       end;
         --dbms_output.Put_line('var_monto_bice: '||var_monto_bice ||' sidTrebolBice: '|| sidTrebolBice || ' id_oper_cl: '||id_oper_cl|| ' rut_beneficiario: '||var_rut_benef);      
     

       if(sidVisaCore > 0)then
          estadoCore := 1;
          
          else
              estadoCore := 3;
              operador_sb := 0;
       end if;
        --dbms_output.Put_line('estadoBice: '||estadoBice); 
        --dbms_output.Put_line('var_sid_bice: '||var_sid_bice ||'sidTrebolBice: '||sidTrebolBice);
       begin
       
       --dbms_output.Put_line(sidVisaCore || ' - ' || var_sid_core);
         select  sid into var_conc_visa_inc from TBL_CONC_SALCOBRAND 
         where (SID_SALCOBRAND = sidVisaCore and sidVisaCore >0) or (SID_CORE = var_sid_core);
         exception
        WHEN NO_DATA_FOUND THEN
        var_conc_visa_inc := 0;
       end;
       
         --dbms_output.Put_line(var_conc_visa_inc);        
        update TBL_CONC_SALCOBRAND set sid_core = var_sid_core, SID_SALCOBRAND = sidVisaCore, sid_estado = estadoCore
          where sid = var_conc_visa_inc;
          
          --dbms_output.Put_line( 'update TBL_CONC_SALCOBRAND set sid_core ='|| var_sid_core||', SID_SALCOBRAND = '|| sidVisaCore||', sid_estado ='|| estadoCore||' where sid ='|| var_conc_visa_inc);
          
          if(SQL%ROWCOUNT = 0)THEN
          
           insert into TBL_CONC_SALCOBRAND(SID, SID_SALCOBRAND, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, OPERADOR, ORIGEN)
           --values(SEQ_TBL_CONC_VISA_CORE.nextval,sidCoreVisa,var_sid_visa,estadoVisa,fecha_inc,monto_transac, SYSDATE, 0); 
           values(SEQ_TBL_CONC_SALCOBRAND.nextval,sidVisaCore,var_sid_core,estadoCore,fecha_inc_core, monto_core, SYSDATE, operador_sb, 'P');  
          end if;
        --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorCore;
      --dbms_output.Put_line('Fin cursor Core');
      --dbms_output.Put_line('##############################################################################  FIN CURSOR CORE ');
      
 /*==============================================================*/
--dbms_output.Put_line('Final del proceso');
--Sin este commit, el proceso no deja las conciliaciones registradas, no se cae, no da error, simplemente no las deja registradas
  COMMIT;
  END;
     
    EXCEPTION     
         WHEN NO_DATA_FOUND THEN
         --dbms_output.Put_line( 'NO HAY REGISTROS EN TABLA' ); 
         warning := warning || '- NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS';
        WHEN OTHERS THEN                                                                      
                 warning := SQLERRM || ' - ' || DBMS_UTILITY.format_error_backtrace;
                 cod_error := SQLCODE;
                 dbms_output.Put_line( 'ERROR : ' || warning ||' SQLCODE : ' || cod_error );
                 dbms_output.Put_line( 'DESCRIPTION: ' || DBMS_UTILITY.format_error_backtrace);
                
                INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CONC_PAGO_SALCOBRAND', cod_error || '-' || warning);
                COMMIT;

END SP_SIG_CONC_PAGO_SALCOBRAND;