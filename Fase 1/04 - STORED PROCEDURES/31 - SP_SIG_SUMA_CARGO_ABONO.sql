create or replace PROCEDURE "SP_SIG_SUMA_CARGO_ABONO" (
                                                      sidTrx              IN        TBL_CORE_AUTOR.SID%TYPE,
                                                      suma_cargos         OUT       VARCHAR2,
                                                      suma_abonos         OUT       VARCHAR2,
                                                      codigo_error        OUT       NUMERIC,
                                                      warning             OUT       VARCHAR2
                                                      )
-- Proyecto: SIG
-- By: Developer || ACL | 21/04/2016
-- Versión : 1.0
-- Descripcion: Procedimiento que dado un id de una transaccion calcula la 
-- sumatoria de los cargos y de los abonos. 

IS

BEGIN
   DECLARE

   BEGIN
    codigo_error := 0;
    warning := 'SP_SIG_GET_SUM_CARG_ABO ha sido ejecutado exitosamente';
    DBMS_OUTPUT.PUT_LINE('*****************************************');
    DBMS_OUTPUT.PUT_LINE('*   Se ejecuta  SP_SIG_GET_SUM_CARG     *');
    DBMS_OUTPUT.PUT_LINE('*****************************************');
    DBMS_OUTPUT.PUT_LINE ('PARAMETROS DE ENTRADA :');
    DBMS_OUTPUT.PUT_LINE ('sidTrx = '||sidTrx  );
    DBMS_OUTPUT.PUT_LINE ('----------------------------------------');

  -- SE BUSCA LA SUMATORIA DE LOS CARGOS 
  SELECT TRIM(TO_CHAR(SUM(CA.MONTO_CARGO_ABONO))) into suma_cargos FROM TBL_GESTION_CARGO_ABONO CA  WHERE CA.TRANSACCION = sidTrx AND CA.TBL_CARGO_ABONO = 1;

  -- SE BUSCA LA SUMATORIA DE LOS ABONOS
  SELECT TRIM(TO_CHAR(SUM(CA.MONTO_CARGO_ABONO))) into suma_abonos FROM TBL_GESTION_CARGO_ABONO CA WHERE CA.TRANSACCION = sidTrx AND CA.TBL_CARGO_ABONO = 2;     

    -- SE VALIDA SI SE RETORNA NULL O VACIO
    IF suma_abonos IS null THEN
           suma_abonos := '000';
    ELSIF  suma_abonos = ''  THEN
           suma_abonos := '000';           
    END IF;

    IF suma_cargos IS null THEN
          suma_cargos := '000';
    ELSIF  suma_cargos = '' THEN
          suma_cargos := '000';
    END IF;


      DBMS_OUTPUT.PUT_LINE ('PARAMETROS DE SALIDA');
      DBMS_OUTPUT.PUT_LINE ('suma_cargos = '|| suma_cargos );
      DBMS_OUTPUT.PUT_LINE ('suma_abonos = '|| suma_abonos );
      DBMS_OUTPUT.PUT_LINE ('*****************************************');
  END;

 EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         codigo_error := SQLCODE;
         warning := SQLCODE || '- ' || SQLERRM ||'-' ||DBMS_UTILITY.format_error_backtrace;
         DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);
         INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_SUMA_CARGO_ABONO', warning);
      WHEN OTHERS
      THEN
         codigo_error := SQLCODE;
         warning := SQLCODE || '- ' || SQLERRM ||'-' ||DBMS_UTILITY.format_error_backtrace;
         DBMS_OUTPUT.PUT_LINE ('Salida Error: ' || warning);
         INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_SUMA_CARGO_ABONO', warning);
END;

