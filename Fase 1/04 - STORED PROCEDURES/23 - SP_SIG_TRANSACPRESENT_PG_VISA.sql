create or replace PROCEDURE       "SP_SIG_TRANSACPRESENT_PG_VISA" (
-- Proyecto: SIG
-- By: Developer || ACL | 07/12/2015
-- Version : 1.0
-- Descripcion: Consulta las transacciones de presentacion de acuerdo a los parametros de entrada.
                                                                              numeroTarjeta  IN VARCHAR2,
                                                                              fechaInicio    IN VARCHAR2,
                                                                              fechaTermino   IN VARCHAR2,
                                                                              numPagina      IN NUMBER,
                                                                              cantReg        IN NUMBER,
                                                                              codTransac     IN VARCHAR2,
                                                                              codFuncion     IN VARCHAR2,
                                                                              warning        OUT VARCHAR2,
                                                                              cod_error      OUT NUMBER,
                                                                              prfCursor      OUT Sys_RefCursor,
          totReg         OUT number
                                                               )

IS
    voCursor Sys_RefCursor:=null;
    v_cant NUMBER;
    v_reg Sys_RefCursor;
BEGIN

    DBMS_OUTPUT.PUT_LINE(numeroTarjeta || ' ' || fechaInicio || ' ' || fechaTermino);

	IF (numeroTarjeta<>'0' and codTransac<>'00') THEN
                          --inicio mod
		OPEN voCursor FOR
                select * FROM 
                (
                  SELECT 
                  ROWNUM  AS registro,
                  I.MIT,
                  I.CODIGO_FUNCION,
                  I.NRO_TARJETA,
                  TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                  REPLACE(I.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                                                          AS COMERCIOPAIS,
                  TRIM((CASE WHEN I.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_TRANSAC,1,LENGTH(I.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_TRANSAC,LENGTH(I.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                  TRIM((CASE WHEN I.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_FACTURACION,1,LENGTH(I.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_FACTURACION,LENGTH(I.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,


                  NVL(I.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                  I.CODIGO_AUTORIZACION,
                  TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                  NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                  'PENDIENTE'                                                         AS ESTADOTRX,
                  NVL(I.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                  NVL(I.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                  NVL(I.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                  NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                  NVL(I.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                  NVL(I.MCC, ' ')                                                     AS RUBROCOMERCIO,
                  I.SID                                                               AS SID,
                  A.DESCRIPCION                                                      AS GLOSAGENERAL,
                  NVL(I.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                  TRIM((CASE WHEN I.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_CONCILIACION,1,LENGTH(I.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_CONCILIACION,LENGTH(I.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION ,             
                  I.DATOS_ADICIONALES_5                                              AS DATOSADICIONALES5,
                  NVL(P.P0023, '-')                                                AS  PATPASS,
                  I.OPERADOR                                                       AS IOPERADOR,
                  TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR

                FROM SIG.TBL_INCOMING I  LEFT OUTER  JOIN SIG.TBL_PDS P ON (I.SID      = P.TRANSACCION),  SIG.TBL_ESTADO_PROCESO E, SIG.TBL_ACCION_TRANSAC A
                WHERE                
                  I.MIT                               = codTransac                                     AND
                  I.CODIGO_FUNCION                       = codFuncion                                              AND
                  I.NRO_TARJETA                                            =  numeroTarjeta           AND
                  TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio              AND 
                  TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
                  I.ESTADO_PROCESO = E.SID                                    AND
                  E.XKEY = 'IN_AC' AND I.MIT = A.COD_TRANSAC AND I.CODIGO_FUNCION = A.COD_FUNCION
                  )
                 WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
              AND  numPagina * cantReg;    
          --fin mod
          --cantidad
          select
            count(I.SID) AS cantidad into v_cant
          FROM    SIG.TBL_INCOMING I, SIG.TBL_ESTADO_PROCESO E
          WHERE       I.MIT                             = codTransac                                           AND
            I.CODIGO_FUNCION                             = codFuncion                                              AND
            I.NRO_TARJETA                                                  =  numeroTarjeta           AND
            TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio    AND 
            TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
            I.ESTADO_PROCESO = E.SID                                    AND
            E.XKEY = 'IN_AC' ;

    ELSIF (numeroTarjeta<>'0' and codTransac='00') THEN
                          --inicio mod
		OPEN voCursor FOR
                select * from (
                select
                  ROWNUM  AS registro,
                  I.MIT,
                  I.CODIGO_FUNCION,
                  I.NRO_TARJETA,
                  TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                  REPLACE(I.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                                                          AS COMERCIOPAIS,
                  TRIM((CASE WHEN I.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_TRANSAC,1,LENGTH(I.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_TRANSAC,LENGTH(I.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                  TRIM((CASE WHEN I.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_FACTURACION,1,LENGTH(I.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_FACTURACION,LENGTH(I.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,


                  NVL(I.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                  I.CODIGO_AUTORIZACION,
                  TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                  NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                  'PENDIENTE'                                                         AS ESTADOTRX,
                  NVL(I.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                  NVL(I.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                  NVL(I.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                  NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                  NVL(I.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                  NVL(I.MCC, ' ')                                                     AS RUBROCOMERCIO,
                  I.SID                                                               AS SID,
                  A.DESCRIPCION                                                     AS GLOSAGENERAL,
                  NVL(I.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                 TRIM((CASE WHEN I.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_CONCILIACION,1,LENGTH(I.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_CONCILIACION,LENGTH(I.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                 I.DATOS_ADICIONALES_5                                              AS DATOSADICIONALES5,
                 NVL(P.P0023, '-')                                                AS  PATPASS,
                 I.OPERADOR                                                       AS IOPERADOR,
                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR
                FROM SIG.TBL_INCOMING I  LEFT OUTER  JOIN SIG.TBL_PDS P ON (I.SID      = P.TRANSACCION), SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_PROCESO E
                WHERE                
                 -- I.MIT                               = codTransac                                     AND
                 -- I.CODIGO_FUNCION                       = codFuncion                                              AND
                  I.NRO_TARJETA                                            =  numeroTarjeta           AND
                  TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio              AND 
                  TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino  AND
				  I.ESTADO_PROCESO = E.SID                                    AND
                  E.XKEY = 'IN_AC' AND 
                  I.MIT = A.COD_TRANSAC AND I.CODIGO_FUNCION = A.COD_FUNCION order by FECHATRANSAC ASC
                  )
              WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
              AND  numPagina * cantReg;
          --fin mod
          --cantidad
          select
            count(I.SID) AS cantidad into v_cant
          FROM    SIG.TBL_INCOMING I --, SIG.TBL_ESTADO_PROCESO E
          WHERE    --   I.MIT                             = codTransac                                           AND
            --I.CODIGO_FUNCION                             = codFuncion                                              AND
            I.NRO_TARJETA                                                  =  numeroTarjeta           AND
            TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio    AND 
            TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino ; --   AND
            --I.ESTADO_PROCESO = E.SID                                    AND
           -- E.XKEY = 'IN_AC';        

    ELSE
                          --inicioMOd
          OPEN voCursor FOR
              SELECT * FROM (
                select
                  --ROWNUM  AS registro,
                  ROW_NUMBER() OVER (ORDER BY I.FECHA_HR_TRASAC ASC) AS registro,
                  I.MIT,
                  I.CODIGO_FUNCION,
                  I.NRO_TARJETA,
                  TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                  REPLACE(I.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                                                          AS COMERCIOPAIS,
                  TRIM((CASE WHEN I.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_TRANSAC,1,LENGTH(I.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_TRANSAC,LENGTH(I.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                  TRIM((CASE WHEN I.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_FACTURACION,1,LENGTH(I.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_FACTURACION,LENGTH(I.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,

                  NVL(I.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                  I.CODIGO_AUTORIZACION,
                  TO_CHAR(I.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                  NVL(SUBSTR(I.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                  NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                  'PENDIENTE'                                                         AS ESTADOTRX,
                  NVL(I.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                  NVL(I.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                  NVL(I.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                  NVL(I.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                  NVL(I.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                  NVL(I.MCC, ' ')                                                     AS RUBROCOMERCIO,
                  I.SID                                                               AS SID,
                  A.DESCRIPCION                                                      AS GLOSAGENERAL,
                  NVL(I.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                  TRIM((CASE WHEN I.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(I.MONTO_CONCILIACION,1,LENGTH(I.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(I.MONTO_CONCILIACION,LENGTH(I.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                 I.DATOS_ADICIONALES_5                                              AS DATOSADICIONALES5,
                 NVL(P.P0023, '-')                                                AS  PATPASS,
                 I.OPERADOR                                                       AS IOPERADOR,
                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR


                FROM    SIG.TBL_INCOMING I   LEFT OUTER  JOIN SIG.TBL_PDS P ON (I.SID      = P.TRANSACCION),  SIG.TBL_ESTADO_PROCESO E, SIG.TBL_ACCION_TRANSAC A
                WHERE                I.MIT                             = codTransac                                           AND
                  I.CODIGO_FUNCION                       = codFuncion                                              AND
                  TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio              AND 
                  TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
                  I.ESTADO_PROCESO = E.SID                                    AND
                  E.XKEY = 'IN_AC' AND I.MIT = A.COD_TRANSAC AND I.CODIGO_FUNCION = A.COD_FUNCION
                  --order by     I.FECHA_HR_TRASAC DESC
                  )
              WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
              AND  numPagina * cantReg;
                                                                                              --fin mod
                        --cantidad
          select
            count(I.SID) AS cantidad into v_cant
          FROM    SIG.TBL_INCOMING I, SIG.TBL_ESTADO_PROCESO E
          WHERE       I.MIT                             = codTransac                                           AND
            I.CODIGO_FUNCION                             = codFuncion                                              AND
            TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio    AND 
            TO_CHAR(I.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino     AND
            I.ESTADO_PROCESO = E.SID                                    AND
            E.XKEY = 'IN_AC';
    END IF;

        prfCursor:=voCursor;
        totReg:=v_cant;

   --     loop
   --       fetch voCursor into v_reg;
   --       exit when voCursor%notfound;
   --     end loop;

                                               EXCEPTION
                                                                              WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.PUT_LINE ('No data');
            cod_error := SQLCODE;
            warning := SQLERRM;
                                                                                              DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                                                                              WHEN OTHERS                                 THEN
                                                                                  cod_error:=SQLCODE;
                                                                                              warning:=SQLERRM;
         INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) 
          VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_TRANSACPRESENTACION_PG_VISA', cod_error || '-' || warning);
                                                                                              DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
          DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);       

END SP_SIG_TRANSACPRESENT_PG_VISA;