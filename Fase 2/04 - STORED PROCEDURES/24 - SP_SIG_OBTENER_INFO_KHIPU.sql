create or replace PROCEDURE "SP_SIG_OBTENER_INFO_KHIPU"(
                                                              id_sid           IN NUMBER,
                                                              warning       OUT VARCHAR2,
                                                              cod_error     OUT NUMBER,
                                                              prfcursor     OUT SYS_REFCURSOR)
IS  
/******************************************************************************
   NAME:       SP_SIG_OBTENER_INFO_KHIPU
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        XX-10-2021   acl(PTM)       

   NOTES:

      Object Name:     SP_SIG_OBTENER_INFO_KHIPU
      Sysdate:          XX-10-2021 
******************************************************************************/
  vocursor                 SYS_REFCURSOR := NULL;
BEGIN
    cod_error := 0;
    warning := 'Proceso SP_SIG_OBTENER_INFO_KHIPU ejecutado correctamente ';

    BEGIN
        OPEN vocursor FOR    
            SELECT 
                SID AS sid,
                OFICINA AS xkey,
                NUM_DOCUMENTO AS mit,
                CLIENTE_NUMERO_CUENTA AS nroTarjeta,
                ESTRUCTURA_PAGO AS codDatosPuntoServicio,
                TRANSACCION_ID AS datosReferenciaAdquiriente,
                RUT AS rut,
                RUT_PAGADOR AS codigoServicio,
                OPERADOR AS operador,
                MONTO_FINAL AS monto,
                MONTO_FINAL as montoConciliacion,
                MONTO_FINAL as montoFacturacion,
                RUT_PAGADOR AS nombreUbicAcepTarjeta,
                FECHA_PAGO as fechaTransaccion
            FROM TBL_PAGOS_RECIBIDOS
            WHERE SID = id_sid;

        prfcursor := vocursor;
 END;

EXCEPTION
  WHEN no_data_found THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             dbms_output.Put_line( 'NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS' ); WHEN OTHERS THEN
             cod_error := SQLCODE;

             warning := SQLERRM;

             INSERT INTO sig.LOG_ERROR
                         (sid,
                          fecha_insercion,
                          nombre_sp,
                          msg_error)
             VALUES      (sig.seq_log_error.NEXTVAL,
                          SYSDATE,
                          'SP_SIG_OBTENER_INFO_KHIPU',
                          cod_error
                          || '-'
                          || warning);

             dbms_output.Put_line( 'ERROR : '
                                   || ' '
                                   || cod_error
                                   || '-'
                                   || warning );
END SP_SIG_OBTENER_INFO_KHIPU;