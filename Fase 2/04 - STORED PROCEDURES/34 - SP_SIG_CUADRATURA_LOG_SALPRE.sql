create or replace PROCEDURE SP_SIG_CUADRATURA_LOG_SALPRE (
-- Proyecto: SIG                   
-- By: Developer || RADS || ACL | 22/01/2021
-- Versiòn : 1.0
-- Descripcion: Consulta la cantidad de transacciones de incoming y el total de transacciones de excepciones 
--              en la carga para Salcobrand Y Preunic.

                                                                  fechaProceso        IN VARCHAR2,
                                                                  filtro_operador     IN NUMBER,
                                                                  warning             OUT VARCHAR2,
                                                                  cod_error           OUT NUMBER,
                                                                  prfCursor           OUT SYS_REFCURSOR,
                                                                  cantidadIncoming    OUT NUMBER
                                                                  )
IS
    voCursor SYS_REFCURSOR:=null;
    numeroRecordIncoming NUMBER:=0;
BEGIN

            BEGIN
                        OPEN voCursor FOR

                            SELECT SID                                     AS SID, 
                                   LOG_CARGA                             AS DESCRIPCION, 
                                   NRO_TARJETA                           AS NUMEROTARJETA,  
                                   MIT                                   AS MIT, 
                                   MCC                                   AS MCC, 
                                   NVL(COD_MOTIVO_MENSAJE, ' ')          AS CODIGORAZON,
                                   DATOS_REFERENCIA_ADQUIRENTE           AS DATOS_REFERENCIA_ADQUIRENTE, 
                                   COD_IDENTIFICACION_ADQUIRENTE         AS COD_IDENTIFICACION_ADQUIRENTE, 
                                   NOMBRE_UBIC_ACEP_TARJETA              AS LOCAL_COMERCIO, 
                                   MONTO_TRANSAC                         AS MONTOTRANSACCION,
                                   OPERADOR                              AS IDOPERADOR,
                                   CODIGO_AUTORIZACION
                            FROM TBL_EXCEPT_INCOMING 
                            WHERE TO_CHAR(FECHA_INCOMING, 'DDMMYY')= fechaProceso and
                            DATOS_ADICIONALES_5 = 'I'
                            AND OPERADOR = filtro_operador;

                            prfCursor:=voCursor;    

                        SELECT SUM(numeroIncoming) INTO numeroRecordIncoming 
                        FROM(
                          SELECT COUNT(*) as numeroIncoming
                          FROM TBL_INCOMING 
                          WHERE  
                          TO_DATE(FECHA_INCOMING, 'DD-MM-YY') = TO_DATE(fechaProceso,'DD-MM-YY') AND 
                          MIT IN ('05', '06', '07') AND 
                          OPERADOR =  filtro_operador
                          UNION
                          SELECT COUNT(*) as numeroIncoming
                          FROM TBL_PAGOS_RECIBIDOS 
                          WHERE  
                          TO_DATE(FECHA_CONTABLE, 'DD-MM-YY') = TO_DATE(fechaProceso,'DD-MM-YY') AND 
                          OPERADOR = filtro_operador);

                    
                        cantidadIncoming:=numeroRecordIncoming;
                        --DBMS_OUTPUT.PUT_LINE('cantidadIncoming: '  || cantidadIncoming); 
                    

            END;

            EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CUADRATURALOGPROCESOINC', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
END SP_SIG_CUADRATURA_LOG_SALPRE;