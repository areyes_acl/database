create or replace PROCEDURE       "SP_SIG_TRANSACRECPETVALPG" (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/07/2013
-- Version: 2.0
-- Descripcion: Consulta las transacciones de confirmacion de recepcion peticion de vales de acuerdo a los parametros de entrada.
                                                            numeroTarjeta       IN VARCHAR2,
                                                            fechaInicio         IN VARCHAR2,
                                                            fechaTermino        IN VARCHAR2,
                                                            vxkey               IN VARCHAR2,
                                                            tipoBusqueda			  IN VARCHAR2,
                                                            numPagina           IN NUMBER,
                                                            cantReg             IN NUMBER,
                                                            warning             OUT VARCHAR2,
                                                            cod_error           OUT NUMBER,
                                                            prfCursor           OUT Sys_RefCursor,
                                                            totReg              OUT number
                                                            )
IS
    voCursor Sys_RefCursor:=null;
    v_cant NUMBER;

BEGIN

            BEGIN
                DBMS_OUTPUT.PUT_LINE(numeroTarjeta || ' ' || fechaInicio || ' ' || fechaTermino);
                IF (numeroTarjeta<>'0' AND tipoBusqueda = 'T') THEN
                 BEGIN
                        OPEN voCursor FOR 
                        SELECT * FROM (
                            SELECT     
                            ROWNUM  AS registro,
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                                T.NRO_TARJETA, 
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                        AS COMERCIOPAIS,
                                TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                T.CODIGO_AUTORIZACION,
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                                NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                T.SID                                                               AS SID,
                                T.GLOSA_GENERAL                                                     AS GLOSAGENERAL,
                                NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                T.NUM_INCIDENTE                                                     AS NUM_INCIDENTE ,
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                AS USUARIO,
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'DD/MM/YYYY')                       AS FECHA_GESTION,
                                 NVL(P.P0023, '-')                                                AS  PATPASS,
                                 I.OPERADOR                                                       AS IOPERADOR,
                                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR
                            FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION)
                             LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION)
                                 LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID,
                                 SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E,
                                 TBL_INCOMING I
                            WHERE     
                                T.NRO_TARJETA                           =  numeroTarjeta        AND
                                GT.TRANSACCION                           =  I.SID                AND
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ACCION_TRANSAC                         = A.SID                AND
                                A.XKEY                                   = vxkey                AND
                                A.ACTIVO                                 = 1                    AND
                                T.ESTADO_TRANSAC                         = E.SID                AND
                                GT.ESTADO_PROCESO                       IN (4,5)    AND
                                E.SID=1
                                order by     T.FECHA_HR_TRASAC 
                                 )
							WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
							AND  numPagina * cantReg;
                                 select
									count(T.SID) AS cantidad into v_cant            
									FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) ,
                        SIG.TBL_ACCION_TRANSAC A,
                        SIG.TBL_ESTADO_TRANSAC E,
                        TBL_INCOMING I
								WHERE     
									T.NRO_TARJETA                           =  numeroTarjeta        AND
                  GT.TRANSACCION                           =  I.SID                AND
									TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio          AND  
									TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino         AND
									T.ACCION_TRANSAC                         = A.SID                AND
									A.XKEY                                   = vxkey                AND
									A.ACTIVO                                 = 1                    AND
									T.ESTADO_TRANSAC                         = E.SID                AND
                  GT.ESTADO_PROCESO                       IN (4,5)                AND
									E.SID=1;       


                    END;
                ELSE
                    BEGIN
                    IF (numeroTarjeta<>'0' AND tipoBusqueda = 'G' ) THEN
                      BEGIN
                        OPEN voCursor FOR
                        SELECT * FROM (
                                  SELECT   
                                        ROWNUM  AS registro,
                                        T.MIT, 
                                        T.CODIGO_FUNCION, 
                                        T.NRO_TARJETA, 
                                        TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                        REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                                        TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                        TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                        NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                        NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                        T.CODIGO_AUTORIZACION,
                                        TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                        NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                        NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                        NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                        UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                                        NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                        NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                        NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                        NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                        NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                        NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                        T.SID                                                               AS SID,
                                        T.GLOSA_GENERAL                                                     AS GLOSAGENERAL,
                                        NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                        TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                        T.NUM_INCIDENTE                                                     AS NUM_INCIDENTE,
                                        CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                AS USUARIO,
                                        TO_CHAR(GT.FECHA_ACTUALIZACION, 'DD/MM/YYYY')                       AS FECHA_GESTION,
                                         NVL(P.P0023, '-')                                                AS  PATPASS,
                                         I.OPERADOR                                                       AS IOPERADOR,
                                         TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR
                                  FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) 
                                   LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION)
                                        LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID, SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E,
                                        TBL_INCOMING I
                                  WHERE     
                                        T.NRO_TARJETA                           =  numeroTarjeta        AND
                                        GT.TRANSACCION                           =  I.SID                AND
                                        TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio        AND  
                                        TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino       AND
                                        T.ACCION_TRANSAC                             = A.SID           		 AND
                                        A.XKEY                                       = vxkey             	 AND
                                        A.ACTIVO                                     = 1                 	 AND
                                        T.ESTADO_TRANSAC                         = E.SID                   AND
                                        GT.ESTADO_PROCESO                            IN (4,5)              AND
                                        E.SID=1  ORDER BY   GT.FECHA_ACTUALIZACION)

                              WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
															  AND  numPagina * cantReg;

                                select count(T.SID) AS cantidad into v_cant
																		 FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) ,
                                          SIG.TBL_ACCION_TRANSAC A,
                                          SIG.TBL_ESTADO_TRANSAC E,
                                          TBL_INCOMING I
                                     WHERE   
                                        T.NRO_TARJETA                           =  numeroTarjeta        AND
                                        GT.TRANSACCION                           =  I.SID                AND 
																				TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
																				TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
																				T.ACCION_TRANSAC                              = A.SID            		 AND
																				A.XKEY                                        = vxkey            		 AND
																				A.ACTIVO                                      = 1                  	 AND
																				T.ESTADO_TRANSAC                              = E.SID           		 AND
                                        GT.ESTADO_PROCESO                             IN (4,5)               AND
																				E.SID=1; 
                      END;
                    END IF;
                 END;
              END IF;


              IF (numeroTarjeta ='0' AND tipoBusqueda = 'T') THEN

                 BEGIN
                        OPEN voCursor FOR 
                        SELECT * FROM (
                            SELECT  
                                ROWNUM  AS registro,
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                                T.NRO_TARJETA, 
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                        AS COMERCIOPAIS,
                                TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                T.CODIGO_AUTORIZACION,
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                                NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                T.SID                                                               AS SID,
                                T.GLOSA_GENERAL                                                     AS GLOSAGENERAL,
                                NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                T.NUM_INCIDENTE                                                     AS NUM_INCIDENTE ,
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                AS USUARIO,
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'DD/MM/YYYY')                       AS FECHA_GESTION,
                                 NVL(P.P0023, '-')                                                AS  PATPASS,
                                 I.OPERADOR                                                       AS IOPERADOR,
                                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR
                            FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) 
                              LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION)
                                 LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID,
                                 SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E ,
                                 TBL_INCOMING I
                            WHERE     
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino         AND
                                GT.TRANSACCION                           =  I.SID                AND
                                T.ACCION_TRANSAC                         = A.SID                AND
                                A.XKEY                                   = vxkey                AND
                                A.ACTIVO                                 = 1                    AND
                                T.ESTADO_TRANSAC                         = E.SID                AND
                                E.SID=1                                                         AND
                                GT.ESTADO_PROCESO                             IN (4,5)          
                                order by     T.FECHA_HR_TRASAC 
                                 )
							WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
							AND  numPagina * cantReg;
                                 select
									count(T.SID) AS cantidad into v_cant            
									FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) ,
                      SIG.TBL_ACCION_TRANSAC A,
                      SIG.TBL_ESTADO_TRANSAC E,
                      TBL_INCOMING I
								WHERE     
									TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio          AND  
									TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino         AND
                  GT.TRANSACCION                           =  I.SID                AND
									T.ACCION_TRANSAC                         = A.SID                AND
									A.XKEY                                   = vxkey                AND
									A.ACTIVO                                 = 1                    AND
									T.ESTADO_TRANSAC                         = E.SID                AND
                  E.SID                                     =1                     AND
									GT.ESTADO_PROCESO                         IN (4,5)          ;

                    END;
                ELSE
                    BEGIN
                    IF (numeroTarjeta='0' AND tipoBusqueda = 'G' ) THEN
                      BEGIN
                        OPEN voCursor FOR
                        SELECT * FROM (
                              SELECT  			
                                    ROWNUM  AS registro,
                                    T.MIT, 
                                    T.CODIGO_FUNCION, 
                                    T.NRO_TARJETA, 
                                    TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                    REPLACE(T.NOMBRE_UBIC_ACEP_TARJETA, '\','|')                         AS COMERCIOPAIS,
                                    TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                    TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                    NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                    NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                                    T.CODIGO_AUTORIZACION,
                                    TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                    NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                    NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                    NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                    UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                                    NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                                    NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                                    NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                    NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                    NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                    NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                                    T.SID                                                               AS SID,
                                    T.GLOSA_GENERAL                                                     AS GLOSAGENERAL,
                                    NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                    TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                    T.NUM_INCIDENTE                                                     AS NUM_INCIDENTE,
                                    CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                AS USUARIO,
                                    TO_CHAR(GT.FECHA_ACTUALIZACION, 'DD/MM/YYYY')                       AS FECHA_GESTION,
                                      NVL(P.P0023, '-')                                                AS  PATPASS,
                                     I.OPERADOR                                                       AS IOPERADOR,
                                     TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS OPERADOR

														FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION)
                              LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION)
															LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID, SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E,
                                 TBL_INCOMING I
														WHERE     
                                    TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio        AND  
                                    TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino       AND
                                    T.ACCION_TRANSAC                         = A.SID               		 AND
                                    GT.TRANSACCION                           =  I.SID                AND
                                    A.XKEY                                   = vxkey                   AND
                                    A.ACTIVO                                 = 1                    	 AND
                                    T.ESTADO_TRANSAC                         = E.SID                   AND
                                    E.SID=1                                                            AND
                                    GT.ESTADO_PROCESO IN (4,5)
                                    ORDER BY   GT.FECHA_ACTUALIZACION)

													WHERE registro BETWEEN (numPagina - 1) * cantReg + 1
															  AND  numPagina * cantReg;

                                select count(T.SID) AS cantidad into v_cant
																		 FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) , SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E,
                                 TBL_INCOMING I
                                        WHERE     
																				TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
																				TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                        GT.TRANSACCION                           =  I.SID                AND
                                        T.ACCION_TRANSAC                         = A.SID               		 AND
																				A.XKEY                                   = vxkey               		   AND
																				A.ACTIVO                                 = 1                    	   AND
																				T.ESTADO_TRANSAC                         = E.SID               		   AND
                                        GT.ESTADO_PROCESO IN (4,5)                                          AND
																				E.SID=1;  
                      END;
                    END IF;
                 END;
              END IF;

                 prfCursor:=voCursor;
                 totReg:=v_cant;
                EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_TRANSACCIONCONRECPETVAL', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
                END;

END SP_SIG_TRANSACRECPETVALPG;