INSERT INTO "SIG"."TBL_SUB_MENU" (SID, SID_MENU, NOMBRE_SUB_MENU, POSICION, LINK, DESCRIPCION) VALUES 
((select max(SID)+1 from TBL_SUB_MENU), '2', 'Conciliaci�n SERVIPAG', '9', 'conciliacionServipagIndexAction', 'Submen� de conciliaci�n SERVIPAG - Core');

INSERT INTO "SIG"."TBL_SUB_MENU" (SID, SID_MENU, NOMBRE_SUB_MENU, POSICION, LINK, DESCRIPCION) VALUES 
((select max(SID)+1 from TBL_SUB_MENU), '2', 'Conciliaci�n KHIPU', '10', 'conciliacionKhipuIndexAction', 'Submen� de conciliaci�n KHIPU - Core');

INSERT INTO "SIG"."TBL_SUB_MENU" (SID, SID_MENU, NOMBRE_SUB_MENU, POSICION, LINK, DESCRIPCION) VALUES 
((select max(SID)+1 from TBL_SUB_MENU), '2', 'Conciliaci�n SALCOBRAND/PREUNIC', '11', 'conciliacionSalcoPreunicIndexAction', 'Submen� de conciliaci�n SALCO/PREUNIC - CORE');

Insert into TBL_SUB_MENU (SID, SID_MENU, NOMBRE_SUB_MENU, DESCRIPCION, POSICION, LINK) 
values (
(SELECT MAX(SID) FROM TBL_SUB_MENU) + 1, 
(SELECT SID FROM TBL_MENU WHERE NOMBRE_MENU = 'Conciliaci�n' OR NOMBRE_MENU = 'Conciliacion'),
'Conciliaci�n SALCOBRAND/PREUNIC (Pagos)',
'Submen� de conciliaci�n para Pagos',
(SELECT MAX(POSICION) FROM TBL_SUB_MENU WHERE SID_MENU = (SELECT SID FROM TBL_MENU WHERE NOMBRE_MENU = 'Conciliaci�n' OR NOMBRE_MENU = 'Conciliacion')) + 1,
'conciliacionSalcoPreunicPagosIndexAction'
);

COMMIT;