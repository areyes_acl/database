create or replace PROCEDURE       "SP_SIG_CARGA_INCOMING_PREUNIC" (warning       OUT VARCHAR2,
                                             cod_error      OUT NUMBER
                                                       
)
IS
   
        voCursor  Sys_RefCursor:=null;
        prfCursor Sys_RefCursor:=null;

-- Proyecto: SIG
-- Nombre Objeto:sSP_SIG_CARGA_INCOMING_PREUNIC
-- By: Developer || ACL PTM | 21/12/2020
-- VersiÃƒÂ³n : 1.1
-- Descripcion:carga TBL_INCOMING, TBL_PDS Y TBL_PAGOS_RECIBIDOS dependiendo del tipo de transacciÃ²n desde TMP_INCOMING_PREUNIC
-- ModificaciÃƒÂ³n Angel Blanco
-- DescripciÃƒÂ³n se agrega carga PDS Internacional.

BEGIN
      DECLARE
               SID                                      SIG.TMP_INCOMING_PREUNIC.SID%TYPE                        ; 
FECHA_PROCESO  SIG.TMP_INCOMING_PREUNIC.FECHA_PROCESO %TYPE  ;
           MIT                                       SIG.TMP_INCOMING_PREUNIC.MIT%TYPE                           ; 
           RUT                                       SIG.TMP_INCOMING_PREUNIC.RUT%TYPE        ; 
           NRO_TARJETA                               SIG.TMP_INCOMING_PREUNIC.NRO_TARJETA%TYPE                   ; 
           MONTO_TRANSAC                             SIG.TMP_INCOMING_PREUNIC.MONTO_TRANSAC%TYPE                 ; 
           FECHA_HR_TRASAC                           SIG.TMP_INCOMING_PREUNIC.FECHA_HR_TRASAC%TYPE               ; 
           MCC                                       SIG.TMP_INCOMING_PREUNIC.MCC%TYPE                           ; 
           DATOS_REFERENCIA_ADQUIRENTE               SIG.TMP_INCOMING_PREUNIC.DATOS_REFERENCIA_ADQUIRENTE%TYPE   ; 
           COD_IDENTIFICACION_ADQUIRENTE             SIG.TMP_INCOMING_PREUNIC.COD_IDENTIFICACION_ADQUIRENTE%TYPE ; 
           CODIGO_AUTORIZACION                       SIG.TMP_INCOMING_PREUNIC.CODIGO_AUTORIZACION%TYPE           ; 
           NOMBRE_COMERCIO                  		 SIG.TMP_INCOMING_PREUNIC.NOMBRE_COMERCIO%TYPE      ; 
           CIUDAD_COMERCIO                  		 SIG.TMP_INCOMING_PREUNIC.CIUDAD_COMERCIO%TYPE      ; 
           ORIGEN_TRANSACCION                        SIG.TMP_INCOMING_PREUNIC.ORIGEN_TRANSACCION%TYPE                      ; 
           CUOTAS                                    SIG.TMP_INCOMING_PREUNIC.CUOTAS%TYPE                      ; 
           TIPO_CUOTA                                SIG.TMP_INCOMING_PREUNIC.TIPO_CUOTA%TYPE                      ; 
           VALOR_CUOTA                               SIG.TMP_INCOMING_PREUNIC.VALOR_CUOTA%TYPE                      ; 
           COMISION_COMERCIO                         SIG.TMP_INCOMING_PREUNIC.COMISION_COMERCIO%TYPE                      ; 
           seqtmp               				 	            SIG.TBL_INCOMING.SID%TYPE := NULL;
           AVANCES_TRANFERENCIA                       NUMBER(1,0);
            l_datos_adicionales                          VARCHAR2(2000)                                       ;
          l_datos_adicionales_2  VARCHAR2(2000)                                       ;

     BEGIN
        
           --DBMS_OUTPUT.PUT_LINE('Entrando');
           dbms_output.enable(1);

                OPEN voCursor FOR
                SELECT SID,
                      FECHA_PROCESO,
                      MIT,
                      RUT,
                      NRO_TARJETA,
                      MONTO_TRANSAC,
                      FECHA_HR_TRASAC,
                      MCC,
                      DATOS_REFERENCIA_ADQUIRENTE,
                      COD_IDENTIFICACION_ADQUIRENTE,
                      CODIGO_AUTORIZACION,
                      CUOTAS,
                      TIPO_CUOTA,
                      VALOR_CUOTA,
                      COMISION_COMERCIO,
                      NOMBRE_COMERCIO,
                      CIUDAD_COMERCIO ,
                      ORIGEN_TRANSACCION
                         
                 FROM  SIG.TMP_INCOMING_PREUNIC;
                    prfCursor:=voCursor;

        LOOP                                                                                           
                    FETCH prfCursor                                                                              
                    INTO  SID,
                          FECHA_PROCESO,
                          MIT,                                        
                          RUT ,                  
                          NRO_TARJETA,                               
                          MONTO_TRANSAC,                             
                          FECHA_HR_TRASAC,                          
                          MCC,                                       
                          DATOS_REFERENCIA_ADQUIRENTE ,              
                          COD_IDENTIFICACION_ADQUIRENTE,             
                          CODIGO_AUTORIZACION ,
                          CUOTAS,
                          TIPO_CUOTA,
                          VALOR_CUOTA,
                          COMISION_COMERCIO,
                          NOMBRE_COMERCIO,
                          CIUDAD_COMERCIO,
                          ORIGEN_TRANSACCION ;  

                    EXIT WHEN prfCursor%NOTFOUND; 

					  --SOLO PRUEBAS
				   --DBMS_OUTPUT.PUT_LINE('salida fetch');
          
				  SELECT seq_incoming.NEXTVAL
					INTO   seqtmp
					FROM   DUAL;
    
					IF MIT = '08' THEN
						--DBMS_OUTPUT.PUT_LINE('Insertando datos en la tabla  TBL_PAGOS_RECIBIDOS');
               IF  NRO_TARJETA IS NULL  OR  FECHA_HR_TRASAC IS NULL  OR CODIGO_AUTORIZACION IS NULL OR MONTO_TRANSAC IS NULL THEN
  
             
              /* **************************************************************** */
               /*  se inserta en tbl_except_incoming  si viene incompleto el xkey */
               /* *************************************************************** */    
               
             l_datos_adicionales := RUT;
                                     

             l_datos_adicionales_2 := CUOTAS||'-'||TIPO_CUOTA||'-'||VALOR_CUOTA||'-'||COMISION_COMERCIO;
             
                      
                                      
             INSERT INTO SIG.TBL_EXCEPT_INCOMING (SID,             
                     LOG_CARGA,
                     FECHA_INCOMING,
                     NRO_TARJETA,
                     FECHA_HR_TRASAC,
                     DATOS_REFERENCIA_ADQUIRENTE,
                     DATOS_ADICIONALES_1,
                     DATOS_ADICIONALES_2,
                     DATOS_ADICIONALES_5,
                     OPERADOR,
                     MIT,
                     NOMBRE_UBIC_ACEP_TARJETA,
                     COD_IDENTIFICACION_ADQUIRENTE,
                     MCC,
                     MONTO_TRANSAC,
                     CODIGO_AUTORIZACION)
              VALUES(SIG.SEQ_EXCEPT_INCOMING.NEXTVAL,
                     'Datos requeridos incompletos',
                      sysdate,
                      NRO_TARJETA,
                      FECHA_HR_TRASAC,
                      DATOS_REFERENCIA_ADQUIRENTE,
                      l_datos_adicionales,
                      l_datos_adicionales_2,
                      'I',
                      ORIGEN_TRANSACCION,
                      MIT,
                      NOMBRE_COMERCIO || ' ' || CIUDAD_COMERCIO,
                      COD_IDENTIFICACION_ADQUIRENTE,
                      MCC,
                      MONTO_TRANSAC,
                      CODIGO_AUTORIZACION); 
                  COMMIT;  
            
              ELSE    
						INSERT INTO TBL_PAGOS_RECIBIDOS(
								SID,
								FECHA_CONTABLE,
								RUT,
								NUM_TARJETA, 
								MONTO_FINAL,
								FECHA_PAGO,
								HORA_PAGO,
								MCC,
								TRANSACCION_ID,
								LOCAL_PAGO,
								CODIGO_AUTORIZACION,
								DESCRIPCION,
								OPERADOR)
						VALUES  (SEQ_TBL_PAGOS_RECIBIDOS.NEXTVAL,
								 FECHA_PROCESO, --fecha incoming,
								 RUT,
								 NRO_TARJETA,
								 MONTO_TRANSAC,
                 FECHA_HR_TRASAC,
                 to_char(FECHA_HR_TRASAC,'HH:mm:ss'),
								 MCC,
								 DATOS_REFERENCIA_ADQUIRENTE,
								 COD_IDENTIFICACION_ADQUIRENTE,
								 CODIGO_AUTORIZACION,
								 NOMBRE_COMERCIO || ' ' || CIUDAD_COMERCIO,
								 ORIGEN_TRANSACCION); 
						COMMIT;
            END IF;
				ELSE 
					IF MIT = '09' THEN
						--DBMS_OUTPUT.PUT_LINE('Avance con transferencia');
            AVANCES_TRANFERENCIA:=0;
					ELSE 
          
              IF  NRO_TARJETA IS NULL  OR  FECHA_HR_TRASAC IS NULL  OR CODIGO_AUTORIZACION IS NULL OR MONTO_TRANSAC IS NULL THEN
  
             
              /* **************************************************************** */
               /*  se inserta en tbl_except_incoming  si viene incompleto el xkey */
               /* *************************************************************** */    
               
             l_datos_adicionales := RUT;
                                     

             l_datos_adicionales_2 := CUOTAS||'-'||TIPO_CUOTA||'-'||VALOR_CUOTA||'-'||COMISION_COMERCIO;
             
                      
                                      
             INSERT INTO SIG.TBL_EXCEPT_INCOMING (SID,             
                     LOG_CARGA,
                     FECHA_INCOMING,
                     NRO_TARJETA,
                     FECHA_HR_TRASAC,
                     DATOS_REFERENCIA_ADQUIRENTE,
                     DATOS_ADICIONALES_1,
                     DATOS_ADICIONALES_2,
                     DATOS_ADICIONALES_5,
                     OPERADOR,
                     MIT,
                     NOMBRE_UBIC_ACEP_TARJETA,
                     COD_IDENTIFICACION_ADQUIRENTE,
                     MCC,
                     MONTO_TRANSAC,
                     CODIGO_AUTORIZACION)
              VALUES(SIG.SEQ_EXCEPT_INCOMING.NEXTVAL,
                     'Datos requeridos incompletos',
                      sysdate,
                      NRO_TARJETA,
                      FECHA_HR_TRASAC,
                      DATOS_REFERENCIA_ADQUIRENTE,
                      l_datos_adicionales,
                      l_datos_adicionales_2,
                      'I',
                      ORIGEN_TRANSACCION,
                      MIT,
                      NOMBRE_COMERCIO || ' ' || CIUDAD_COMERCIO,
                      COD_IDENTIFICACION_ADQUIRENTE,
                      MCC,
                      MONTO_TRANSAC,
                      CODIGO_AUTORIZACION);
                  COMMIT;
              ELSE
						 INSERT INTO TBL_INCOMING(
								SID,
								FECHA_INCOMING,
								MIT,
								COD_IDENT_ACEP_TARJETA,
								NRO_TARJETA,
								MONTO_TRANSAC,
								FECHA_HR_TRASAC,
								MCC,
								DATOS_REFERENCIA_ADQUIRENTE,
								COD_IDENTIFICACION_ADQUIRENTE,
								CODIGO_AUTORIZACION,
								NOMBRE_UBIC_ACEP_TARJETA,
								OPERADOR)
						 VALUES (seqtmp,
								 sysdate,
								 MIT,
								 RUT,
								 NRO_TARJETA,
								 MONTO_TRANSAC,
								 FECHA_HR_TRASAC,
								 MCC,
								 DATOS_REFERENCIA_ADQUIRENTE,
								 COD_IDENTIFICACION_ADQUIRENTE,
								 CODIGO_AUTORIZACION,
								 NOMBRE_COMERCIO || ' ' || CIUDAD_COMERCIO,
								 ORIGEN_TRANSACCION	);
									
									
						 INSERT INTO TBL_PDS(
								SID,
								TRANSACCION,
								P1004,
								P1001,
								P1005,
								P1003)
						VALUES  (SIG.SEQ_PDS.NEXTVAL,
								 seqtmp,
								 CUOTAS,
								 TIPO_CUOTA,
								 VALOR_CUOTA,
								 COMISION_COMERCIO);
      
						COMMIT;
				END IF;
			END IF;
      END IF;
              
                END LOOP;
               -- DBMS_OUTPUT.PUT_LINE('salida loop');
                CLOSE prfCursor;
               -- DBMS_OUTPUT.PUT_LINE('cierre cursor');
     END;            

    --tabla SIG.TMP_INCOMING_PREUNIC SE LIMPIA
 execute immediate 'truncate table SIG.TMP_INCOMING_PREUNIC'; 
 COMMIT;
EXCEPTION
WHEN  NO_DATA_FOUND     THEN
            DBMS_OUTPUT.PUT_LINE('NO HAY REGISTROS EN TABLA SIG.TMP_INCOMING_PREUNIC');
WHEN  OTHERS THEN
      warning:=SQLERRM;   
      cod_error:=SQLCODE;
      DBMS_OUTPUT.PUT_LINE('ERROR : '||warning||' SQLCODE : '||cod_error);   
--insercion en tabla log error en caso de algun error desconocido          
     INSERT INTO LOG_ERROR 
			    (SID, 
                FECHA_INSERCION, 
                NOMBRE_SP,
                MSG_ERROR)
        VALUES ( SIG.SEQ_LOG_ERROR.NEXTVAL,
                 SYSDATE,
                'SP_SIG_CARGA_INCOMING_PREUNIC',
                        cod_error||warning );                       
                COMMIT;
END SP_SIG_CARGA_INCOMING_PREUNIC;