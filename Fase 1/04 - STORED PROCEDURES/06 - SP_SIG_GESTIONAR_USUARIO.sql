create or replace PROCEDURE       "SP_SIG_GESTIONAR_USUARIO" (
                                            p_sid                   IN NUMBER,
                                            p_id_usuario            IN VARCHAR2,
                                            p_nombre                IN VARCHAR2,
                                            p_apellido              IN VARCHAR2,
                                            p_apellido_paterno      IN VARCHAR2,
                                            p_apellido_materno      IN VARCHAR2,
                                            p_email                 IN VARCHAR2,
                                            p_fono                  IN VARCHAR2,
                                            p_super_usuario         IN NUMBER,
                                            p_estado                IN NUMBER,
                                            p_contrasena            IN VARCHAR2,
                                            p_sid_usr_creacion      IN NUMBER,
                                            cod_error             OUT NUMBER,
                                            warning               OUT VARCHAR2,
                                            prfCursor             OUT Sys_RefCursor
                                           )
IS
    voCursor Sys_RefCursor:=null;  
    seq_usuario NUMBER;
    fecha_vigencia DATE;
    nro_dias_vigencia NUMBER;
    aplicativo    VARCHAR2(20);
    estado_usuario    NUMBER;
    nro_intentos     NUMBER;
BEGIN
  cod_error:=0;
  aplicativo := 'politicas';


  IF p_sid IS NULL 
    THEN

    -- SE OBTIENE LA SECUENCIA DEL USUARIO
    SELECT SIG.SEQ_TBL_USUARIO_IG.nextval INTO seq_usuario FROM DUAL;

    -- SE INSERTA USUARIO EN LA TABLA  
     INSERT INTO SIG.TBL_USUARIO_IG (SID,ID_USUARIO,NOMBRE,APELLIDO,CLAVE, APEPAT,APEMAT,EMAIL,FONO,SU,ESTADO,SID_USR_CREACION,FECHA_CREACION) 
     VALUES (seq_usuario, p_id_usuario, p_nombre,p_apellido, p_contrasena, p_apellido_paterno,
            p_apellido_materno, p_email, p_fono, p_super_usuario, p_estado, p_sid_usr_creacion,SYSDATE);

    SIG.SP_SIG_GUARDAR_CONTRASENA(seq_usuario,p_contrasena,'');

    ELSE
      -- Se consulta el nro de intentos y el estado del usuario
      SELECT  USR.ESTADO, USR.INTENTOS INTO estado_usuario, nro_intentos FROM SIG.TBL_USUARIO_IG USR WHERE USR.SID = p_sid; 

      -- SI UN USUARIO ESTA BLOQUEADO Y SE DESBLOQUEA POR ADMINISTRADOR SE DEBE REESTRABLECER EL NRO DE INTENTOS A 0 
      IF(estado_usuario = 2 AND p_estado = 1 AND nro_intentos >  0 ) THEN
        nro_intentos := 0;
      END IF;

      UPDATE SIG.TBL_USUARIO_IG TU SET NOMBRE = p_nombre, ID_USUARIO = p_id_usuario, APELLIDO = p_apellido, CLAVE = p_contrasena, APEPAT = p_apellido_paterno, 
                                        APEMAT = p_apellido_materno,EMAIL = p_email,
                                        FONO = p_fono,SU =  p_super_usuario,ESTADO = p_estado,
                                        SID_USR_MODIFICACION = p_sid_usr_creacion, FECHA_MODIFICACION= SYSDATE,
                                        INTENTOS = nro_intentos
                                      WHERE TU.SID = p_sid;

      IF p_contrasena IS NOT NULL THEN
          SIG.SP_SIG_GUARDAR_CONTRASENA(p_sid,p_contrasena,aplicativo);   
       END IF ;

    END IF;

    COMMIT;
    prfCursor := voCursor;


EXCEPTION 
    WHEN DUP_VAL_ON_INDEX THEN
         ROLLBACK;
        cod_error:=SQLCODE;
        warning:=SQLERRM ||'- ' || DBMS_UTILITY.format_error_backtrace;
        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_GUARDAR_USUARIO', cod_error || '-' || warning);

    WHEN OTHERS  THEN
        ROLLBACK;
        cod_error:=SQLCODE;
        warning:=SQLERRM ||'- ' || DBMS_UTILITY.format_error_backtrace;
        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_GUARDAR_USUARIO', cod_error || '-' || warning);
END SP_SIG_GESTIONAR_USUARIO;