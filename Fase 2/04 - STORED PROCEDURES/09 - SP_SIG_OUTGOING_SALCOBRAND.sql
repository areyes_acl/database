create or replace PROCEDURE SP_SIG_OUTGOING_SALCOBRAND ( 																
																	warning        OUT VARCHAR2,
                                                                    cod_error      OUT NUMBER,
                                                                    l_ind_Ok_Ko    OUT NUMBER)
IS
    prfCursor                                    Sys_RefCursor:=null;
    voCursor                                     Sys_RefCursor:=null ;
    prfCursorPagos                                    Sys_RefCursor:=null;
    voCursorPagos                                     Sys_RefCursor:=null ;
    NO_DATA                                      EXCEPTION;


BEGIN
DECLARE
           SID                                      SIG.TMP_OUTGOING_SALCOBRAND.SID%TYPE                        ; 
           FECHA_INCOMING  SIG.TMP_OUTGOING_SALCOBRAND.FECHA_INCOMING%TYPE  ;
           MIT                                       SIG.TMP_OUTGOING_SALCOBRAND.MIT%TYPE                           ; 
           RUT                                       SIG.TMP_OUTGOING_SALCOBRAND.RUT%TYPE        ; 
           NRO_TARJETA                               SIG.TMP_OUTGOING_SALCOBRAND.NRO_TARJETA%TYPE                   ; 
           MONTO_TRANSAC                             SIG.TMP_OUTGOING_SALCOBRAND.MONTO_TRANSAC%TYPE                 ; 
           FECHA_HR_TRASAC                           SIG.TMP_OUTGOING_SALCOBRAND.FECHA_HR_TRASAC%TYPE               ; 
           MCC                                       SIG.TMP_OUTGOING_SALCOBRAND.MCC%TYPE                           ; 
           DATOS_REFERENCIA_ADQUIRENTE               SIG.TMP_OUTGOING_SALCOBRAND.DATOS_REFERENCIA_ADQUIRENTE%TYPE   ; 
           COD_IDENTIFICACION_ADQUIRENTE             SIG.TMP_OUTGOING_SALCOBRAND.COD_IDENTIFICACION_ADQUIRENTE%TYPE ; 
           CODIGO_AUTORIZACION                       SIG.TMP_OUTGOING_SALCOBRAND.CODIGO_AUTORIZACION%TYPE           ; 
           NOMBRE_COMERCIO                  		 SIG.TMP_OUTGOING_SALCOBRAND.NOMBRE_COMERCIO%TYPE      ; 
           CIUDAD_COMERCIO                  		 SIG.TMP_OUTGOING_SALCOBRAND.CIUDAD_COMERCIO%TYPE      ; 
           ORIGEN_TRANSACCION                        SIG.TMP_OUTGOING_SALCOBRAND.ORIGEN_TRANSACCION%TYPE                      ; 
           CUOTAS                                    SIG.TMP_OUTGOING_SALCOBRAND.CUOTAS%TYPE                      ; 
           TIPO_CUOTA                                SIG.TMP_OUTGOING_SALCOBRAND.TIPO_CUOTA%TYPE                      ; 
           VALOR_CUOTA                               SIG.TMP_OUTGOING_SALCOBRAND.VALOR_CUOTA%TYPE                      ; 
           COMISION_COMERCIO                         SIG.TMP_OUTGOING_SALCOBRAND.COMISION_COMERCIO%TYPE                      ; 
           NOMBRE_UBIC_ACEP_TARJETA  SIG.TBL_INCOMING.NOMBRE_UBIC_ACEP_TARJETA%TYPE ;
           OPERADOR  SIG.TBL_INCOMING.OPERADOR%TYPE                      ;


         
                        out_fecha_cab                                varchar(6);
                        vmonto_original_aux                          VARCHAR2(24):='';
                        vmonto_transac_aux                           VARCHAR2(12):='';
                        vmonto_conciliacion_aux                      VARCHAR2(12):='';
                        l_acc_trans_aux                              NUMBER(8);



   
     BEGIN

                   
             /* Cursor Incoming */
       OPEN voCursor FOR
        SELECT  	TI.SID,
								TI.FECHA_INCOMING,
								TI.MIT,
								TI.COD_IDENT_ACEP_TARJETA,
								TI.NRO_TARJETA,
								TI.MONTO_TRANSAC,
								TI.FECHA_HR_TRASAC,
								TI.MCC,
								TI.DATOS_REFERENCIA_ADQUIRENTE,
								TI.COD_IDENTIFICACION_ADQUIRENTE,
								TI.CODIGO_AUTORIZACION,
								TI.NOMBRE_UBIC_ACEP_TARJETA,
								TI.OPERADOR,
                TP.P1004,-- CUOTAS,
								TP.P1001,--TIPO_CUOTA
								TP.P1005,--VALOR CUOTA
								TP.P1003-- COMISION COMERCIO
            FROM TBL_INCOMING TI
            JOIN SIG.TBL_PDS TP ON (TI.SID = TP.transaccion)
            join TBL_CONC_SALCOBRAND CS on  CS.sid_salcobrand=TI.SID
            WHERE TI.SID IN (SELECT SID_SALCOBRAND FROM TBL_CONC_SALCOBRAND WHERE SID_ESTADO != 1 )
            AND CS.ORIGEN='I'
            AND TO_CHAR(CS.FECHA_REGISTRO, 'DD-MON-YY') = TO_CHAR(SYSDATE, 'DD-MON-YY');
            prfCursor:=voCursor;
            
          OPEN voCursorPagos FOR
             SELECT  		TI.SID,
								TI.FECHA_CONTABLE,
								RUT,
								NUM_TARJETA, 
								MONTO_FINAL,
								FECHA_PAGO,
								--HORA_PAGO,
								MCC,
								TRANSACCION_ID,
								LOCAL_PAGO,
								CODIGO_AUTORIZACION,
								DESCRIPCION,
								TI.OPERADOR
                FROM TBL_CONC_SALCOBRAND CS
            join SIG.TBL_PAGOS_RECIBIDOS TI on  CS.sid_salcobrand=TI.SID
            WHERE TI.SID IN (SELECT SID_SALCOBRAND FROM TBL_CONC_SALCOBRAND WHERE SID_ESTADO != 1 )
            AND TO_CHAR(CS.FECHA_REGISTRO, 'DD-MON-YY') = TO_CHAR(SYSDATE, 'DD-MON-YY')
            AND CS.ORIGEN='P';
            prfCursorPagos:=voCursorPagos;
            
            	            
      	   --limpiar la tabla antes de comenzar a insertar por si quedo basura de algun proceso anterior
          execute immediate 'truncate table SIG.TMP_OUTGOING_SALCOBRAND';

			   LOOP
                    FETCH prfCursor
               INTO  SID,
                      FECHA_INCOMING,
                      MIT,
                      RUT,
                      NRO_TARJETA,
                      MONTO_TRANSAC,
                      FECHA_HR_TRASAC,
                      MCC,
                      DATOS_REFERENCIA_ADQUIRENTE,
                      COD_IDENTIFICACION_ADQUIRENTE,
                      CODIGO_AUTORIZACION,
                      NOMBRE_UBIC_ACEP_TARJETA,
                      ORIGEN_TRANSACCION,
                      CUOTAS,
                      TIPO_CUOTA,
                      VALOR_CUOTA,
                      COMISION_COMERCIO;

             EXIT WHEN prfCursor%NOTFOUND;
            --insercion en TMP_OUTGOING
            
            
          INSERT INTO TMP_OUTGOING_SALCOBRAND(
                 SID,
                 FECHA_INCOMING,
								 MIT,
								 RUT,
								 NRO_TARJETA,
								 MONTO_TRANSAC,
								 FECHA_HR_TRASAC,
								 MCC,
								 DATOS_REFERENCIA_ADQUIRENTE,
								 COD_IDENTIFICACION_ADQUIRENTE,
								 CODIGO_AUTORIZACION,
								 NOMBRE_COMERCIO,
                 CIUDAD_COMERCIO,
								 ORIGEN_TRANSACCION,
                 CUOTAS,
								 TIPO_CUOTA,
								 VALOR_CUOTA,
								 COMISION_COMERCIO,
                 DISPONIBLE,
                 PAIS_COMERCIO)
          VALUES (seq_TMP_OUTGOING_SALCOBRAND.NEXTVAL,
                  FECHA_INCOMING,   
                	MIT,
                	RUT,
                	NRO_TARJETA,
                	MONTO_TRANSAC,
                  FECHA_HR_TRASAC,
                	MCC,
                  DATOS_REFERENCIA_ADQUIRENTE,
                  COD_IDENTIFICACION_ADQUIRENTE,
                  CODIGO_AUTORIZACION,
                  SUBSTR(NOMBRE_UBIC_ACEP_TARJETA,0,10),
                  SUBSTR(NOMBRE_UBIC_ACEP_TARJETA,12,19),
                  to_number (ORIGEN_TRANSACCION, '9')-2,
                  CUOTAS,
                  TIPO_CUOTA,
                  VALOR_CUOTA,
                  COMISION_COMERCIO,
                  '0',
                  'CL ');
COMMIT;
  END LOOP;

       
       			   LOOP
                    FETCH prfCursorPagos
               INTO  SID,
								FECHA_INCOMING,
								RUT,
								NRO_TARJETA, 
								MONTO_TRANSAC,
								FECHA_HR_TRASAC,
								--HORA_PAGO,
								MCC,
								DATOS_REFERENCIA_ADQUIRENTE,
								COD_IDENTIFICACION_ADQUIRENTE,
								CODIGO_AUTORIZACION,
								NOMBRE_UBIC_ACEP_TARJETA,
								OPERADOR;
                --ORIGEN;

             EXIT WHEN prfCursorPagos%NOTFOUND;
            --insercion en TMP_OUTGOING
            
            
          INSERT INTO TMP_OUTGOING_SALCOBRAND(
                 SID,
                 FECHA_INCOMING,
								 MIT,
								 RUT,
								 NRO_TARJETA,
								 MONTO_TRANSAC,
								 FECHA_HR_TRASAC,
								 MCC,
								 DATOS_REFERENCIA_ADQUIRENTE,
								 COD_IDENTIFICACION_ADQUIRENTE,
								 CODIGO_AUTORIZACION,
								 NOMBRE_COMERCIO,
                 CIUDAD_COMERCIO,
								 ORIGEN_TRANSACCION,
                 DISPONIBLE,
                 PAIS_COMERCIO)
          VALUES (seq_TMP_OUTGOING_SALCOBRAND.NEXTVAL,
                  FECHA_INCOMING,   
                	'08',
                	RUT,
                	NRO_TARJETA,
                	MONTO_TRANSAC,
                  FECHA_HR_TRASAC,
                	MCC,
                  DATOS_REFERENCIA_ADQUIRENTE,
                  COD_IDENTIFICACION_ADQUIRENTE,
                  CODIGO_AUTORIZACION,
                  SUBSTR(NOMBRE_UBIC_ACEP_TARJETA,0,10),
                  SUBSTR(NOMBRE_UBIC_ACEP_TARJETA,12,19),
                  to_number (OPERADOR, '9')-2,
                  '0',
                  'CL ');
                  
COMMIT;
  END LOOP;
            
            
        
 END;
 DBMS_OUTPUT.PUT_LINE('salida loop');
    CLOSE prfCursor;

 DBMS_OUTPUT.PUT_LINE('variable de retorno -->'||l_ind_Ok_Ko);
 COMMIT;

EXCEPTION
        WHEN NO_DATA THEN
             warning:=SQLERRM;
             cod_error:=SQLCODE;
             --indicador outgoing no se encuentran archivos de salida outgoing
             l_ind_Ok_Ko:= 1;
              DBMS_OUTPUT.PUT_LINE('excepcion NO_DATA_FOUND -->'||l_ind_Ok_Ko);
        WHEN OTHERS THEN
             --variables de errores
             warning:=SQLERRM;
             cod_error:=SQLCODE;
             --indicador de salido no ok en caso de algun error en la ejecucion de query
             l_ind_Ok_Ko:=-1;
             DBMS_OUTPUT.PUT_LINE('excepcion others -->'||l_ind_Ok_Ko);
              DBMS_OUTPUT.PUT_LINE(DBMS_UTILITY.format_error_backtrace);
             ROLLBACK;
--insercion en tabla log error en caso de algun error desconocido
INSERT INTO LOG_ERROR (SID,
                       FECHA_INSERCION,
                       NOMBRE_SP,
                       MSG_ERROR)
               VALUES ( SEQ_LOG_ERROR.NEXTVAL,
                        SYSDATE,
                        'SP_SIG_OUTGOING_SALCOBRAND',
                        warning||cod_error || DBMS_UTILITY.format_error_backtrace );
                        commit;
END SP_SIG_OUTGOING_SALCOBRAND;