SET DEFINE OFF;

Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG), 1,'PROCESS_AUTHORIZED_TRANSACTIONS_SBPAY','Proceso que realiza la lectura  del archivo de autorizaciones del core de sbpay ');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG),1,'DOWNLOAD_INC_TECNOCOM','Proceso de descarga de archivo Incoming desde Tecnocom');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG),1,'PROCESS_INC_TECNOCOM','Proceso que realiza el procesamiento de la interfaz de Tecnocom');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG),1,'DOWNLOAD_LOG_AUTHORIZED_SBPAY','Proceso que realiza la descarga de la interfaz del core de autorizaciones desde el sftp de sbpay');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG),1,'PROCESS_CONCI_VISA_CORE','Proceso que realiza la conciliación de Visa y el core de sbpay');

Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((select max(sid)+1 from tbl_cron_config),1,'GENERAR_ARCHIVO_OUTGOING_CONTABLE','Proceso encargado de generar el archivo Outgoing Contable');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((select max(sid)+1 from tbl_cron_config),1,'GENERAR_ARCHIVO_OUTGOING_CARGO_ABONO','Proceso encargado de generar el archivo Outgoing CARGOS Y ABONOS');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((select max(sid)+1 from tbl_cron_config),1,'GENERAR_ARCHIVO_OUTGOING_TECNOCOM','Proceso encargado de generar el archivo Outgoing TECNOCOM');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((select max(sid)+1 from tbl_cron_config),1,'SUBIR_ARCHIVO_OUTGOING_CONTABLE','Proceso encargado de SUBIR el archivo Outgoing Contable');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((select max(sid)+1 from tbl_cron_config),1,'SUBIR_ARCHIVO_OUTGOING_CARGO_ABONO','Proceso encargado de SUBIR el archivo Outgoing CARGOS Y ABONOS');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((select max(sid)+1 from tbl_cron_config),1,'SUBIR_ARCHIVO_OUTGOING_TECNOCOM','Proceso encargado de SUBIR el archivo Outgoing TECNOCOM');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG), 1,'PROCESAR_INCOMING_VISA','Proceso que realiza la lectura del archivo INCOMING VISA.');
Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG), 1,'DOWNLOAD_INCOMING_VISA','Proceso que realiza la descarga de los archivos INCOMING VISA.');

Insert into SIG.TBL_CRON_CONFIG (SID,ESTADO,CODIGO,DESCRIPCION) values ((SELECT MAX(SID)+ 1 FROM TBL_CRON_CONFIG), 1,'GENERATE_AND_PROCESS_REJECTED','Proceso que realiza la generacion y procesamiento de rechazos.');


commit;