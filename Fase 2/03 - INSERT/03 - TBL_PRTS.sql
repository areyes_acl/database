/* PATH FILES SERVIPAG*/

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_SERVIPAG',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/SERVIPAG/',
'Ruta en el servidor de SBPAY donde se dejan los archivos Entrada SERVIPAG descargados','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_SERVIPAG_BKP',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/SERVIPAG/BKP/',
'Ruta donde SBPAY dejar� los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_SERVIPAG_ERROR',
'/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/SERVIPAG/ERROR/',
'Ruta donde SBPAY dejar� los archivos con error que no pudieron ser subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_SALIDA_SERVIPAG','MATIKARDBPREN',
'Formato Nomenclatura arch SALIDA SERVIPAG "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_SALIDA_SERVIPAG','txt','Extension del archivo SALIDA SERVIPAG',
'admin',to_date('30-NOV-20','DD-MON-RR'),null);

INSERT INTO SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_SALIDA_SERVIPAG', '^MATIKARDBPREN[a-zA-Z_0-9].*(txt|TXT)$', 
'Expresion regular para validar el formato de los archivos en la salida', 'admin', SYSDATE, SYSDATE);


Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_ENTRADA_SERVIPAG','MATIKARDBPREN',
'Formato Nomenclatura arch ENTRADA SERVIPAG "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_ENTRADA_SERVIPAG','txt','Extension del archivo ENTRADA SERVIPAG',
'admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_ENTRADA_SERVIPAG','^MATIKARDBPREN[a-zA-Z_0-9].*(txt|TXT)$',
'Expresion regular para validar el formato de los archivos','admin',to_date(sysdate,'DD-MON-RR'),null);



Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_ENTRADA_SERVIPAG','172.25.7.169',
'IP del servidor SFTP de SERVIPAG donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_ENTRADA_SERVIPAG','22',
'PUERTO del servidor SFTP de SERVIPAG donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_ENTRADA_SERVIPAG','sigcron',
'USER del servidor SFTP de SERVIPAG donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_ENTRADA_SERVIPAG','G116st3lth',
'PASS  del servidor SFTP de SERVIPAG donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DOWNLOAD_ENTRADA_SERVIPAG','/home/sigcron/SBPAY_SFTP/IN/SERVIPAG/',
'Ruta del sftp donde se DESCARGA archivo SALIDA de SERVIPAG ','admin',to_date(sysdate,'DD-MON-RR'),null);


Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_UP_SALIDA_SERVIPAG','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/SERVIPAG/',
'Ruta de subida local del Archivo Salida Servipag','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_IN_UPLD_SALIDA_SERVIPAG','/home/sigcron/SBPAY_SFTP/OUT/SERVIPAG/',
'Ruta del sftp donde se sube archivo SALIDA SERVIPAG','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_UPLOAD_SALIDA_SERVIPAG','G116st3lth',
'PASS  del servidor SFTP de Tecnocom donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_UPLOAD_SALIDA_SERVIPAG','sigcron',
'USER  del servidor SFTP de Tecnocom donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_UPLOAD_SALIDA_SERVIPAG','22',
'PUERTO del servidor SFTP de Tecnocom donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) 
values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_UPLOAD_SALIDA_SERVIPAG','172.25.7.169',
'IP del servidor SFTP de Tecnocom donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);

Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_SERVIPAG_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/SERVIPAG/BKP/','Ruta donde SBPAY dejará los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_SERVIPAG_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/SERVIPAG/ERROR/','Ruta donde SBPAY dejará los archivos con error que no pudieron ser subidos al FTP  ','admin',to_date(sysdate,'DD-MON-RR'),null);


-- Path para los files de KHIPU
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_INC_KHIPU','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/KHIPU/','Ruta en el servidor de SBPAY donde se dejan los archivos Entrada KHIPU descargados','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_INC_KHIPU_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/KHIPU/BKP/','Ruta donde SBPAY dejar? los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_INC_KHIPU_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/KHIPU/ERROR/','Ruta donde SBPAY dejar? los archivos con error que no pudieron ser subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_UP_SALIDA_KHIPU','KHIPU/SALIDA','Path donde se dejara el archivo Salida de KHIPU para ser subido','admin',to_date(sysdate,'DD-MON-RR'),to_date('16-NOV-15','DD-MON-RR'));
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_INC_KHIPU','^KHIPU_[0-9]{8}.json$','Expresion regular para validar el formato de los archivos','admin',to_date('26-JUN-19','DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_INC_KHIPU','172.25.7.169','IP del servidor SFTP de KHIPU donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_INC_KHIPU','22','PUERTO del servidor SFTP de KHIPU donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_INC_KHIPU','sigcron','USER del servidor SFTP de KHIPU donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_INC_KHIPU','G116st3lth','PASS  del servidor SFTP de KHIPU donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DOWNLOAD_INC_KHIPU','KHIPU','Ruta del sftp donde se DESCARGA archivo SALIDA de KHIPU ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_ENTRADA_KHIPU','KHIPU_','Formato Nomenclatura arch ENTRADA KHIPU "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_SALIDA_KHIPU','KHIPU_','Formato Nomenclatura arch ENTRADA KHIPU "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_SALIDA_KHIPU','txt','Formato Nomenclatura arch Salida KHIPU','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_SALIDA_KHIPU','KHIPU_[0-9]{8}.(txt|TXT)$','Formato Nomenclatura arch Salida KHIPU','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_IN_UPLD_OUTGOING_KHIPU','/home/sigcron/SBPAY_SFTP/OUT/KHIPU/','Ruta del sftp donde se sube archivo OUTGOING KHIPU','admin',to_date('30-NOV-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_UPLOAD_OUTGOING_KHIPU','G116st3lth','PASS  del servidor SFTP de SBPAY donde se sube el archivo OUTGOING','admin',to_date('30-NOV-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_UPLOAD_OUTGOING_KHIPU','sigcron','USER  del servidor SFTP de SBPAY donde se sube el archivo OUTGOING','admin',to_date('30-NOV-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_UPLOAD_OUTGOING_KHIPU','22','PUERTO del servidor SFTP de SBPAY donde se sube el archivo OUTGOING','admin',to_date('30-NOV-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_UPLOAD_OUTGOING_KHIPU','172.25.7.169','IP del servidor SFTP de SBPAY donde se sube el archivo OUTGOING','admin',to_date('30-NOV-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_UP_OUT_KHIPU','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/KHIPU/','Ruta de subida local del Archivo Salida KHIPU','admin',to_date('30-NOV-20','DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_KHIPU_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/KHIPU/BKP/','Ruta donde SBPAY dejará los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_KHIPU_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/KHIPU/ERROR/','Ruta donde SBPAY dejará los archivos con error que no pudieron ser subidos al FTP  ','admin',to_date(sysdate,'DD-MON-RR'),null);

-- Path para los files de Preunic
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_PREUNIC','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/PREUNIC/','Ruta en el servidor de SBPAY donde se dejan los archivos Entrada PREUNIC descargados','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_PREUNIC_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/PREUNIC/BKP/','Ruta donde SBPAY dejar? los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_PREUNIC_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/PREUNIC/ERROR/','Ruta donde SBPAY dejar? los archivos con error que no pudieron ser subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_UPLOAD_SALIDA_PREUNIC','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/PREUNIC/','Path donde se dejara el archivo Salida de PREUNIC para ser subido','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_ENTRADA_PREUNIC','INTERFAZ_PREUNIC_','Formato Nomenclatura arch ENTRADA PREUNIC "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_ENTRADA_PREUNIC','txt','Extension del archivo ENTRADA PREUNIC','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_ENTRADA_PREUNIC','^INTERFAZ_PREUNIC_[a-zA-Z_0-9].*(txt|TXT)$','Expresion regular para validar el formato de los archivos','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_ENTRADA_PREUNIC','172.25.7.169','IP del servidor SFTP de PREUNIC donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_ENTRADA_PREUNIC','22','PUERTO del servidor SFTP de PREUNIC donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_ENTRADA_PREUNIC','sigcron','USER del servidor SFTP de PREUNIC donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_ENTRADA_PREUNIC','G116st3lth','PASS  del servidor SFTP de PREUNIC donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DOWNLOAD_ENTRADA_PREUNIC','/home/sigcron/SBPAY_SFTP/IN/PREUNIC/','Ruta del sftp donde se DESCARGA archivo SALIDA de PREUNIC ','admin',to_date(sysdate,'DD-MON-RR'),null);
/*File Salida Preunic*/
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_SALIDA_PREUNIC','INTERFAZ_PREUNIC_', 'Formato Nomenclatura archivos SALIDA PREUNIC "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_SALIDA_PREUNIC','txt','Extension del archivo SALIDA PREUNIC','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert Into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_SALIDA_PREUNIC', '^INTERFAZ_PREUNIC_[a-zA-Z_0-9].*(txt|TXT)$', 'Expresion regular para validar el formato de los archivos en la salida', 'admin', to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_IN_UPLD_SALIDA_PREUNIC','/home/sigcron/SBPAY_SFTP/OUT/PREUNIC/','Ruta del sftp donde se sube archivo SALIDA PREUNIC','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_UPLOAD_SALIDA_PREUNIC','G116st3lth','PASS  del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_UPLOAD_SALIDA_PREUNIC','sigcron','USER  del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_UPLOAD_SALIDA_PREUNIC','22','PUERTO del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_UPLOAD_SALIDA_PREUNIC','172.25.7.169','IP del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_PREUNIC_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/PREUNIC/BKP/','Ruta donde SBPAY dejará los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_PREUNIC_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/PREUNIC/ERROR/','Ruta donde SBPAY dejará los archivos con error que no pudieron ser subidos al FTP  ','admin',to_date(sysdate,'DD-MON-RR'),null);

-- Path para los files de Salcobrand
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_SALCOBRAND','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/SALCOBRAND/','Ruta en el servidor de SBPAY donde se dejan los archivos Entrada SALCOBRAND descargados','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_SALCOBRAND_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/SALCOBRAND/BKP/','Ruta donde SBPAY dejar? los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_ENTRADA_SALCOBRAND_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/IN_SGIC/SALCOBRAND/ERROR/','Ruta donde SBPAY dejar? los archivos con error que no pudieron ser subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_UPLOAD_SALIDA_SALCOBRAND','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/SALCOBRAND/','Path donde se dejara el archivo Salida de SALCOBRAND para ser subido','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_ENTRADA_SALCOBRAND','INTERFAZ_SALCOBRAND_','Formato Nomenclatura arch ENTRADA SALCOBRAND "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_ENTRADA_SALCOBRAND','txt','Extension del archivo ENTRADA SALCOBRAND','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_ENTRADA_SALCOBRAND','^INTERFAZ_SALCOBRAND_[a-zA-Z_0-9].*(txt|TXT)$','Expresion regular para validar el formato de los archivos','admin',to_date('26-JUN-19','DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_DOWNLOAD_ENTRADA_SALCOBRAND','172.25.7.169','IP del servidor SFTP de SALCOBRAND donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_DOWNLOAD_ENTRADA_SALCOBRAND','22','PUERTO del servidor SFTP de SALCOBRAND donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_DOWNLOAD_ENTRADA_SALCOBRAND','sigcron','USER del servidor SFTP de SALCOBRAND donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_DOWNLOAD_ENTRADA_SALCOBRAND','G116st3lth','PASS  del servidor SFTP de SALCOBRAND donde se DESCARGA el archivo','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_OUT_DOWNLOAD_ENTRADA_SALCOBRAND','/home/sigcron/SBPAY_SFTP/IN/SALCOBRAND/','Ruta del sftp donde se DESCARGA archivo SALIDA de SALCOBRAND ','admin',to_date(sysdate,'DD-MON-RR'),null);
/*Files Salida Salcobrand*/
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_FILENAME_SALIDA_SALCOBRAND','INTERFAZ_SALCOBRAND_','Formato Nomenclatura archivos SALIDA SALCOBRAND "que comience con"','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','FORMAT_EXTNAME_SALIDA_SALCOBRAND','txt','Extension del archivo SALIDA SALCOBRAND','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert Into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','REGEX_FILENAME_SALIDA_SALCOBRAND', '^INTERFAZ_SALCOBRAND_[a-zA-Z_0-9].*(txt|TXT)$', 'Expresion regular para validar el formato de los archivos en la salida', 'admin', to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_IN_UPLD_SALIDA_SALCOBRAND','/home/sigcron/SBPAY_SFTP/OUT/SALCOBRAND/','Ruta del sftp donde se sube archivo SALIDA SALCOBRAND','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PASS_UPLOAD_SALIDA_SALCOBRAND','G116st3lth','PASS  del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_USER_UPLOAD_SALIDA_SALCOBRAND','sigcron','USER  del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_PORT_UPLOAD_SALIDA_SALCOBRAND','22','PUERTO del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','SFTP_IP_UPLOAD_SALIDA_SALCOBRAND','172.25.7.169','IP del servidor SFTP de Salcobrand donde se sube el archivo SALIDA','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_SALCOBRAND_BKP','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/SALCOBRAND/BKP/','Ruta donde SBPAY dejará los archivos procesados y subidos al FTP ','admin',to_date(sysdate,'DD-MON-RR'),null);
Insert into SIG.TBL_PRTS (SID,COD_GRUPO_DATO,COD_DATO,VALOR,DESCRIPCION,ID_USER,FECHA_CREACION,FECHA_MODIFICACION) values ((select max(sid)+1 from tbl_prts),'CRON_SBPAY','PATH_ACL_SALIDA_SALCOBRAND_ERROR','/u01/app/oracle/product/middleware1/user_projects/domains/sbpayApps/sigcron/OUT_SGIC/SALCOBRAND/ERROR/','Ruta donde SBPAY dejará los archivos con error que no pudieron ser subidos al FTP  ','admin',to_date(sysdate,'DD-MON-RR'),null);

