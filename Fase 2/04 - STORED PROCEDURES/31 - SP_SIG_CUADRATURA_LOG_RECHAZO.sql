create or replace PROCEDURE SP_SIG_CUADRATURA_LOG_RECHAZO (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 24/07/2013
-- Versión : 1.0
-- Descripcion: Consulta la cantidad de transacciones de frechazos y el total de transacciones de excepciones de rechazos en la carga.
                                                                  fechaProceso        IN VARCHAR2,
                                                                  filtro_operador     IN NUMBER,
                                                                  warning             OUT VARCHAR2,
                                                                  cod_error           OUT NUMBER,
                                                                  prfCursor           OUT SYS_REFCURSOR,
                                                                  cantidadRechazos    OUT NUMBER
                                                                  )
IS
    voCursor SYS_REFCURSOR:=null;
    numeroRecordRechazos NUMBER:=0;

BEGIN

            BEGIN
                        OPEN voCursor FOR

                            SELECT     SID                                   AS SID, 
                                    LOG_CARGA                             AS DESCRIPCION, 
                                    NRO_TARJETA                           AS NUMEROTARJETA,  
                                    MIT                                   AS MIT, 
                                    CODIGO_FUNCION                        AS CODIGOFUNCION, 
                                    NVL(COD_MOTIVO_MENSAJE, ' ')          AS CODIGORAZON, 
                                    MONTO_TRANSAC                         AS MONTOTRANSACCION, 
                                    MONTO_CONCILIACION                    AS MONTOCONCILIACION, 
                                    MONTO_FACTURACION                     AS MONTOFACTURACION,
                                    OPERADOR                              AS IDOPERADOR
                            FROM TBL_EXCEPT_INCOMING 
                            WHERE  TO_CHAR(FECHA_INCOMING, 'YYYYMMDD')  = fechaProceso  
                            AND DATOS_ADICIONALES_5 = 'R'
                            AND OPERADOR = filtro_operador;

                            prfCursor:=voCursor;    

                        SELECT COUNT(*) INTO numeroRecordRechazos
                        FROM TBL_GESTION_TRANSAC G, TBL_ACCION_TRANSAC A, TBL_INCOMING I  
                        WHERE 
--                        TO_CHAR(G.FECHA_INSERCION, 'YYYYMMDD')  = fechaProceso      AND 
                                                  G.ACCION_TRANSAC = A.SID                AND 
                                                  G.TRANSACCION = I.SID                   AND
                                                  I.OPERADOR = filtro_operador            AND
                                                  A.XKEY = 'REC';

                        cantidadRechazos:=numeroRecordRechazos;

            END;

            EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CUADRATURALOGPROCESOREC', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
END SP_SIG_CUADRATURA_LOG_RECHAZO;