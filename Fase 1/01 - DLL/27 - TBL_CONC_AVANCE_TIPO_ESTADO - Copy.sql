--------------------------------------------------------
--  DDL for Table TBL_CONC_AVANCE_TIPO_ESTADO
--------------------------------------------------------

  CREATE TABLE "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO" 
   (	"SID" NUMBER, 
	"NOMBRE" VARCHAR2(200 BYTE)
   );

   COMMENT ON COLUMN "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO"."SID" IS 'identificador único';
   COMMENT ON COLUMN "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO"."NOMBRE" IS 'Nombre';
--------------------------------------------------------
--  DDL for Index TBL_CONC_AVANCE_TIPO_ESTADO
--------------------------------------------------------

  CREATE UNIQUE INDEX "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO_PK" ON "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO" ("SID") ;
--------------------------------------------------------
--  Constraints for Table TBL_CONC_AVANCE_TIPO_ESTADO
--------------------------------------------------------

  ALTER TABLE "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO" MODIFY ("SID" NOT NULL ENABLE);
  ALTER TABLE "SIG"."TBL_CONC_AVANCE_TIPO_ESTADO" ADD CONSTRAINT "TBL_CONC_AVANCE_TIPO_ESTADO_PK" PRIMARY KEY ("SID")
  ENABLE;
  
  
REM INSERTING into SIG.TBL_CONC_AVANCE_TIPO_ESTADO
SET DEFINE OFF;
Insert into SIG.TBL_CONC_AVANCE_TIPO_ESTADO (SID,NOMBRE) values (1,'Conciliada');
Insert into SIG.TBL_CONC_AVANCE_TIPO_ESTADO (SID,NOMBRE) values (2,'No conciliada');

Commit;
