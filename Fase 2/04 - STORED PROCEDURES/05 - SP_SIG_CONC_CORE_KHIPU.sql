create or replace PROCEDURE "SP_SIG_CONC_CORE_KHIPU"  ( warning OUT VARCHAR2, cod_error OUT NUMBER )
IS 
/**************************
   NAME:       SP_SIG_CONC_CORE_KHIPU
   PURPOSE:

   REVISIONS:
   Ver        Date      Author Description
   ---------  --------- ------ ---------------------------------------------
   1.0     13-10-2020 acl    SP de conciliacion de interfaz KHIPU y autorizaciones del core SBPAY
   NOTES:
      Object Name:     SP_SIG_CONC_CORE_KHIPU
**************************/
BEGIN

    DECLARE   

       -- DATOS KHIPU
       -- Datos necesarios de la TBL_PAGOS_RECIBIDOS
      var_sid_KHIPU NUMBER;
      transaccion_id VARCHAR2(50);   
      monto_transac NUMBER;
      fecha_hora_transac VARCHAR2(50);   
      fecha_inc VARCHAR2(50);
      rut_cliente VARCHAR2(50);
      var_operador number;

      -- DATOS CORE
      -- Datos necesarios de la tbl_core_autor
      var_sid_core NUMBER;
      monto_transac_core NUMBER;
      fecha_transac_core VARCHAR2(50);
      fecha_inc_core VARCHAR2(50);
      codigo_autor_core VARCHAR2(50); 
      rut VARCHAR2(50);
      var_origen_transac number;  
      

      --variables conciliacion

      sidKhipuCore NUMBER;
      num_ref VARCHAR2(20);
      aut_core NUMBER;
      RUT_CORE VARCHAR2(20);
      monto_core NUMBER;
      fecha_core VARCHAR2(50);
      var_cod_auth_core VARCHAR2(50);
      var_conc_KHIPU_core number;
      var_conc_khipu_inc number;
      estadoKHIPU NUMBER;


      sidCoreKHIPU   NUMBER;
      var_monto_core NUMBER(12,2);
      var_rut_benef NUMBER;
      estadoBice   NUMBER;
      id_oper_cl VARCHAR2(50);
      var_fecha_transac VARCHAR2(50);
      var_fecha_inc VARCHAR2(50);
      estadoCore NUMBER;
      var_tarjeta_core VARCHAR2(20 BYTE);
      operador_khipu NUMBER;

      contador NUMBER;

      rr VARCHAR2(20);
      ftc VARCHAR2(50);
      montf number;
      

      /* Cursor PAGOS RECIBIDOS */
      CURSOR cursorPagos IS
       SELECT PAGO.SID AS SIDPAGO,
       TO_NUMBER(PAGO.MONTO_FINAL) AS MONTO,
       PAGO.FECHA_PAGO AS FECHATRX,
       PAGO.FECHA_CONTABLE AS FECHAINC,
       PAGO.TRANSACCION_ID AS CODAUTH,
       PAGO.RUT AS RUTCLIENTE,
       PAGO.OPERADOR AS OPERADOR 
       FROM SIG.TBL_PAGOS_RECIBIDOS PAGO
       WHERE PAGO.FECHA_CONTABLE = TO_CHAR(SYSDATE, 'DD-MON-YY', 'NLS_DATE_LANGUAGE = english')
       AND PAGO.OPERADOR = 6;


      /* Cursor CORE */
      CURSOR cursorCore IS
        SELECT CA.SID,
        TO_NUMBER(CA.MONTO_TRANSAC) AS MONTO,
        to_char(to_date(CA.FECHA_HR_TRASAC, 'DD/MM/YY'), 'DD-MON-YY', 'NLS_DATE_LANGUAGE = english') AS FEC_HR_TRX,
        CA.FECHA_INCOMING AS FECHAINC,
        CA.CODIGO_AUTORIZACION AS COD_AUT,
        CA.RUT AS CORE_RUT,
        CA.ORIGEN_TRANSAC AS OTRANSAC
        FROM TBL_CORE_AUTOR CA
        WHERE CA.FECHA_INCOMING = TO_CHAR(SYSDATE, 'DD-MON-YY', 'NLS_DATE_LANGUAGE = english')
        AND CA.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE UPPER(EMPRESA) = 'KHIPU');


      BEGIN
      cod_error := 0;
      warning := 'Proceso SP_SIG_CONC_CORE_KHIPU ejecutado';

       --/=====================CURSOR VISA=========================================/
        OPEN cursorPagos;
        LOOP
          fetch cursorPagos into 
              var_sid_KHIPU,
              monto_transac,
              fecha_hora_transac, 
              fecha_inc,
              transaccion_id,
              rut_cliente,
              var_operador;
          EXIT WHEN cursorPagos%NOTFOUND;
          dbms_output.Put_line('loop cursor Visa');
          sidCoreKHIPU := 0;
          -- 1 query para identificar las que no califican por hora
          -- 2 insertarlas con estado de no conciliadas por limite de hora
          -- 3 seguir proceso normal
          dbms_output.Put_line('var_sid_KHIPU: '|| var_sid_KHIPU ||
          ' monto_transac:  ' ||monto_transac||
          ' fecha_hora_transac: ' ||fecha_hora_transac|| 
          ' fecha_inc:' ||fecha_inc|| 
          ' transaccion_id: '||transaccion_id|| 
          ' rut_cliente: '||rut_cliente|| 
          ' var_operador: ' ||var_operador);
         begin
           select  AC.SID, AC.RUT, AC.MONTO_TRANSAC, AC.FECHA_HR_TRASAC 
           into sidCoreKHIPU, RUT_CORE, monto_core, fecha_core
           from TBL_CORE_AUTOR AC where 
           AC.MONTO_TRANSAC = (monto_transac) 
           AND AC.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE UPPER(EMPRESA) = 'KHIPU') 
           AND AC.RUT = (rut_cliente)
           AND AC.FECHA_HR_TRASAC = (fecha_hora_transac)
           AND AC.SID =( SELECT MIN(SID) FROM TBL_CORE_AUTOR AC where 
           AC.MONTO_TRANSAC = (monto_transac) 
           AND AC.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE UPPER(EMPRESA) = 'KHIPU') 
           AND AC.RUT = (rut_cliente)
           AND AC.FECHA_HR_TRASAC = (fecha_hora_transac)
          )
           group by AC.SID, AC.RUT, AC.MONTO_TRANSAC, AC.FECHA_HR_TRASAC, AC.FECHA_HR_TRASAC;
         exception
          WHEN NO_DATA_FOUND THEN
          sidCoreKHIPU := 0;
         end;
            ---dbms_output.Put_line('sidBiceTrebol: '||sidBiceTrebol || ' var_sid_trebol: '||var_sid_trebol || ' rut_clie: '||rut_clie || ' monto_bic: '||monto_bic || ' sid_opr_cl: '||sid_opr_cl);
           if(sidCoreKHIPU > 0)then
              estadoKHIPU := 1;
             elsif(sidCoreKHIPU = 0)then
              estadoKHIPU := 2;

           end if;

          begin

          dbms_output.Put_line(sidCoreKHIPU || ' - ' || var_sid_KHIPU);
           select  sid into var_conc_KHIPU_core from TBL_CONC_CORE_KHIPU where (SID_CORE = sidCoreKHIPU and sidCoreKHIPU > 0 ) or (SID_KHIPU = var_sid_KHIPU);
           --dbms_output.put_line('var_conc_KHIPU_core: '||var_conc_KHIPU_core);
           exception
          WHEN NO_DATA_FOUND THEN
          --dbms_output.put_line('sid_bice_trebol: '||sidBiceTrebol||'--'||'var_sid_trebol: '||var_sid_trebol);
          if(sidCoreKHIPU>0 or var_sid_KHIPU>0)then
            delete from TBL_CONC_CORE_KHIPU where ((SID_CORE = sidCoreKHIPU and sidCoreKHIPU>0) or (SID_KHIPU = var_sid_KHIPU)) and ESTADO_CONTABLE not like '1';
            dbms_output.put_line(SQL%ROWCOUNT);
          end if;
          var_conc_KHIPU_core := 0;
         end;
           dbms_output.Put_line('var_conc_KHIPU_core: '||var_conc_KHIPU_core);
            dbms_output.Put_line('fecha_inc: '||fecha_inc);
            update TBL_CONC_CORE_KHIPU set  SID_CORE = sidCoreKHIPU, SID_KHIPU=var_sid_KHIPU, SID_ESTADO=estadoKHIPU, FECHA=fecha_inc, FECHA_REGISTRO= sysdate
            where sid = var_conc_KHIPU_core;
            if(SQL%ROWCOUNT = 0)THEN
              select count(*) into contador from TBL_CONC_CORE_KHIPU VC where SID_CORE=sidCoreKHIPU and VC.SID_KHIPU=var_sid_KHIPU;
              --dbms_output.Put_line('Encuentra duplicados: '||contador);
              if(contador = 0)THEN

                -- dbms_output.Put_line('Datos a insertar: '||sidBiceTrebol||'-'||var_sid_trebol||'-'||estadoTrebol||'-'||fecha_reg_trebol||'-'||num_ref_trans||'-'|| monto_trebol ||'-'|| rut_cliente ||'-'|| SYSDATE);
                dbms_output.put_line(fecha_inc||'-'||fecha_inc);
                --insert into TBL_CONC_CORE_KHIPU(SID, SID_KHIPU, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, ESTADO_RECHAZO, OPERADOR )
                --values(SEQ_TBL_CONC_CORE_KHIPU.nextval,var_sid_KHIPU,sidCoreKHIPU,estadoKHIPU,TO_DATE(fecha_inc, 'DD-MON-YY'),monto_transac, TO_DATE(SYSDATE, 'DD-MON-YY'), 0, var_operador); 

                insert into TBL_CONC_CORE_KHIPU(SID, SID_KHIPU, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, OPERADOR )
                values(SEQ_TBL_CONC_CORE_KHIPU.nextval,var_sid_KHIPU,sidCoreKHIPU,estadoKHIPU,fecha_inc,monto_transac, SYSDATE, var_operador); 

            
              end if;
            end if;
          --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorPagos;
      dbms_output.Put_line('#################################################### FIN CURSOR PAGOS RECIBIDOS');


    --/=====================CURSOR CORE=========================================/
     OPEN cursorCore;
        LOOP
         FETCH cursorCore INTO       
             var_sid_core,
             monto_transac_core, 
             fecha_transac_core, 
             fecha_inc_core, 
             codigo_autor_core,
             rut,
             var_origen_transac;
             
             
          
             
         EXIT WHEN cursorCore%NOTFOUND;
         dbms_output.Put_line('loop cursor Core');
         sidKhipuCore := 0;
         var_monto_core:= 0;
         dbms_output.Put_line('var_sid_core :'||var_sid_core ||'-'||
         'monto_transac_core :'||monto_transac_core ||'-'||
         'fecha_transac_core :'||fecha_transac_core||'-'||
         'fecha_inc_core :'||fecha_inc_core||'-'||
         'codigo_autor_core :'||fecha_inc_core||'-'||
         'rut :'||rut||'-'||
         'var_origen_transac :'||var_origen_transac);
       begin
       
       rr:= rut;
       ftc:= fecha_transac_core;
       montf := monto_transac_core;
       
       
      /* warning:= 'SELECT I.sid, I.TRANSACCION_ID, I.MONTO_FINAL, I.FECHA_PAGO, I.OPERADOR
        INTO sidKhipuCore, var_cod_auth_core, var_monto_core, var_fecha_transac, operador_khipu
        FROM TBL_PAGOS_RECIBIDOS I
        WHERE 
        I.MONTO_FINAL = ('||montf||')
        --AND I.CODIGO_AUTORIZACION = codigo_autor_core
        AND I.FECHA_PAGO =  ('||ftc||')
        AND I.RUT = ('||rr||')
        AND I.OPERADOR = 6
        --AND SUBSTR(I.NRO_TARJETA, -4) = tarjeta_core
        GROUP BY I.sid, I.TRANSACCION_ID, I.MONTO_FINAL, I.FECHA_PAGO, I.OPERADOR;';*/
       
       
       
        SELECT I.sid, I.MONTO_FINAL, I.FECHA_PAGO, I.OPERADOR
        INTO sidKhipuCore, var_monto_core, var_fecha_transac, operador_khipu
        FROM TBL_PAGOS_RECIBIDOS I
        WHERE 
        I.MONTO_FINAL = (montf)
        AND I.FECHA_PAGO =  (ftc)
        AND I.RUT = (rr)
        AND I.OPERADOR = 6
        AND I.SID =( SELECT MIN(SID) FROM TBL_PAGOS_RECIBIDOS I
        WHERE 
        I.MONTO_FINAL = (montf)
        AND I.FECHA_PAGO =  (ftc)
        AND I.RUT = (rr)
        AND I.OPERADOR = 6) 
        GROUP BY I.sid, I.MONTO_FINAL, I.FECHA_PAGO, I.OPERADOR;

        
       exception
        WHEN NO_DATA_FOUND THEN
        sidKhipuCore := 0;

      
       end;
         --dbms_output.Put_line('var_monto_bice: '||var_monto_bice ||' sidTrebolBice: '|| sidTrebolBice || ' id_oper_cl: '||id_oper_cl|| ' rut_beneficiario: '||var_rut_benef);      


       if(sidKhipuCore > 0)then
          estadoCore := 1;

          else
              estadoCore := 3;
              operador_khipu := 0;
       end if;
        --dbms_output.Put_line('estadoBice: '||estadoBice); 
        --dbms_output.Put_line('var_sid_bice: '||var_sid_bice ||'sidTrebolBice: '||sidTrebolBice);
       begin
       
       
       --dbms_output.Put_line('SELECT I.sid, I.TRANSACCION_ID, I.MONTO_FINAL, I.FECHA_PAGO, I.FECHA_PAGO, I.OPERADOR        INTO sidKhipuCore, var_cod_auth_core, var_monto_core, var_fecha_transac, var_fecha_inc, operador_khipu        FROM TBL_PAGOS_RECIBIDOS I        WHERE         I.MONTO_FINAL = TO_NUMBER('||monto_transac_core||')       AND I.FECHA_PAGO =  '||fecha_transac_core||'        AND I.RUT = '||rut||'        AND I.OPERADOR = 6        --AND SUBSTR(I.NRO_TARJETA, -4) = tarjeta_core        GROUP BY I.sid, I.TRANSACCION_ID, I.MONTO_FINAL, I.FECHA_PAGO, I.FECHA_PAGO, I.OPERADOR;');
       

       dbms_output.Put_line(sidKhipuCore || ' - ' || var_sid_core );
         select  sid into var_conc_khipu_inc from TBL_CONC_CORE_KHIPU 
         where (SID_KHIPU = sidKhipuCore and sidKhipuCore >0) or (SID_CORE = var_sid_core);
         exception
        WHEN NO_DATA_FOUND THEN
        var_conc_khipu_inc := 0;
       end;

         --dbms_output.Put_line('---->>>var_conc_khipu_inc: '||var_conc_khipu_inc);        
        update TBL_CONC_CORE_KHIPU set sid_core = var_sid_core, SID_KHIPU = sidKhipuCore, sid_estado = estadoCore
          where sid = var_conc_khipu_inc;
          if(SQL%ROWCOUNT = 0)THEN
           --dbms_output.Put_line('---->>>FECHA: '||fecha_inc_core);  
           insert into TBL_CONC_CORE_KHIPU(SID, SID_KHIPU, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO,  OPERADOR )
           --values(SEQ_TBL_CONC_CORE_KHIPU.nextval,sidCoreVisa,var_sid_visa,estadoVisa,fecha_inc,monto_transac, SYSDATE, 0); 
           values(SEQ_TBL_CONC_CORE_KHIPU.nextval,sidKhipuCore,var_sid_core,estadoCore,fecha_inc_core, monto_transac_core, SYSDATE,  operador_khipu);  

          -- insert into TBL_CONC_CORE_KHIPU(SID, SID_KHIPU, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, ESTADO_RECHAZO, OPERADOR )
           --values(SEQ_TBL_CONC_CORE_KHIPU.nextval,sidCoreVisa,var_sid_visa,estadoVisa,fecha_inc,monto_transac, SYSDATE, 0); 
           --values(SEQ_TBL_CONC_CORE_KHIPU.nextval,sidCoreKhipu,var_sid_core,estadoCore,fecha_inc_core, monto_transac_core, SYSDATE, 0, operador_khipu);  
          end if;
        --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorCore;
      dbms_output.Put_line('Fin cursor Core');
      dbms_output.Put_line('##############################################################################  FIN CURSOR CORE ');

-- /==============================================================/
--dbms_output.Put_line('Final del proceso');
--Sin este commit, el proceso no deja las conciliaciones registradas, no se cae, no da error, simplemente no las deja registradas
  COMMIT;
  
  
  
  END;

    EXCEPTION     
         WHEN NO_DATA_FOUND THEN
         dbms_output.Put_line( 'NO HAY REGISTROS EN TABLA' ); 
         warning := warning || '- NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS';
        WHEN OTHERS THEN                                                                      
                 warning := SQLERRM || ' - ' || DBMS_UTILITY.format_error_backtrace;
                 cod_error := SQLCODE;
                 dbms_output.Put_line( 'ERROR : ' || warning ||' SQLCODE : ' || cod_error );
                 dbms_output.Put_line( 'DESCRIPTION: ' || DBMS_UTILITY.format_error_backtrace);

                INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CONC_CORE_KHIPU', cod_error || '-' || warning);
                COMMIT;

END SP_SIG_CONC_CORE_KHIPU;