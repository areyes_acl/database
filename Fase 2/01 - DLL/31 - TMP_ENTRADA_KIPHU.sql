CREATE TABLE TMP_ENTRADA_KHIPU
(
        SID NUMBER NOT NULL ,
	    "FECHA_PAGO" VARCHAR2(20 BYTE),
		"HORA_PAGO" VARCHAR2(20 BYTE),
        "RUT" VARCHAR2(50 BYTE),		
		"TRANSACCION_ID" VARCHAR2(50 BYTE),
        "DESCRIPCION" VARCHAR2(50 BYTE),
		"LOCAL_PAGO" VARCHAR2(50 BYTE),
		"CLIENTE_NOMBRE" VARCHAR2(50 BYTE),		
		"CLIENTE_EMAIL" VARCHAR2(50 BYTE),
		"CLIENTE_NOMBRE_BANCO" VARCHAR2(50 BYTE),
		"CLIENTE_NUMERO_CUENTA" VARCHAR2(50 BYTE),
		"ESTRUCTURA_PAGO" VARCHAR2(50 BYTE),
		"MONTO_PAGO" NUMBER(12,2),		
		"MONTO_FINAL" NUMBER(12,2),
        "MONTO_MINIMO" NUMBER(12,2),
        "DESCUENTO" NUMBER(12,2),
		"COMISION" NUMBER(12,2),
		"COMISION_INTEGRADA" NUMBER(12,2),
		"CUSTOM" NUMBER(12,2),
        "OPERADOR" NUMBER(2),
		"FECHA_CONTABLE" VARCHAR2(20 BYTE)
, CONSTRAINT TMP_ENTRADA_KIPHU_PK PRIMARY KEY 
  (
    SID 
  )
  ENABLE 
);

COMMENT ON COLUMN "SIG"."TMP_ENTRADA_KHIPU"."SID" IS 'identificador único';
COMMENT ON COLUMN "SIG"."TMP_ENTRADA_KHIPU"."FECHA_PAGO" IS 'Fecha en que se realizo el Pago';
COMMENT ON COLUMN "SIG"."TMP_ENTRADA_KHIPU"."HORA_PAGO" IS 'Hora en que se realizo el pago';
COMMENT ON COLUMN "SIG"."TMP_ENTRADA_KHIPU"."RUT" IS 'Rut Pagador';
COMMENT ON COLUMN TMP_ENTRADA_KHIPU.OPERADOR IS 'Operador';

COMMIT;
