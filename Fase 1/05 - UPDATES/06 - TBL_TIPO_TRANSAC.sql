update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Cliente no reconoce transacción' where sid = 923;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Clonación' where sid = 924;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Duplicidad' where sid = 925;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Monto transacción no corresponde' where sid = 926;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Pérdida documentos' where sid = 927;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Recarga telefónica no efectuada' where sid = 928;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Robo tarjeta' where sid = 929;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Servicio no entregado por comercio' where sid = 930;
update tbl_tipo_transac set descripcion = 'Compra en comercio nacional no reconocido - Servicio no entregado por comercio' where sid = 931;
commit;