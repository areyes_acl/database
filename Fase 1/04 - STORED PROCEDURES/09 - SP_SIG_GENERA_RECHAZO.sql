create or replace PROCEDURE "SP_SIG_GENERA_RECHAZO"  ( warning   OUT VARCHAR2,
                                                   cod_error OUT NUMBER )
IS 
/******************************************************************************
   NAME:       SP_SIG_GENERA_RECHAZO
   PURPOSE:

   REVISIONS:
   Ver        Date      Author Description
   ---------  --------- ------ ---------------------------------------------
   1.0     29-10-2020 acl    SP que genera los rechazos
   NOTES:
      Object Name:     SP_SIG_GENERA_RECHAZO
******************************************************************************/
BEGIN

    DECLARE   
       -- Datos de la tabla de conciliacion
      sid_conc NUMBER;
      sid_core NUMBER;
      sid_visa NUMBER; 
      fecha_registro VARCHAR2(50);
      sid_estado number;   
      
      cod_id_adq NUMBER;
      datos_ref_adq NUMBER;
      mit VARCHAR2(50 BYTE);
      monto_transac VARCHAR2(50 BYTE);
      fecha_hr_transac VARCHAR2(50 BYTE);
      cod_auth VARCHAR2(50 BYTE);
      nro_tarjeta NUMBER;
      cod_moneda_transac VARCHAR2(50 BYTE);
      cod_procesamiento NUMBER;
      deb_cred VARCHAR(20 BYTE);
      currency_code VARCHAR2(50 BYTE);
      operador number;
      
      /* Cursor VISA */
      CURSOR cursorRechazo IS
       Select SID, 
       SID_CORE, 
       SID_VISA, 
       FECHA_REGISTRO, 
       SID_ESTADO 
       FROM TBL_CONC_VISA_CORE WHERE --(ROUND((SELECT SYSDATE FROM DUAL) - FECHA_REGISTRO) > 5) AND 
       SID_ESTADO > 1;
        
            
      BEGIN
      cod_error := 0;
      warning := 'Proceso SP_SIG_GENERA_RECHAZO ejecutado';

       /*=====================CURSOR VISA=========================================*/
        OPEN cursorRechazo;
        LOOP
          fetch cursorRechazo into 
            sid_conc, 
            sid_core, 
            sid_visa, 
            fecha_registro,
            sid_estado;
          EXIT WHEN cursorRechazo%NOTFOUND;
          dbms_output.Put_line('loop cursor Rechazos');
          --dbms_output.Put_line('sid_conc: '|| sid_conc ||' sid_core:  ' ||sid_core||' sid_visa: ' ||sid_visa|| ' fecha_registro:' ||fecha_registro|| ' sid_estado: '||sid_estado);
         begin
         --rechazos de visa
         
         if (sid_core = 0 AND sid_visa <> 0) THEN
         
         
         cod_moneda_transac:= 'CLP';
         dbms_output.Put_line('rechazos de visa');
         dbms_output.Put_line(sid_visa);
         -- si no esta en core se rechaza
         
         -- Obtener data desde TBL_INCOMING
         
          select cod_identificacion_adquirente, DATOS_REFERENCIA_ADQUIRENTE, MIT,
          MONTO_TRANSAC, FECHA_HR_TRASAC, CODIGO_AUTORIZACION, NRO_TARJETA, COD_MONEDA_TRANSAC,
          CODIGO_PROCESAMIENTO, OPERADOR
          into 
          cod_id_adq, datos_ref_adq, mit, monto_transac, fecha_hr_transac, cod_auth, nro_tarjeta, cod_moneda_transac, cod_procesamiento, operador
          from tbl_incoming WHERE SID = sid_visa;
          
          
          -- Insertar en tabla temporal de rechazos
          IF(mit = '05') THEN
          deb_cred := 'D';
          ELSIF(mit = '06') THEN
          deb_cred := 'C';
          END IF;
          
          
          IF(cod_moneda_transac != '152') THEN
            cod_moneda_transac := 'USD';
          END IF;
          
          --dbms_output.Put_line(mit);
          
          
          INSERT INTO TMP_RECHAZOS VALUES(
            SEQ_TMP_INCOMING.nextval,
            cod_id_adq,
            cod_procesamiento,
            'Visa',
            datos_ref_adq,
            cod_procesamiento,
            cod_auth,
            TO_CHAR(TO_DATE(fecha_hr_transac, 'DD-MON-YY'), 'YYMMDD'),
            nro_tarjeta,
            deb_cred,
            currency_code,
            monto_transac,
            'Transacción Visa que no existe en autorizaciones',
            operador
          );
         
         
         end if;
         
          end;
        END LOOP;
      CLOSE cursorRechazo;
      dbms_output.Put_line('#################################################### FIN CURSOR RECHAZO');

 /*==============================================================*/
--dbms_output.Put_line('Final del proceso');
--Sin este commit, el proceso no deja las en la tabla de rechazos
  COMMIT;
  END;
     
    EXCEPTION     
         WHEN NO_DATA_FOUND THEN
         dbms_output.Put_line( 'NO HAY REGISTROS EN TABLA' ); 
         warning := warning || '- NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS';
        WHEN OTHERS THEN                                                                      
                 warning := SQLERRM || ' - ' || DBMS_UTILITY.format_error_backtrace;
                 cod_error := SQLCODE;
                 dbms_output.Put_line( 'ERROR : ' || warning ||' SQLCODE : ' || cod_error );
                 dbms_output.Put_line( 'DESCRIPTION: ' || DBMS_UTILITY.format_error_backtrace);
                
                INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_GENERA_RECHAZO', cod_error || '-' || warning);
                COMMIT;

END SP_SIG_GENERA_RECHAZO;