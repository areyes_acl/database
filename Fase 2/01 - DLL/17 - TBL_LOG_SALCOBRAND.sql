  CREATE TABLE "SIG"."TBL_LOG_SALCOBRAND" 
   (	"SID" NUMBER NOT NULL ENABLE, 
	"FILE_TS" TIMESTAMP (6), 
	"FILE_NAME" VARCHAR2(50 BYTE), 
	"FILE_FLAG" VARCHAR2(2 BYTE), 
	"FECHA" DATE, 
	 CONSTRAINT "TBL_LOG_SALCOBRAND_PK" PRIMARY KEY ("SID") ENABLE
   );

   COMMENT ON COLUMN "SIG"."TBL_LOG_SALCOBRAND"."SID" IS 'Corresponde al id de la tabla TBL_LOG_SALCOBRAND';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SALCOBRAND"."FILE_TS" IS 'Corresponde a la estampa de tiempo del proceso del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SALCOBRAND"."FILE_NAME" IS 'Corresponde al nombre del archivo';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SALCOBRAND"."FILE_FLAG" IS 'Corresponde al flag de procesamiento creado 0:subido 1:-1:error';
   COMMENT ON COLUMN "SIG"."TBL_LOG_SALCOBRAND"."FECHA" IS 'Corresponde a la fecha de Procesamiento del archivo';
COMMIT;