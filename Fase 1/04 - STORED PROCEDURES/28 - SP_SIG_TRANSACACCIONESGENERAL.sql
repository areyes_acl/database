create or replace PROCEDURE       "SP_SIG_TRANSACACCIONESGENERAL" (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 10/07/2013
-- Versión : 1.0
-- Descripcion: Consulta las transacciones segun la opcion de accion seleccionada.
                                                            numeroTarjeta       IN VARCHAR2,
                                                            fechaInicio         IN VARCHAR2,
                                                            fechaTermino        IN VARCHAR2,
                                                            codigoTransaccion   IN VARCHAR2,
                                                            codigoFuncion       IN VARCHAR2,
                                                            tipoBusqueda        IN VARCHAR2,
                                                            warning             OUT VARCHAR2,
                                                            cod_error           OUT NUMBER,
                                                            prfCursor           OUT Sys_RefCursor
                                                            )
IS
    voCursor Sys_RefCursor:=null;

BEGIN

            BEGIN
                DBMS_OUTPUT.PUT_LINE(numeroTarjeta || ' ==' || fechaInicio || ' == ' || fechaTermino || ' == ' || codigoTransaccion || ' == ' || codigoFuncion);
                IF (numeroTarjeta<>'0' AND tipoBusqueda = 'T') THEN
                BEGIN
                        OPEN voCursor FOR
                           select * from  (SELECT ROWNUM  AS registro,t.* FROM (    
                                SELECT DISTINCT
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                               Substr(T.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(T.NRO_TARJETA, 13, 16) AS NRO_TARJETA,  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                  REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)  AS PAIS,  
                                TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                               --T.CODIGO_AUTORIZACION,
                                --TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                             -- NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                             -- NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                              --NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                               -- T.SID                                                               AS SID,
                                T.GLOSA_GENERAL                                                     AS DESCRIPCION,
                                T.NUM_INCIDENTE														                          AS NUM_INCIDENTE,								
                                --NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                AS USUARIO,
                                TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                     AS FECHA_GESTION,
                                 NVL(P.P0023, '-')                                                AS  PATPASS,
                                 I.OPERADOR                                                       AS  IOPERADOR,
                                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS  OPERADOR
                            FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) 
                                 LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID
                                  LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION),
                                 SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E, TBL_INCOMING I
                            WHERE   
                                T.NRO_TARJETA                           =  numeroTarjeta        AND
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ACCION_TRANSAC                         = A.SID                AND
                                A.COD_TRANSAC                            = codigoTransaccion    AND
                                A.COD_FUNCION                            = codigoFuncion        AND
                                A.ACTIVO                                 = 1                    AND
                                GT.TRANSACCION                           = I.SID                AND
                                T.ESTADO_TRANSAC                         = E.SID                AND
                                GT.ESTADO_PROCESO                        IN (4,5)               AND
                                E.SID                                    = 1 
                          group by 
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                                T.NRO_TARJETA,
                                T.FECHA_HR_TRASAC,
                                T.NOMBRE_UBIC_ACEP_TARJETA,
                                T.MONTO_TRANSAC, 
                                T.MONTO_FACTURACION,
                                T.COD_MOTIVO_MENSAJE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.CODIGO_AUTORIZACION,
                                T.FECHA_HR_TRASAC,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                E.DESCRIPCION,
                                T.CODIGO_PROCESAMIENTO,
                                T.TASA_CONVENIO_CONCILIACION,
                                T.TASA_CONVENIO_FACTURACION,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                T.COD_MONEDA_TRANSAC,
                                T.MCC,
                                T.SID,
                                T.GLOSA_GENERAL,
                                T.NUM_INCIDENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.MONTO_CONCILIACION, 
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,'')),
                                P.P0023,
                                I.OPERADOR
                              ORDER BY    FECHATRANSAC) t );

                    END;
                ELSE
                  BEGIN
                     IF (numeroTarjeta<>'0' AND tipoBusqueda = 'G') THEN
                      BEGIN
                        OPEN voCursor FOR
                        select * from  (SELECT ROWNUM  AS registro,t.* FROM (    
                              SELECT DISTINCT
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                               Substr(T.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(T.NRO_TARJETA, 13, 16) AS NRO_TARJETA,  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                  REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)  AS PAIS,  
                                TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                               --T.CODIGO_AUTORIZACION,
                                --TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                             -- NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                             -- NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                              --NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                               -- T.SID                                                               AS SID,
                                T.GLOSA_GENERAL                                                     AS DESCRIPCION,
                                T.NUM_INCIDENTE														                          AS NUM_INCIDENTE,								
                                --NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                AS USUARIO,
                                TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                     AS FECHA_GESTION,
                                 NVL(P.P0023, '-')                                                AS  PATPASS,
                                 I.OPERADOR                                                       AS  IOPERADOR,
                                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS  OPERADOR
                            FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) 
                                 LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID
                                  LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION),
                                 SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E, TBL_INCOMING I
                            WHERE   
                                T.NRO_TARJETA                           =  numeroTarjeta        AND
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ACCION_TRANSAC                         = A.SID                AND
                                A.COD_TRANSAC                            = codigoTransaccion    AND
                                A.COD_FUNCION                            = codigoFuncion        AND
                                A.ACTIVO                                 = 1                    AND
                                GT.TRANSACCION                           = I.SID                AND
                                T.ESTADO_TRANSAC                         = E.SID                AND
                                GT.ESTADO_PROCESO                        IN (4,5)               AND
                                E.SID                                    = 1 
                          group by 
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                                T.NRO_TARJETA,
                                T.FECHA_HR_TRASAC,
                                T.NOMBRE_UBIC_ACEP_TARJETA,
                                T.MONTO_TRANSAC, 
                                T.MONTO_FACTURACION,
                                T.COD_MOTIVO_MENSAJE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.CODIGO_AUTORIZACION,
                                T.FECHA_HR_TRASAC,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                E.DESCRIPCION,
                                T.CODIGO_PROCESAMIENTO,
                                T.TASA_CONVENIO_CONCILIACION,
                                T.TASA_CONVENIO_FACTURACION,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                T.COD_MONEDA_TRANSAC,
                                T.MCC,
                                T.SID,
                                T.GLOSA_GENERAL,
                                T.NUM_INCIDENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.MONTO_CONCILIACION, 
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,'')),
                                P.P0023,
                                I.OPERADOR
                              ORDER BY    FECHA_GESTION) t );

                    END;
                END IF;
              END;
           END IF;


          IF (numeroTarjeta ='0' AND tipoBusqueda = 'T') THEN
                    BEGIN
                        OPEN voCursor FOR
                             select * from  (SELECT ROWNUM  AS registro,t.* FROM (    
                                SELECT DISTINCT
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                               Substr(T.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(T.NRO_TARJETA, 13, 16) AS NRO_TARJETA,  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                  REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)  AS PAIS,  
                                TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                               --T.CODIGO_AUTORIZACION,
                                --TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                             -- NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                             -- NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                              --NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                               -- T.SID                                                               AS SID,
                                T.GLOSA_GENERAL                                                     AS DESCRIPCION,
                                T.NUM_INCIDENTE														                          AS NUM_INCIDENTE,								
                                --NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                                       AS USUARIO,
                                TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                     AS FECHA_GESTION,
                                 NVL(P.P0023, '-')                                                AS  PATPASS,
                                 I.OPERADOR                                                       AS  IOPERADOR,
                                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS  OPERADOR
                            FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) 
                                 LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID
                                   LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION),
                                 SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E, TBL_INCOMING I
                            WHERE   
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ACCION_TRANSAC                         = A.SID                AND
                                A.COD_TRANSAC                            = codigoTransaccion    AND
                                A.COD_FUNCION                            = codigoFuncion        AND
                                A.ACTIVO                                 = 1                    AND
                                GT.TRANSACCION                           = I.SID                AND
                                T.ESTADO_TRANSAC                         = E.SID                AND
                                GT.ESTADO_PROCESO                        IN (4,5)               AND
                                E.SID                                    = 1 
                          group by 
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                                T.NRO_TARJETA,
                                T.FECHA_HR_TRASAC,
                                T.NOMBRE_UBIC_ACEP_TARJETA,
                                T.MONTO_TRANSAC, 
                                T.MONTO_FACTURACION,
                                T.COD_MOTIVO_MENSAJE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.CODIGO_AUTORIZACION,
                                T.FECHA_HR_TRASAC,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                E.DESCRIPCION,
                                T.CODIGO_PROCESAMIENTO,
                                T.TASA_CONVENIO_CONCILIACION,
                                T.TASA_CONVENIO_FACTURACION,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                T.COD_MONEDA_TRANSAC,
                                T.MCC,
                                T.SID,
                                T.GLOSA_GENERAL,
                                T.NUM_INCIDENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.MONTO_CONCILIACION, 
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,'')),
                                P.P0023,
                                I.OPERADOR
                              ORDER BY    FECHATRANSAC) t );

                    END;
                ELSE
                 BEGIN
                    IF (numeroTarjeta='0' AND tipoBusqueda = 'G' ) THEN
                      BEGIN
                         OPEN voCursor FOR
                            select * from  (SELECT ROWNUM  AS registro,t.* FROM (    
                                SELECT DISTINCT
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                               Substr(T.NRO_TARJETA, 1, 4)||'XXXXXXXX'||Substr(T.NRO_TARJETA, 13, 16) AS NRO_TARJETA,  
                                TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHATRANSAC,
                                  REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,1) AS COMERCIO,
                                REGEXP_SUBSTR(T.NOMBRE_UBIC_ACEP_TARJETA,'[^\]+',1,3)  AS PAIS,  
                                TRIM((CASE WHEN T.MONTO_TRANSAC is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_TRANSAC,1,LENGTH(T.MONTO_TRANSAC)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_TRANSAC,LENGTH(T.MONTO_TRANSAC)-1)) END))     AS MONTO_TRANSAC,
                                TRIM((CASE WHEN T.MONTO_FACTURACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_FACTURACION,1,LENGTH(T.MONTO_FACTURACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_FACTURACION,LENGTH(T.MONTO_FACTURACION)-1)) END))      AS MONTO_FACTURACION,
                                NVL(T.COD_MOTIVO_MENSAJE, ' ')                                      AS CODRAZON,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 12, 11),' ')              AS MICROFILM,
                               --T.CODIGO_AUTORIZACION,
                                --TO_CHAR(T.FECHA_HR_TRASAC, 'DD/MM/YYYY hh24:mi:ss')                 AS FECHAEFECTIVA, 
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 8, 4), ' ')               AS FECHAPROCESO,
                                --NVL(SUBSTR(T.DATOS_REFERENCIA_ADQUIRENTE, 2, 6), ' ')               AS BINADQUIRENTE,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS LEEBANDA,
                                UPPER(E.DESCRIPCION)                                                AS ESTADOTRX,
                             -- NVL(T.CODIGO_PROCESAMIENTO, ' ')                                    AS OTRODATO1,
                             -- NVL(T.TASA_CONVENIO_CONCILIACION, ' ')                              AS OTRODATO2,
                              --NVL(T.TASA_CONVENIO_FACTURACION, ' ')                               AS OTRODATO3,
                                --NVL(T.COD_DATOS_PUNTO_SERVICIO, ' ')                                AS OTRODATO4,
                                --NVL(T.COD_MONEDA_TRANSAC, ' ')                                      AS CODMONEDATRX,
                                --NVL(T.MCC, ' ')                                                     AS RUBROCOMERCIO,
                               -- T.SID                                                               AS SID,
                                T.GLOSA_GENERAL                                                     AS DESCRIPCION,
                                T.NUM_INCIDENTE														                          AS NUM_INCIDENTE,								
                                --NVL(T.DATOS_REFERENCIA_ADQUIRENTE, ' ')                             AS REFERENCIA,
                                TRIM((CASE WHEN T.MONTO_CONCILIACION is null THEN '' ELSE CONCAT(CONCAT(TO_CHAR(SUBSTR(T.MONTO_CONCILIACION,1,LENGTH(T.MONTO_CONCILIACION)-2),'999G999G999G999G999G999G999G999'),','),SUBSTR(T.MONTO_CONCILIACION,LENGTH(T.MONTO_CONCILIACION)-1)) END))     AS MONTO_CONCILIACION,
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,''))                                                       AS USUARIO,
                                TO_CHAR(MAX(GT.FECHA_ACTUALIZACION),'DD/MM/YYYY')                     AS FECHA_GESTION,
                                 NVL(P.P0023, '-')                                                AS  PATPASS,
                                 I.OPERADOR                                                       AS  IOPERADOR,
                                 TRIM((CASE WHEN I.OPERADOR = 2 THEN 'VN' ELSE 'VI' END))         AS  OPERADOR
                            FROM (SIG.TBL_TRANSACCIONES T LEFT JOIN TBL_GESTION_TRANSAC GT ON T.SID = GT.TRANSACCION) 
                                 LEFT JOIN TBL_USUARIO_IG US  ON GT.USUARIO = US.SID
                                   LEFT OUTER  JOIN SIG.TBL_PDS P ON (T.SID      = P.TRANSACCION),
                                 SIG.TBL_ACCION_TRANSAC A, SIG.TBL_ESTADO_TRANSAC E, TBL_INCOMING I
                            WHERE   
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  >= fechaInicio          AND  
                                TO_CHAR(GT.FECHA_ACTUALIZACION, 'YYYYMMDD')  <= fechaTermino         AND
                                T.ACCION_TRANSAC                         = A.SID                AND
                                A.COD_TRANSAC                            = codigoTransaccion    AND
                                A.COD_FUNCION                            = codigoFuncion        AND
                                A.ACTIVO                                 = 1                    AND
                                GT.TRANSACCION                           = I.SID                AND
                                T.ESTADO_TRANSAC                         = E.SID                AND
                                GT.ESTADO_PROCESO                        IN (4,5)               AND
                                E.SID                                    = 1 
                          group by 
                                T.MIT, 
                                T.CODIGO_FUNCION, 
                                T.NRO_TARJETA,
                                T.FECHA_HR_TRASAC,
                                T.NOMBRE_UBIC_ACEP_TARJETA,
                                T.MONTO_TRANSAC, 
                                T.MONTO_FACTURACION,
                                T.COD_MOTIVO_MENSAJE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.CODIGO_AUTORIZACION,
                                T.FECHA_HR_TRASAC,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                E.DESCRIPCION,
                                T.CODIGO_PROCESAMIENTO,
                                T.TASA_CONVENIO_CONCILIACION,
                                T.TASA_CONVENIO_FACTURACION,
                                T.COD_DATOS_PUNTO_SERVICIO,
                                T.COD_MONEDA_TRANSAC,
                                T.MCC,
                                T.SID,
                                T.GLOSA_GENERAL,
                                T.NUM_INCIDENTE,
                                T.DATOS_REFERENCIA_ADQUIRENTE,
                                T.MONTO_CONCILIACION, 
                                CONCAT(NVL(US.RUT,''),NVL(US.DV,'')),
                                P.P0023,
                                I.OPERADOR
                              ORDER BY    FECHA_GESTION) t );

                    END;
                  END IF;
                 END;
              END IF;

                prfCursor:=voCursor;

            EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_TRANSACACCIONESGENERAL', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
            END;

END SP_SIG_TRANSACACCIONESGENERAL;