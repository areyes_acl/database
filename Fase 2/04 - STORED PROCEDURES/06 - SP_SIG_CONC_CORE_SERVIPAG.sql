create or replace PROCEDURE "SP_SIG_CONC_CORE_SERVIPAG"  ( warning OUT VARCHAR2, cod_error OUT NUMBER )
IS 
/******************************************************************************
   NAME:       SP_SIG_CONC_CORE_SERVIPAG
   PURPOSE:

   REVISIONS:
   Ver        Date      Author Description
   ---------  --------- ------ ---------------------------------------------
   1.0     13-10-2020 acl    SP de conciliacion de interfaz SERVIPAG y autorizaciones del core SBPAY
   NOTES:
      Object Name:     SP_SIG_CONC_CORE_SERVIPAG
******************************************************************************/
BEGIN

    DECLARE   

       -- DATOS SERVIPAG
       -- Datos necesarios de la TBL_PAGOS_RECIBIDOS
      var_sid_SERVIPAG NUMBER;
      monto_transac_pagos NUMBER;
      fecha_hora_transac VARCHAR2(50);   
      fecha_inc VARCHAR2(50);
      rut_cliente VARCHAR2(50);
      var_operador number;

      -- DATOS CORE
      -- Datos necesarios de la tbl_core_autor
      var_sid_core NUMBER;
      monto_transac_core NUMBER;
      fecha_transac_core VARCHAR2(50);
      fecha_inc_core VARCHAR2(50);
      codigo_autor_core VARCHAR2(50); 
      rut_core_autor VARCHAR2(50);
      var_origen_transac number;  

      --variables conciliacion

      sidServipagCore NUMBER;
      num_ref VARCHAR2(20);
      aut_core NUMBER;
      RUT_CORE VARCHAR2(20);
      monto_core NUMBER;
      fecha_core VARCHAR2(50);
      var_conc_SERVIPAG_core number;
      var_conc_SERVIPAG_inc number;
      estadoServipag NUMBER;


      sidCoreServipag   NUMBER;
      var_monto_core NUMBER;
      var_rut_benef NUMBER;
      estadoBice   NUMBER;
      id_oper_cl VARCHAR2(50);
      var_fecha_transac VARCHAR2(50);
      var_fecha_inc VARCHAR2(50);
      estadoCore NUMBER;
      var_tarjeta_core VARCHAR2(20 BYTE);
      operador_SERVIPAG NUMBER;

      contador NUMBER;

      /* Cursor PAGOS RECIBIDOS */
      CURSOR cursorPagos IS
       SELECT PAGO.SID AS SIDPAGO,
       TO_NUMBER(PAGO.MONTO_FINAL) AS MONTO,
       PAGO.FECHA_PAGO AS FECHATRX,
       PAGO.FECHA_CONTABLE AS FECHAINC,
       SUBSTR(PAGO.RUT,-10) AS RUTCLIENTE,
       PAGO.OPERADOR AS OPERADOR 
       FROM SIG.TBL_PAGOS_RECIBIDOS PAGO
       WHERE PAGO.FECHA_CONTABLE = TO_CHAR(SYSDATE, 'DD-MON-YY')
       AND OPERADOR = 7;


      /* Cursor CORE */
      CURSOR cursorCore IS
        SELECT CA.SID,
        TO_NUMBER(SUBSTR(CA.MONTO_TRANSAC,0,LENGTH(CA.MONTO_TRANSAC)-2)) AS MONTO,
        to_char(to_date(CA.FECHA_HR_TRASAC, 'DD/MM/YY'), 'DD-MON-YY', 'NLS_DATE_LANGUAGE = english') AS FEC_HR_TRX,
        TO_CHAR(CA.FECHA_INCOMING, 'YYYYMMDD') AS FECHAINC,
        CA.RUT AS CORE_RUT,
        CA.ORIGEN_TRANSAC AS OTRANSAC
        FROM TBL_CORE_AUTOR CA
        WHERE TO_CHAR(CA.FECHA_INCOMING, 'YYYYMMDD') = TO_CHAR(SYSDATE, 'YYYYMMDD')
        AND CA.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE UPPER(EMPRESA) = 'SERVIPAG');


      BEGIN
      cod_error := 0;
      warning := 'Proceso SP_SIG_CONC_CORE_SERVIPAG ejecutado';

       /*=====================CURSOR VISA=========================================*/
        OPEN cursorPagos;
        LOOP
          fetch cursorPagos into 
              var_sid_SERVIPAG,
              monto_transac_pagos,
              fecha_hora_transac, 
              fecha_inc,
              rut_cliente,
              var_operador;
          EXIT WHEN cursorPagos%NOTFOUND;
        --  dbms_output.Put_line('loop cursor Visa');
          sidCoreServipag := 0;
          -- 1 query para identificar las que no califican por hora
          -- 2 insertarlas con estado de no conciliadas por limite de hora
          -- 3 seguir proceso normal
       /*   dbms_output.Put_line('var_sid_SERVIPAG: '|| var_sid_SERVIPAG ||
          ' monto_transac:  ' ||monto_transac||
          ' fecha_hora_transac: ' ||fecha_hora_transac|| 
          ' fecha_inc:' ||fecha_inc|| 
          ' transaccion_id: '||transaccion_id|| 
          ' rut_cliente: '||rut_cliente|| 
          ' var_operador: ' ||var_operador);*/
         begin
           select  AC.SID, AC.RUT, AC.MONTO_TRANSAC, AC.FECHA_HR_TRASAC  
           into sidCoreServipag, RUT_CORE, monto_core, fecha_core
           from TBL_CORE_AUTOR AC where 
           TO_NUMBER(SUBSTR(AC.MONTO_TRANSAC,0,LENGTH(AC.MONTO_TRANSAC)-2))  = (monto_transac_pagos) 
           AND AC.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE UPPER(EMPRESA) = 'SERVIPAG') 
           AND AC.RUT = (rut_cliente)
           AND AC.FECHA_HR_TRASAC = (fecha_hora_transac)
           AND AC.SID = (SELECT MIN(SID) from TBL_CORE_AUTOR AC where 
           TO_NUMBER(SUBSTR(AC.MONTO_TRANSAC,0,LENGTH(AC.MONTO_TRANSAC)-2))  = (monto_transac_pagos) 
           AND AC.ORIGEN_TRANSAC IN (SELECT CODIGO_ORIGEN FROM TBL_ORIGEN_TRX WHERE UPPER(EMPRESA) = 'SERVIPAG') 
           AND AC.RUT = (rut_cliente)
           AND AC.FECHA_HR_TRASAC = (fecha_hora_transac))
           group by AC.SID, AC.RUT, AC.MONTO_TRANSAC, AC.FECHA_HR_TRASAC;
         exception
          WHEN NO_DATA_FOUND THEN
          sidCoreServipag := 0;
         end;
            ---dbms_output.Put_line('sidBiceTrebol: '||sidBiceTrebol || ' var_sid_trebol: '||var_sid_trebol || ' rut_clie: '||rut_clie || ' monto_bic: '||monto_bic || ' sid_opr_cl: '||sid_opr_cl);
           if(sidCoreServipag > 0)then
              estadoServipag := 1;
             elsif(sidCoreServipag = 0)then
              estadoServipag := 2;

           end if;

          begin

          -- dbms_output.Put_line(sidCoreServipag || ' - ' || var_sid_SERVIPAG);
           select  sid into var_conc_SERVIPAG_core from TBL_CONC_SERVIPAG_CORE where (SID_CORE = sidCoreServipag and sidCoreServipag > 0 ) or (SID_PAGOS_RECIBIDOS = var_sid_SERVIPAG);
           --dbms_output.put_line('var_conc_SERVIPAG_core: '||var_conc_SERVIPAG_core);
           exception
          WHEN NO_DATA_FOUND THEN
          --dbms_output.put_line('sid_bice_trebol: '||sidBiceTrebol||'--'||'var_sid_trebol: '||var_sid_trebol);
          if(sidCoreServipag>0 or var_sid_SERVIPAG>0)then
            delete from TBL_CONC_SERVIPAG_CORE where ((SID_CORE = sidCoreServipag and sidCoreServipag>0) or (SID_PAGOS_RECIBIDOS = var_sid_SERVIPAG)) and ESTADO_CONTABLE not like '1';
            --dbms_output.put_line(SQL%ROWCOUNT);
          end if;
          var_conc_SERVIPAG_core := 0;
         end;
       --    dbms_output.Put_line('var_conc_SERVIPAG_core: '||var_conc_SERVIPAG_core);
            update TBL_CONC_SERVIPAG_CORE set  SID_CORE = sidCoreServipag, SID_PAGOS_RECIBIDOS=var_sid_SERVIPAG, SID_ESTADO=estadoServipag, FECHA=fecha_inc, FECHA_REGISTRO= sysdate
            where sid = var_conc_SERVIPAG_core;
            if(SQL%ROWCOUNT = 0)THEN
              select count(*) into contador from TBL_CONC_SERVIPAG_CORE VC where SID_CORE=sidCoreServipag and VC.SID_PAGOS_RECIBIDOS=var_sid_SERVIPAG;
              --dbms_output.Put_line('Encuentra duplicados: '||contador);
              if(contador = 0)THEN             
                --dbms_output.Put_line('elementos a insertar: '||SYSDATE);
                insert into TBL_CONC_SERVIPAG_CORE(SID, SID_PAGOS_RECIBIDOS, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, OPERADOR )
                values(SEQ_TBL_CONC_SERVIPAG_CORE.nextval,var_sid_SERVIPAG,sidCoreServipag,estadoServipag,SYSDATE,monto_transac_pagos, SYSDATE, var_operador); 

                
              end if;
            end if;
          --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorPagos;
      dbms_output.Put_line('#################################################### FIN CURSOR PAGOS RECIBIDOS');


    /*=====================CURSOR CORE=========================================*/
     OPEN cursorCore;
        LOOP
         FETCH cursorCore INTO       
             var_sid_core,
             monto_core, 
             fecha_transac_core, 
             fecha_inc_core, 
             rut_core_autor,
             var_origen_transac;
         EXIT WHEN cursorCore%NOTFOUND;
        -- dbms_output.Put_line('loop cursor Core');
         sidServipagCore := 0;
         var_monto_core:= 0;
      /*   dbms_output.Put_line('var_sid_core :'||var_sid_core ||'-'||
         'monto_transac_core :'||monto_transac_core ||'-'||
         'fecha_transac_core :'||fecha_transac_core||'-'||
         'fecha_inc_core :'||fecha_inc_core||'-'||
         'codigo_autor_core :'||fecha_inc_core||'-'||
         'rutCORE :'||rut_core_autor||'-'||
         'var_origen_transac :'||var_origen_transac);*/
       begin

        SELECT I.sid, TO_NUMBER(I.MONTO_FINAL), I.FECHA_PAGO, I.FECHA_CONTABLE, I.OPERADOR
        INTO sidServipagCore, var_monto_core, var_fecha_transac, var_fecha_inc, operador_servipag
        FROM TBL_PAGOS_RECIBIDOS I  WHERE 
        TO_NUMBER(I.MONTO_FINAL) = monto_core 
        AND I.FECHA_PAGO = fecha_transac_core
        AND SUBSTR(I.RUT,-10) = rut_core_autor
        AND OPERADOR = 7
        AND I.SID = (SELECT MIN(SID) from TBL_PAGOS_RECIBIDOS I  WHERE 
        TO_NUMBER(I.MONTO_FINAL) = monto_core 
        AND I.FECHA_PAGO = fecha_transac_core
        AND SUBSTR(I.RUT,-10) = rut_core_autor
        AND OPERADOR = 7)
        GROUP BY I.sid, I.MONTO_FINAL, I.FECHA_PAGO, I.FECHA_CONTABLE, I.OPERADOR;
       exception
        WHEN NO_DATA_FOUND THEN
        sidServipagCore := 0;


       end;
         --dbms_output.Put_line('var_monto_bice: '||var_monto_bice ||' sidTrebolBice: '|| sidTrebolBice || ' id_oper_cl: '||id_oper_cl|| ' rut_beneficiario: '||var_rut_benef);      


       if(sidServipagCore > 0)then
          estadoCore := 1;

          else
              estadoCore := 3;
              operador_servipag := 0;
       end if;
        --dbms_output.Put_line('estadoBice: '||estadoBice); 
        --dbms_output.Put_line('var_sid_bice: '||var_sid_bice ||'sidTrebolBice: '||sidTrebolBice);
       begin

       --dbms_output.Put_line(var_sid_core || ' - ' || sidServipagCore);
         select  sid into var_conc_SERVIPAG_inc from TBL_CONC_SERVIPAG_CORE 
         where (SID_PAGOS_RECIBIDOS = sidServipagCore and sidServipagCore >0) or (SID_CORE = var_sid_core);
         exception
        WHEN NO_DATA_FOUND THEN
        var_conc_SERVIPAG_inc := 0;
       end;

      --   dbms_output.Put_line('---->>>var_conc_core_servipag: '||var_conc_SERVIPAG_inc);        
        update TBL_CONC_SERVIPAG_CORE set sid_core = var_sid_core, SID_PAGOS_RECIBIDOS = sidServipagCore, sid_estado = estadoCore
          where sid = var_conc_SERVIPAG_inc;
          if(SQL%ROWCOUNT = 0)THEN

           insert into TBL_CONC_SERVIPAG_CORE(SID, SID_PAGOS_RECIBIDOS, SID_CORE, SID_ESTADO, FECHA, MONTO, FECHA_REGISTRO, OPERADOR )
           --values(SEQ_TBL_CONC_VISA_CORE.nextval,sidCoreVisa,var_sid_visa,estadoVisa,fecha_inc,monto_transac, SYSDATE, 0); 
           values(SEQ_TBL_CONC_SERVIPAG_CORE.nextval,sidServipagCore,var_sid_core,estadoCore,SYSDATE, monto_core, SYSDATE, operador_servipag);  
          end if;
        --dbms_output.Put_line('==============================================================================');  
        END LOOP;
      CLOSE cursorCore;
      dbms_output.Put_line('Fin cursor Core');
      dbms_output.Put_line('##############################################################################  FIN CURSOR CORE ');

 /*==============================================================*/
--dbms_output.Put_line('Final del proceso');
--Sin este commit, el proceso no deja las conciliaciones registradas, no se cae, no da error, simplemente no las deja registradas
  COMMIT;
  END;

    EXCEPTION     
         WHEN NO_DATA_FOUND THEN
         dbms_output.Put_line( 'NO HAY REGISTROS EN TABLA' ); 
         warning := warning || '- NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS';
        WHEN OTHERS THEN                                                                      
                 warning := SQLERRM || ' - ' || DBMS_UTILITY.format_error_backtrace;
                 cod_error := SQLCODE;
                 dbms_output.Put_line( 'ERROR : ' || warning ||' SQLCODE : ' || cod_error );
                 dbms_output.Put_line( 'DESCRIPTION: ' || DBMS_UTILITY.format_error_backtrace);

                INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CONC_CORE_SERVIPAG', cod_error || '-' || warning);
                COMMIT;

END SP_SIG_CONC_CORE_SERVIPAG;