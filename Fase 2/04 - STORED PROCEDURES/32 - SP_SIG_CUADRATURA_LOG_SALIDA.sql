create or replace PROCEDURE SP_SIG_CUADRATURA_LOG_SALIDA (
-- Proyecto: SIG
-- By: Developer || RADS || ACL | 24/07/2013
-- Versión : 1.0
-- Descripcion: Consulta la cantidad de transacciones de outgoing pendientes, ok, cargo abono pendiente y ok.
                                                                  fechaProceso        IN VARCHAR2,
                                                                  filtro_operador     IN NUMBER,
                                                                  cantidadoutpen      OUT NUMBER,
                                                                  cantidadoutok       OUT NUMBER,
                                                                  cantidadcarpen      OUT NUMBER,
                                                                  cantidadcarok       OUT NUMBER,
                                                                  warning             OUT VARCHAR2,
                                                                  cod_error           OUT NUMBER
                                                                  )
IS
    v_cantidadoutpen     NUMBER:=0;
    v_cantidadoutok      NUMBER:=0;
    v_cantidadcarpen     NUMBER:=0;
    v_cantidadcarok      NUMBER:=0;

BEGIN

            BEGIN

                    -- Para outgoing
                    SELECT COUNT(*) INTO v_cantidadoutpen FROM TBL_GESTION_TRANSAC G, TBL_ESTADO_PROCESO E, TBL_INCOMING I  
                    WHERE 
--                    TO_CHAR(G.FECHA_INSERCION, 'YYYYMMDD')  = fechaProceso  AND 
                                                  G.TRANSACCION = I.SID           AND
                                                  I.OPERADOR = filtro_operador    AND
                                                  G.ESTADO_PROCESO = E.SID        AND 
                                                  E.XKEY = 'OU_PEN';

                    SELECT COUNT(*) INTO v_cantidadoutok FROM TBL_GESTION_TRANSAC G, TBL_ESTADO_PROCESO E, TBL_INCOMING I  
                    WHERE 
--                    TO_CHAR(G.FECHA_ACTUALIZACION, 'YYYYMMDD')  = fechaProceso    AND
                                                  G.TRANSACCION = I.SID                 AND
                                                  I.OPERADOR = filtro_operador          AND
                                                  G.ESTADO_PROCESO = E.SID              AND 
                                                  E.XKEY = 'OU_OK';

                    -- Para cargos abonos 
                    SELECT COUNT(*) INTO v_cantidadcarpen FROM TBL_GESTION_CARGO_ABONO G, TBL_ESTADO_PROCESO E, TBL_INCOMING I  
                    WHERE 
--                    TO_CHAR(G.FECHA_CARGO_ABONO, 'YYYYMMDD')  = fechaProceso    AND 
                                                  G.TRANSACCION = I.SID               AND
                                                  I.OPERADOR = filtro_operador        AND
                                                  G.ESTADO_PROCESO = E.SID            AND 
                                                  E.XKEY = 'CA_PEN';

                    SELECT COUNT(*) INTO v_cantidadcarok FROM TBL_GESTION_CARGO_ABONO G, TBL_ESTADO_PROCESO E, TBL_INCOMING I 
                    WHERE 
--                    TO_CHAR(G.FECHA_ACTUALIZACION, 'YYYYMMDD')  = fechaProceso  AND
                                                  G.TRANSACCION = I.SID               AND
                                                  I.OPERADOR = filtro_operador        AND
                                                  G.ESTADO_PROCESO = E.SID            AND 
                                                  E.XKEY = 'CA_OK';    

                    cantidadoutpen:=v_cantidadoutpen;    
                    cantidadoutok:=v_cantidadoutok;    
                    cantidadcarpen:=v_cantidadcarpen;      
                    cantidadcarok:=v_cantidadcarok;       

            END;

            EXCEPTION 
                    WHEN NO_DATA_FOUND     THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        DBMS_OUTPUT.PUT_LINE('NO EXISTE INFORMACION PARA LOS PARAMETROS INGRESADOS');
                    WHEN OTHERS            THEN
                        cod_error:=SQLCODE;
                        warning:=SQLERRM;
                        INSERT INTO SIG.LOG_ERROR (SID, FECHA_INSERCION, NOMBRE_SP, MSG_ERROR) VALUES (SIG.SEQ_LOG_ERROR.NEXTVAL, SYSDATE, 'SP_SIG_CUADRATURALOGPROCESOSAL', cod_error || '-' || warning);
                        DBMS_OUTPUT.PUT_LINE('ERROR : ' || ' ' || cod_error || '-' || warning);
END SP_SIG_CUADRATURA_LOG_SALIDA;